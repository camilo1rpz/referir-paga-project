from django.urls import path
from . import views

app_name = 'logginas'

urlpatterns = [
    path('select/<int:usuario_id>/', views.select_view, name='select-user'),
    path('reset/', views.reset_view, name='reset-user'),
]