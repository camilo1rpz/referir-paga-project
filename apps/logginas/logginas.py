from django.conf import settings


class ViewAs(object):
    def __init__(self, request):
        self.session = request.session
        view_as = self.session.get(settings.USER_SESSION_ID)
        if not view_as:
            view_as = self.session[settings.USER_SESSION_ID] = {}
        self.view_as = view_as

    def add(self, usuario_id):
        self.view_as['as_usuario'] = usuario_id
        self.save()

    def save(self):
        self.session.modifided = True

    def remove(self):
        self.view_as['as_usuario'] = ""
        self.save()

    def clear(self):
        del self.session[settings.USER_SESSION_ID]
        self.save()
