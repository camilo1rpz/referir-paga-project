from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from .logginas import ViewAs


def select_view(request, usuario_id):
    view_as = ViewAs(request)
    view_as.add(usuario_id)
    view_as.save()
    request.user.is_administrador = True
    request.user.save()
    return redirect('usuarios:usuario-index')


def reset_view(request):
    view_as = ViewAs(request)
    view_as.remove()
    view_as.clear()
    view_as.save()
    request.user.is_administrador = False
    request.user.save()
    return redirect('clientes:cliente-index')
