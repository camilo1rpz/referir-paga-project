from django.conf import settings

from apps.clientes.models import Cliente
from apps.usuarios.models import Usuario
from django.views.generic import View
from django.shortcuts import render
from django.http import HttpResponse
from apps.sedes.models import PtoOperacion
from apps.gestores.models import Enlace, Servicio, Publicidad, Campana
import xlsxwriter
import io
import os
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required   
def ReportesClientes(request):
    queryset = Cliente.objects.filter(eliminado=False).order_by('-estado')
    fila = 1
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Clientes Referir Paga")
    # Add a bold format to use to highlight cells.

    header_style = workbook.add_format({
        'bold': True,
        'font_color': '#ffffff',
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#419BAF',
        'border': 1,
        'font_size': 10,
        'text_wrap': 1})

    body_style = workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size': 8})

    fields = ["Estado", "Nombre/Razón Social", "Apellidos",
              "Dirección", "Complemento", "Barrio", "Tipo Documento",
              "# Documento", "Teléfono", "Email", "Contacto", "Ciudad",
              "Tipo Sociedad", "Tipo Organización", "Régimen Fiscal"]

    for x in range(len(fields)):
        worksheet.write(0, x, fields[x], header_style)
        worksheet.set_row(0, 25)  # Set the height of Row 1 to 20.
        # worksheet.set_default_row(20) para altura a todas las filas

    for x in queryset:
        bis1 = ""
        if x.bis_via:
            bis1 = "bis"

        bis2 = ""
        if x.bis_via_generadora:
            bis2 = "bis"

        direccion = (str(x.tipo_via) + " " + str(x.numero_via or "") + " " +
                     str(x.prefijo_via or "") + " " + str(bis1 or "") + " " +
                     str(x.cuadrante_via or "") + " " + str(x.numero_via_generadora or "") + " " +
                     str(x.prefijo_via_generadora or "") + " " + str(bis2 or "") + " " +
                     str(x.numero_placa or "") + str(x.cuadrante_via_generadora or ""))


        if x.estado:
            estado = "Activo"
        else:
            estado ="Inactivo"

        worksheet.write(
            fila, 0, estado.upper(), body_style)  # estado
        worksheet.write(
            fila, 1, str(
                x.nombre_o_razon or "").upper(), body_style)  # estado
        worksheet.write(fila, 2, str(x.apellido or "").upper(),
                        body_style)  # apellido
        worksheet.write(
            fila,
            3,
            str(direccion).upper(),
            body_style)  # direccion
        worksheet.write(
            fila, 4, str(
                x.complemento or "").upper(), body_style)  # Complemento
        worksheet.write(
            fila, 5, str(
                x.barrio or "").upper(), body_style)  # Barrio
        worksheet.write(fila,
                        6,
                        str(x.tipo_identificacion or "").upper(),
                        body_style)  # Tipo Documento
        worksheet.write(
            fila, 7, str(
                x.identificacion or "").upper(), body_style)  # Documento
        worksheet.write(fila, 8, str(x.telefono or "").upper(),
                        body_style)  # Telefono
        worksheet.write(fila, 9, str(x.email or ""), body_style)  # Email
        worksheet.write(
            fila, 10, str(
                x.nombre_contacto or "").upper(), body_style)  # Contacto
        worksheet.write(
            fila, 11, str(
                x.ciudad or "").upper(), body_style)  # Ciudad"
        worksheet.write(
            fila, 12, str(
                x.tipo_sociedad or "").upper(), body_style)  # Tipo Sociedad
        worksheet.write(fila,
                        13,
                        str(x.tipo_organizacion or "").upper(),
                        body_style)  # Tipo Organizacion
        worksheet.write(
            fila, 14, str(
                x.regimenfiscal or "").upper(), body_style)  # RegimenFiscal
        fila = fila + 1

    worksheet.set_column(0, 14, 20)
    worksheet.autofilter(0, 0, fila, 14)
    workbook.close()

    output.seek(0)
    filename = 'Clientes-Referir-Paga.xlsx'
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response

@login_required   
def ReportesUsuarios(request):
    as_user = request.session.get(settings.USER_SESSION_ID)
    if as_user:
        cliente = Cliente.objects.get(id=as_user['as_usuario'])
    else:
        cliente = request.user.cliente_set.all()[0]
    queryset = Usuario.objects.filter(
        eliminado=False,
        cliente=cliente).order_by('first_name')

    fila = 1
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Usuarios Referir Paga")
    # Add a bold format to use to highlight cells.

    header_style = workbook.add_format({
        'bold': True,
        'font_color': '#ffffff',
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#419BAF',
        'border': 1,
        'font_size': 10,
        'text_wrap': 1})

    body_style = workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size': 8})

    fields = [
        "Estado", "Nombres", "Apellidos", "Tipo Documento",
        "# Documento", "Rol", "Código", "Email", "Teléfono/Ext",
        "Dirección", "Complemento", "Barrio", "Fecha Nacimiento",
        "Asesor", "R.Campaña", "Punto Operación", "Regional",
        "Ciudad", "Departamento ", "País"
    ]

    for x in range(len(fields)):
        worksheet.write(0, x, fields[x], header_style)
        worksheet.set_row(0, 25)  # Set the height of Row 1 to 20.
        # worksheet.set_default_row(20) para altura a todas las filas

    for x in queryset:
        ext =""
        if x.extencion:
            ext = "/ " + str(x.extencion)

        bis1 = ""
        if x.bis_via_generadora:
            bis1 = "BIS"

        bis2 = ""
        if x.bis_via:
            bis2 = "BIS"

        rol = "Otro rol"

        if x.estado:
            estado = "Activo"
        else:
            estado ="Inactivo"

        direccion = (str(x.tipo_via or "") + " " + str(x.numero_via or "") + " " +
                     str(x.prefijo_via or "") + " " + str(bis1 or "") + " " +
                     str(x.cuadrante_via or "") + " " + str(x.numero_via_generadora or "") + " " +
                     str(x.prefijo_via_generadora or "") + " " + str(bis2 or "") + " " +
                     str(x.numero_placa or "") + " " + str(x.cuadrante_via_generadora or ""))

        #regional = ptooperacion_set
        a = ""
        b = ""
        contador = 1
        for y in x.ptooperacion_set.all():
            a += "\n" +"* "+str(y)
            b += "\n" +"* "+ str(y.regional)
            contador += 1

        rol = ""
        if x.is_administrador:
            rol = "ADMINISTRADOR"
        elif x.is_administrador_punto:
            rol = "ADMINISTRADOR PUNTO"
        elif x.is_responsable_campana:
            rol = "RESPONSABLE CAMPAÑA"
        elif x.is_asesor:
            rol = "ASESOR"
        elif x.is_persona_natural and not obj.is_asesor:
            rol = "PERSONA NATURAL"
        else:
            pass

        try :
            asesor = str(x.asesor.codigo) + " "+str(x.asesor)
        except Exception:
            asesor = ""

        worksheet.write(
            fila, 0, estado.upper(), body_style)  # estado
        worksheet.write(
            fila, 1, str(
                x.first_name or "").upper(), body_style)  # first_name
        worksheet.write(
            fila, 2, str(
                x.last_name or "").upper(), body_style)  # last_name
        worksheet.write(fila,
                        3,
                        str(x.tipo_identificacion or "").upper(),
                        body_style)  # tipo_identificacion
        worksheet.write(fila,
                        4,
                        str(x.numero_documento or "").upper(),
                        body_style)  # numero_documento
        worksheet.write(fila, 5, str(rol).upper(), body_style)  # rol
        worksheet.write(
            fila, 6, str(
                x.codigo or "").upper(), body_style)  # codigo
        worksheet.write(
            fila, 7, str(
                x.email or ""), body_style)  # email
        worksheet.write(
            fila,
            8,
            (str(
                x.telefono or "") + ext ).upper(),
            body_style)  # telefono
        worksheet.write(fila, 9, str(direccion or ""), body_style)  # direccion
        worksheet.write(
            fila, 10, str(
                x.complemento or "").upper(), body_style)  # complemento
        worksheet.write(
            fila, 11, str(
                x.barrio or "").upper(), body_style)  # barrio
        worksheet.write(fila,
                        12,
                        str(x.fecha_nacimiento or "").upper(),
                        body_style)  # fecha_nacimiento

        worksheet.write(
            fila, 13, str(asesor).upper(), body_style)  # asesor
        worksheet.write(
            fila,
            14,
            str("* Resposable_de_campaña").upper(),
            body_style)  # Resposable_de_campaña
        worksheet.write(fila, 15, str(a or "").upper(),
                        body_style)  # Resposable_de_campaña
        # Resposable_de_campaña
        worksheet.write(fila, 16, str(b or ""), body_style)
        worksheet.write(fila, 17, str(x.ciudad or "").upper(),
                        body_style)  # Resposable_de_campaña
        worksheet.write(fila, 18, str(x.departamento or "").upper(),
                        body_style)  # Resposable_de_campaña
        worksheet.write(
            fila, 19, str(
                x.pais or "").upper(), body_style)  # pais

        fila = fila + 1

    worksheet.set_column(0, 19, 20)
    worksheet.autofilter(0, 0, fila, 19)
    workbook.close()

    output.seek(0)
    filename = 'Usuarios-Referir-Paga.xlsx'
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response


@login_required   
def ReportesRegionalesPto(request):

    #cliente = request.user.cliente_set.all()[0]
    cliente = request.user.cliente_set.first()
    queryset = PtoOperacion.objects.filter(cliente=cliente).order_by('nombre')

    fila = 1
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Regionales-pto Referir Paga")
    # Add a bold format to use to highlight cells.

    header_style = workbook.add_format({
        'bold': True,
        'font_color': '#ffffff',
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#419BAF',
        'border': 1,
        'font_size': 10,
        'text_wrap': 1})

    body_style = workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size': 8})

    fields = ["Estado","Nombre", "Regional", "Telefono1", "Telefono2", 
                "Direccion", "Complemento", "Barrio", 
                "Ciudad", "Departamento", "Pais",
                ]

    for x in range(len(fields)):
        worksheet.write(0, x, fields[x], header_style)
        worksheet.set_row(0, 25)  # Set the height of Row 1 to 20.
        # worksheet.set_default_row(20) para altura a todas las filas


    for x in queryset:
        estado = ""
        extencion1 =""
        if x.extencion1:
            extencion1 = "/ " + str(x.extencion1)

        extencion2 =""
        if x.extencion2:
            extencion2 = "/ " + str(x.extencion2)


        bis1 = ""
        if x.bis_via:
            bis1 = "BIS"

        bis2 = ""
        if x.bis_via_generadora:
            bis2 = "BIS"

        if x.estado:
            estado = "Activo"
        else:
            estado ="Inactivo"

        direccion = (str(x.tipo_via or "") + " " + str(x.numero_via or "") + " " +
                     str(x.prefijo_via or "") + " " + str(bis1 or "") + " " +
                     str(x.cuadrante_via or "") + " " + str(x.numero_via_generadora or "") + " " +
                     str(x.prefijo_via_generadora or "") + " " + str(bis2 or "") + " " +
                     str(x.numero_placa or "") + " " + str(x.cuadrante_via_generadora or ""))

        #regional = ptooperacion_set

        worksheet.write(
            fila, 0, estado.upper(), body_style)  # estado
        worksheet.write(
            fila, 1, str(
                x.nombre or "").upper(), body_style)  # first_name
        worksheet.write(
            fila, 2, str(
                x.regional or "").upper(), body_style)  # last_name


        worksheet.write(
            fila,
            3,
            (str(
                x.telefono1 or "") + extencion1 ).upper(),
            body_style) 

        worksheet.write(
            fila,
            4,
            (str(
                x.telefono2 or "") + extencion2 ).upper(),
            body_style) 



        worksheet.write(fila, 5, str(direccion or ""), body_style)  # direccion

        worksheet.write(fila, 6, str(x.complemento or "").upper(), body_style)
        worksheet.write(fila, 7, str(x.barrio or "").upper(), body_style)
       
        worksheet.write(fila, 8, str(x.ciudad or "").upper(),
                        body_style)  # Resposable_de_campaña
        worksheet.write(fila, 9, str(x.departamento or "").upper(),
                        body_style)  # Resposable_de_campaña
        worksheet.write(
            fila, 10, str(
                x.pais or "").upper(), body_style)  # pais

        fila = fila + 1

    worksheet.set_column(0, 10, 20)
    worksheet.autofilter(0, 0, fila, 10)
    workbook.close()

    output.seek(0)
   
    filename = 'Regional-pto-Referir-Paga.xlsx'
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response

@login_required   
def ReportesEnlaces(request):
    #cliente = request.user.cliente_set.all()[0]
    cliente = request.user.cliente_set.first()
    queryset = Enlace.objects.filter(cliente=cliente).order_by('nombre')
    fila = 1
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Enlaces-Enlaces Referir Paga")
    # Add a bold format to use to highlight cells.
    header_style = workbook.add_format({
        'bold': True,
        'font_color': '#ffffff',
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#419BAF',
        'border': 1,
        'font_size': 10,
        'text_wrap': 1})
    body_style = workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size': 8})
    
    fields = ["Estado","Nombre", "Url",]

    for x in range(len(fields)):
        worksheet.write(0, x, fields[x], header_style)
        worksheet.set_row(0, 25)  # Set the height of Row 1 to 20.
        # worksheet.set_default_row(20) para altura a todas las filas

    for x in queryset:

        if x.estado:
            estado = "Activo"
        else:
            estado ="Inactivo"

        worksheet.write(
            fila, 0, estado.upper(), body_style)  # estado
        worksheet.write(
            fila, 1, str(
                x.nombre or "").upper(), body_style)  # first_name
        worksheet.write(
            fila, 2, str(
                x.url or ""), body_style)
        fila = fila + 1

    worksheet.set_column(0, 2, 20)
    worksheet.autofilter(0, 0, fila, 2)
    workbook.close()
    output.seek(0)   
    filename = 'Enlaces-Enlaces Referir Paga.xlsx'
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response




@login_required   
def ReportesPublicidades(request):
    as_user = request.session.get(settings.USER_SESSION_ID)
    if as_user:
        cliente = Cliente.objects.get(id=as_user['as_usuario'])
    else:
        cliente = request.user.cliente_set.all()[0]
    queryset = Publicidad.objects.filter(cliente=cliente).order_by('nombre')
    fila = 1
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Publicidad Referir Paga")
    # Add a bold format to use to highlight cells.
    header_style = workbook.add_format({
        'bold': True,
        'font_color': '#ffffff',
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#419BAF',
        'border': 1,
        'font_size': 10,
        'text_wrap': 1})
    body_style = workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size': 8})
    
    fields = ["Estado","Nombre", "Url", "Imagen"]

    for x in range(len(fields)):
        worksheet.write(0, x, fields[x], header_style)
        # worksheet.set_default_row(20) para altura a todas las filas

    for x in queryset:

        if x.estado:
            estado = "Activo"
        else:
            estado ="Inactivo"

        worksheet.write(
            fila, 0, estado.upper(), body_style)  # estado
        worksheet.write(
            fila, 1, str(
                x.nombre or "").upper(), body_style)  # first_name
        worksheet.write(
            fila, 2, str(
                x.url or ""), body_style)

        worksheet.insert_image(fila, 3,  str(os.getcwd()+"/static/"+str(x.imagen)), {'x_scale': 0.05, 'y_scale': 0.05})
        
        worksheet.set_row(fila, 40)  # Set the height of Row 1 to 20.
        fila = fila + 1

    worksheet.set_column(0, 2, 20)
    worksheet.autofilter(0, 0, fila, 2)
    workbook.close()
    output.seek(0)   
    filename = 'Publicidad Referir Paga.xlsx'
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response
        

@login_required   
def ReportesServicios(request):
    as_user = request.session.get(settings.USER_SESSION_ID)
    if as_user:
        cliente = Cliente.objects.get(id=as_user['as_usuario'])
    else:
        cliente = request.user.cliente_set.all()[0]
    queryset = Servicio.objects.filter(cliente=cliente, eliminado=False).order_by('nombre')
    fila = 1
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Servicios Referir Paga")
    # Add a bold format to use to highlight cells.
    header_style = workbook.add_format({
        'bold': True,
        'font_color': '#ffffff',
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#419BAF',
        'border': 1,
        'font_size': 10,
        'text_wrap': 1})
    body_style = workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size': 8})

    fields = ["Estado","Nombre", "Icono", "Color Icono", "Color Fondo"]

    for x in range(len(fields)):
        worksheet.write(0, x, fields[x], header_style)
        worksheet.set_row(0, 25)  # Set the height of Row 1 to 20.
        # worksheet.set_default_row(20) para altura a todas las filas
    for x in queryset:

        if x.estado:
            estado = "Activo"
        else:
            estado ="Inactivo"

        worksheet.write(
            fila, 0, estado.upper(), body_style)  # estado
        
        worksheet.write(
            fila, 1, str(
                x.nombre or "").upper(), body_style)  # first_name
        
        worksheet.write(
            fila, 2, str(
                x.icono or ""), body_style)

        worksheet.write(
            fila, 3, str(
                x.color_icono or ""), 
                    workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size':10,
                                      'color': str(x.color_icono),
                                      'bold': True,}))

        worksheet.write(
            fila, 4, str(
                x.color_fondo or ""), 
                    workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size':10,
                                      'color': str(x.color_fondo),
                                      'bold': True,}))
        fila = fila + 1

    worksheet.set_column(0, 4, 20)
    worksheet.autofilter(0, 0, fila, 4)
    workbook.close()
    output.seek(0)   
    filename = 'Servicios Referir Paga.xlsx'
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response

@login_required
def ReportesProductos(request):
    as_user = request.session.get(settings.USER_SESSION_ID)
    if as_user:
        cliente = Cliente.objects.get(id=as_user['as_usuario'])
    else:
        cliente = request.user.cliente_set.all()[0]
    queryset = Servicio.objects.filter(cliente=cliente, eliminado=False).order_by('nombre')
    fila = 1
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Servicios Referir Paga")
    # Add a bold format to use to highlight cells.
    header_style = workbook.add_format({
        'bold': True,
        'font_color': '#ffffff',
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#419BAF',
        'border': 1,
        'font_size': 10,
        'text_wrap': 1})
    body_style = workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size': 8})

    fields = ["Estado","Nombre", "Icono", "Color Icono", "Color Fondo"]

    for x in range(len(fields)):
        worksheet.write(0, x, fields[x], header_style)
        worksheet.set_row(0, 25)  # Set the height of Row 1 to 20.
        # worksheet.set_default_row(20) para altura a todas las filas
    for x in queryset:

        if x.estado:
            estado = "Activo"
        else:
            estado ="Inactivo"

        worksheet.write(
            fila, 0, estado.upper(), body_style)  # estado

        worksheet.write(
            fila, 1, str(
                x.nombre or "").upper(), body_style)  # first_name

        worksheet.write(
            fila, 2, str(
                x.icono or ""), body_style)

        worksheet.write(
            fila, 3, str(
                x.color_icono or ""),
                    workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size':10,
                                      'color': str(x.color_icono),
                                      'bold': True,}))

        worksheet.write(
            fila, 4, str(
                x.color_fondo or ""),
                    workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size':10,
                                      'color': str(x.color_fondo),
                                      'bold': True,}))
        fila = fila + 1

    worksheet.set_column(0, 4, 20)
    worksheet.autofilter(0, 0, fila, 4)
    workbook.close()
    output.seek(0)
    filename = 'Servicios Referir Paga.xlsx'
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response


@login_required
def ReportesCampana(request):
    as_user = request.session.get(settings.USER_SESSION_ID)
    if as_user:
        cliente = Cliente.objects.get(id=as_user['as_usuario'])
    else:
        cliente = request.user.cliente_set.all()[0]
    queryset = Campana.objects.filter(cliente=cliente).order_by('nombre')
    fila = 1
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet("Servicios Referir Paga")
    # Add a bold format to use to highlight cells.
    header_style = workbook.add_format({
        'bold': True,
        'font_color': '#ffffff',
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '#419BAF',
        'border': 1,
        'font_size': 10,
        'text_wrap': 1})
    body_style = workbook.add_format({'align': 'left',
                                      'valign': 'vcenter',
                                      'border': 1,
                                      'font_size': 8})

    fields = ["Estado","Nombre", "codigoCampana", "responsableCampana", "fecha_inicio", "fecha_fin"]

    for x in range(len(fields)):
        worksheet.write(0, x, fields[x], header_style)
        worksheet.set_row(0, 25)  # Set the height of Row 1 to 20.
        # worksheet.set_default_row(20) para altura a todas las filas
    for x in queryset:

        if x.estado:
            estado = "Activo"
        else:
            estado ="Inactivo"

        worksheet.write(
            fila, 0, estado.upper(), body_style)  # estado

        worksheet.write(
            fila, 1, str(
                x.nombre or "").upper(), body_style)  # first_name

        worksheet.write(
            fila, 2, str(
                x.codigoCampana or ""), body_style)

        worksheet.write(
            fila, 3, str(
                x.responsableCampana or ""), body_style)

        worksheet.write(
            fila, 4, str(
                x.fecha_inicio or ""), body_style)

        worksheet.write(
            fila, 5, str(
                x.fecha_fin or ""), body_style)

        fila = fila + 1

    worksheet.set_column(0, 5, 20)
    worksheet.autofilter(0, 0, fila, 5)
    workbook.close()
    output.seek(0)
    filename = 'Campana Referir Paga.xlsx'
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response


@login_required   
def A(request):
    data={}
    data['user']="nombre usuario"
    data['rol'] = "Admin"
    data['username'] = "Username"
    data['password'] = "PASSWORD"

    return render(request,'otros/template_email_registro.html',{'data':data})
    pass