from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.views.generic import CreateView, ListView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.views.generic import DetailView
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
import json
from .forms import *
from apps.usuarios.models import Usuario
from apps.validators import contrasena, codigo
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.auth.mixins import LoginRequiredMixin
import logging

from ..gestores.models import Campana, ReferidosApp

logger = logging.getLogger(__name__)

# Create your views here.

class CrearUsuario(LoginRequiredMixin, CreateView):
    model = Usuario
    template_name = "usuarios/nuevo.html"
    success_url = reverse_lazy('usuarios:usuario-index')
    form_class = UsuarioForm

    def get_form_kwargs(self):
        kwargs = super(CrearUsuario, self).get_form_kwargs()
        kwargs.update({'login_user': self.request.user})
        return kwargs

    def form_valid(self, form):
        """Se crea instancioa de fomulario y se le agrega una pass y un username """

        model = form.save(commit=False)
        model.padre = self.request.user
        model.username = form.data['email']
        model.empresa_idCliente = self.request.user.cliente_set.all()[0].pk
        asesordata = form.data['asesor']
        regional = form.data['regional']
        rol = form.data['rol']
        password = contrasena()
        user_logged = self.request.user

        if "is_administrador" == rol:
            if self.request.user.is_administrador:
                model.is_administrador = True
                model.set_password(password)
                model.save()
                logger.info('Usuario: {}, con email: {}, con rol: {}. ha sido CREADO por administrador con email: {}'.format
                            (str(form.data['first_name']), str(form.data['email']), rol, user_logged))
                data = {'user': str(form.data['first_name']), 'rol': "Administrador",
                        'username': str(form.data['email']), 'password': password}
                subject, from_email, to = 'welcome', 'noreply@mail.tnfactor.com.co', str(form.data['email'])
                html_content = render_to_string('otros/template_email_registro.html', {'data': data})
                msg = EmailMessage(subject, html_content, from_email, [to])
                msg.content_subtype = "text/html"  # Main content is now text/html
                msg.send()

                """Aca se le asigna la relacion con el cliente"""
                model.cliente_set.add(self.request.user.cliente_set.all()[0].pk)

        elif "is_administrador_punto" == rol:
            pto_operaciones = form.cleaned_data['Punto_Operacion'].values()
            list_result = [entry for entry in pto_operaciones]

            if self.request.user.is_administrador:
                model.is_administrador_punto = True
                model.set_password(password)
                model.save()
                logger.info('Usuario: {}, con email: {}, con rol: {}. ha sido CREADO por '
                            'administrador con email: {}'.format(str(form.data['first_name']),
                                                                 str(form.data['email']), rol,
                                                                 user_logged))
                data = {'user': str(form.data['first_name']), 'rol': "Administrador_punto",
                        'username': str(form.data['email']), 'password': password}
                subject, from_email, to = 'welcome', 'noreply@mail.tnfactor.com.co', str(form.data['email'])
                html_content = render_to_string('otros/template_email_registro.html', {'data': data})
                msg = EmailMessage(subject, html_content, from_email, [to])
                msg.content_subtype = "html"  # Main content is now text/html
                msg.send()

                if list_result:
                    for pto in list_result:
                        model.ptooperacion_set.add(pto['id'])
                        model.save()
                        logger.info('Punto de operaciones: {} ha sido asignado a usuario: {}, con email: {}, con rol: '
                            '{},  por administrador con email: {}'.format(pto['nombre'],
                                                                          str(form.data['first_name']),
                                                                          str(form.data['email']), rol, user_logged))
            model.cliente_set.add(self.request.user.cliente_set.all()[0].pk)

        elif "is_asesor" == rol:
            a = form.cleaned_data['Punto_Operacion'].values()
            list_result = [entry for entry in a]

            if self.request.user.is_administrador or self.request.user.is_administrador_punto:
                model.is_asesor = True
                model.is_persona_natural = True
                while True:
                    codigo_generado = codigo()
                    if not Usuario.objects.filter(is_asesor=True, codigo=codigo_generado):
                        break
                model.codigo = codigo_generado
                model.save()
                logger.info('Usuario: {}, con email: {}, con rol: {}. ha sido CREADO por '
                            'administrador con email: {}'.format(str(form.data['first_name']),
                                                                 str(form.data['email']), rol,
                                                                 user_logged))
                if list_result:
                    for pto in list_result:
                        model.ptooperacion_set.add(pto['id'])
                        model.save()
                        logger.info('Punto de operaciones: {} ha sido asignado a usuario: {}, con email: {}, con rol: '
                                    '{},  por administrador con email: {}'.format(pto['nombre'],
                                                                                  str(form.data['first_name']),
                                                                                  str(form.data['email']), rol,
                                                                                  user_logged))
            model.cliente_set.add(self.request.user.cliente_set.all()[0].pk)

        elif 'is_responsable_campana' == rol:
            if self.request.user.is_administrador_punto or self.request.user.is_administrador:
                model.is_responsable_campana = True
                # if form.data['asesor']:
                #     print(dir(form.data))
                # else:
                #     print("Nada")
                # ##guarda un asesor
                model.save()
                logger.info('Usuario: {}, con email: {}, con rol: {}. ha sido CREADO por '
                            'administrador con email: {}'.format(str(form.data['first_name']),
                                                                 str(form.data['email']), rol,
                                                                 user_logged))
                model.cliente_set.add(self.request.user.cliente_set.all()[0].pk)

        #		elif 'is_persona_natural' == rol:
        #			model.is_persona_natural=True

        else:
            model.save()
            model.cliente_set.add(self.request.user.cliente_set.all()[0].pk)
            pass
        return super().form_valid(form)


class ConsultarUsuario(LoginRequiredMixin, DetailView):
    model = Usuario
    template_name = 'usuarios/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'Eliminar' in request.get_full_path():
            context = self.get_context_data(object=self.object, eliminar=True)
        return self.render_to_response(context)


class EliminarUsuario(LoginRequiredMixin, DeleteView):
    model = Usuario

    def post(self, request, *args, **kwargs):
        try:
            borrar = request.POST.get('id')
            valores = self.model.objects.get(pk=borrar)
            cliente = valores.cliente_set.all()[0]
            rol = ''
            if valores.is_administrador:
                if (cliente.usuario.filter(is_administrador=True).count() > 1 and not Usuario.objects.filter(
                        padre=valores.pk) and not valores.ptooperacion_set.filter()):
                    valores.cliente_set.remove(cliente)
                    valores.delete()
                    valores.save()
                    dato = 0
                    rol = 'administrador'
                else:
                    dato = 1

            elif valores.is_administrador_punto:
                if not Usuario.objects.filter(padre=valores.pk):
                    for x in PtoOperacion.objects.filter(usuarios=valores):
                        valores.ptooperacion_set.remove(x)
                    valores.delete()
                    valores.save()
                    dato = 0
                    rol = 'administrador de punto'
                else:
                    dato = 2

            elif valores.is_asesor:
                if not ReferidosApp.objects.filter(asesor=valores.codigo) \
                        and not Usuario.objects.filter(asesor=valores):
                    valores.delete()
                    valores.save()
                    dato = 0
                    rol = 'asesor'
                # por validar
                # Si el rol del usuario a eliminar es Asesor validar para su eliminación:
                # Que no haya una apertura con su código.
                # Que no haya referido a nadie y/o
                # Que no haya un responsable de campaña asociado a su código.
                else:
                    dato = 3

            elif valores.is_responsable_campana:
                if not Campana.objects.filter(responsableCampana=valores):
                    valores.delete()
                    valores.save()
                    dato = 0
                    rol = 'responsable de campana'
                # pendiente a validar:
                # Si el rol del usuario a eliminar es Responsable Campaña validar para su eliminación:
                # Que no esté asociado a una campaña y/o
                # Que no haya creado, actualizado, o eliminado una campaña.
                # Si el rol del usuario Responsable Campaña puede ser eliminado mostrar modal de confirmación. (ir al paso Confirmar Eliminación)
                # Si el rol del usuario Responsable Camapaña no puede ser eliminado mostrar el modal de error. (ir al paso Error Eliminación)
                else:
                    dato = 4

            elif valores.is_persona_natural:
                if True:
                    dato = 0
                    rol = 'persona natural'
                else:
                    dato = 5
            else:
                dato = 6

        except Exception:
            dato = 7
        if dato == 0:
            logger.info('Usuario: {}, con email: {}, con rol: {}. ha sido ELIMINADO por '
                        'administrador con email: {}'.format(valores.first_name, valores.email,
                                                             rol, request.user))
        return HttpResponse(json.dumps(dato), content_type='application/json')


class ListarUsuario(LoginRequiredMixin, ListView):
    model = Usuario
    template_name = 'usuarios/index.html'

    paginate_by = 50

    def get(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Cliente.objects.get(id=as_user['as_usuario'])
        else:
            cliente = request.user.cliente_set.all()[0]

        search = request.GET.get('q')
        if search:
            filtro = self.model.objects.filter(Q(eliminado=False) & Q(cliente=cliente) &
                                               (
                                                       Q(last_name__icontains=search) |
                                                       Q(barrio__icontains=search) |
                                                       Q(ciudad__nombre__icontains=search) |
                                                       Q(codigo__icontains=search) |
                                                       Q(complemento__icontains=search) |
                                                       Q(cuadrante_via__icontains=search) |
                                                       Q(cuadrante_via_generadora__icontains=search) |
                                                       Q(numero_placa__icontains=search) |
                                                       Q(email__icontains=search) |
                                                       Q(extencion__icontains=search) |
                                                       Q(telefono__icontains=search) |
                                                       Q(first_name__icontains=search) |
                                                       Q(fecha_nacimiento__icontains=search) |
                                                       Q(numero_documento__icontains=search)
                                                   # tipo doc
                                                   # roles
                                               )
                                               ).order_by('first_name')

            self.object_list = filtro
        else:
            self.object_list = self.model.objects.filter(eliminado=False, cliente=cliente).order_by('first_name')

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        return self.render_to_response(context)


class ActualizarUsuario(LoginRequiredMixin, UpdateView):
    model = Usuario
    template_name = "usuarios/editar.html"
    success_url = reverse_lazy('usuarios:usuario-index')
    form_class = UsuarioUpdateForm

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        model = form.save()
        logger.info('Usuario: {} {}, ha sido MODIFICADO por administador con email: {}'.
                       format(model.first_name, model.last_name, Cliente.objects.get(id=self.cliente_id).email))
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(ActualizarUsuario, self).get_form_kwargs()
        kwargs.update({'login_user': self.request.user})
        return kwargs


def crud_fail(request):
    response = TemplateResponse(request, 'usuarios/crud_fail.html', {})
    return response

