from django import forms
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from apps.clientes.models import Cliente
from apps.ubicacion.models import Pais, Departamento, Ciudad
from apps.validators import emailexiste
from apps.usuarios.models import *
import datetime
from django.conf import settings
from apps.sedes.models import Regional, PtoOperacion
from email.mime.image import MIMEImage

class UsuarioForm(forms.ModelForm):
    Rol = [["", "Seleccione un Rol"],
           ['is_administrador', 'Administrador'],
           ['is_administrador_punto', 'Administrador Punto'],
           ['is_asesor', 'Asesor'],
           ['is_responsable_campana', 'Responsable Campaña'],
           # ['is_persona_natural','Persona Natural'],
           ]

    email = forms.EmailField(required=True,
                             validators=[emailexiste],
                             widget=forms.EmailInput(
                                 attrs={'class': "form-control",
                                        'placeholder': 'emailempreas@empresa.em'}
                             )
                             )

    ConfirmarEmail = forms.EmailField(required=True,
                                      widget=forms.EmailInput(
                                          attrs={
                                              'class': "form-control", 'placeholder': 'emailempreas@empresa.em'}
                                      )
                                      )

    rol = forms.ChoiceField(choices=Rol, required=True,
                            widget=forms.Select(
                                attrs={'class': "form-control"}
                            )
                            )

    regional = forms.ChoiceField(choices=[["", ""]], required=False,
                                 widget=forms.Select(
                                     attrs={'class': "form-control"}
                                 )
                                 )

    Punto_Operacion = forms.ModelMultipleChoiceField(
        queryset=PtoOperacion.objects.all(), required=False)

    def clean(self):
        cleaned_data = super(UsuarioForm, self).clean()
        e1 = []
        e2 = []

        if cleaned_data.get('numero_documento') != '' and cleaned_data.get('tipo_identificacion') != '':
            datos = {'numero_documento': cleaned_data.get('numero_documento'),
                     'tipo_identificacion__nombre': str(cleaned_data.get('tipo_identificacion'))}

            if Usuario.objects.filter(**datos).count() > 0:
                e1 = "Error en su documento"

        if cleaned_data.get('ConfirmarEmail') != cleaned_data.get('email'):
            e2 = 'Confirmación de correo errada'

        if e1 or e2:
            error_list = [e1, e2]
            raise forms.ValidationError(error_list)

    class Meta:
        model = Usuario
        fields = [
            'rol',
            'estado',
            'imagen',
            'first_name',
            'last_name',
            'tipo_identificacion',
            'numero_documento',
            'fecha_nacimiento',
            'email',
            'ConfirmarEmail',
            'tipo_via',
            'numero_via',
            'prefijo_via',
            'bis_via',
            'cuadrante_via',
            'numero_via_generadora',
            'prefijo_via_generadora',
            'bis_via_generadora',
            'numero_placa',
            'cuadrante_via_generadora',
            'complemento',
            'barrio',
            'pais',
            'departamento',
            'ciudad',
            'telefono',
            'extencion',
            'imagen',
            'asesor',
            'regional',
        ]

        widgets = {
            'first_name': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "Nombres", 'required': 'required'}),
            'last_name': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "Apellidos", 'required': 'required'}),
            'tipo_identificacion': forms.Select(attrs={'class': 'form-control', 'required': "required"}),
            'numero_documento': forms.TextInput(attrs={'class': "form-control", 'placeholder': "numero_documento"}),
            'telefono': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "Telefono", 'required': "required"}),
            'fecha_nacimiento': forms.DateInput(
                attrs={'class': "form-control", 'placeholder': "fecha_nacimiento", 'required': 'required'}),
            'email': forms.TextInput(attrs={'class': "form-control", 'placeholder': "email", 'required': 'required'}),
            'ConfirmarEmail': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "ConfirmarEmail", 'required': 'required'}),
            'tipo_via': forms.Select(attrs={'class': "form-control"}),
            'numero_via': forms.NumberInput(attrs={'class': "form-control", 'placeholder': 'Numero', 'min': "1"}),
            'prefijo_via': forms.Select(attrs={'class': "form-control"}),
            'bis_via': forms.CheckboxInput(),
            'cuadrante_via': forms.Select(attrs={'class': "form-control"}),
            'numero_via_generadora': forms.NumberInput(attrs={'class': "form-control", 'min': "1"}),
            'prefijo_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'bis_via_generadora': forms.CheckboxInput(),
            'numero_placa': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Placa'}),
            'cuadrante_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'complemento': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Complemento'}),
            'barrio': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Barrio'}),
            'contacto': forms.TextInput(attrs={'class': "form-control", 'placeholder': "contacto"}),
            'extencion': forms.TextInput(attrs={'class': "form-control", 'placeholder': "extencion"}),
            'pais': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'departamento': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'ciudad': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'asesor': forms.Select(attrs={'class': "form-control"}),
            'Punto_Operacion': forms.SelectMultiple(),
        }

    def __init__(self, *args, **kwargs):
        login_user = kwargs.pop('login_user')
        super().__init__(*args, **kwargs)

        # clientelogin = login_user.cliente_set.all()[0]
        clientelogin = login_user.cliente_set.first()
        choices = [("", "Seleccione Opcion")]
        choices2 = [("", "Seleccione Opcion")]

        for pt in Regional.objects.filter(cliente=clientelogin).order_by("regional"):
            choices.append([pt.id, pt])

        for pt in Usuario.objects.filter(is_asesor=True, cliente=clientelogin).order_by("first_name"):
            concatenado = ("(" + str(pt.codigo) + ")  " + str(pt.first_name) + "  " + str(pt.last_name)).upper()
            choices2.append([pt.id, concatenado])

        self.fields['pais'].empty_label = "Seleccione Una Opción"
        self.fields['ciudad'].empty_label = "Seleccione Una Opción"
        self.fields['departamento'].empty_label = "Seleccione Una Opción"
        self.fields['tipo_identificacion'].empty_label = "Seleccione Una Opción"
        self.fields['regional'].choices = choices
        self.fields['asesor'].choices = choices2


class UsuarioUpdateForm(forms.ModelForm):
    regional = forms.ChoiceField(choices=[["", ""]], required=False,
                                 widget=forms.Select(
                                     attrs={'class': "form-control"}
                                 )
                                 )

    Punto_Operacion = forms.ModelMultipleChoiceField(
        queryset=PtoOperacion.objects.all(), required=False)

    def clean(self):
        cleaned_data = super(UsuarioUpdateForm, self).clean()
        e1 = []
        e2 = []

        if cleaned_data.get('numero_documento') != '' and cleaned_data.get('tipo_identificacion') != '':
            datos = {'numero_documento': cleaned_data.get('numero_documento'),
                     'tipo_identificacion__nombre': str(cleaned_data.get('tipo_identificacion'))}

            if Usuario.objects.filter(**datos).exclude(pk=self.instance.pk).count() > 0:
                e1 = "Error en su documento"

        if cleaned_data.get('ConfirmarEmail') != cleaned_data.get('email'):
            e2 = 'Confirmación de correo errada'

        if e1 or e2:
            error_list = [e1, e2]
            raise forms.ValidationError(error_list)

    class Meta:
        model = Usuario
        fields = [

            'estado',
            'imagen',
            'first_name',
            'last_name',
            'tipo_identificacion',
            'numero_documento',
            'fecha_nacimiento',
            'tipo_via',
            'numero_via',
            'prefijo_via',
            'bis_via',
            'cuadrante_via',
            'numero_via_generadora',
            'prefijo_via_generadora',
            'bis_via_generadora',
            'numero_placa',
            'cuadrante_via_generadora',
            'complemento',
            'barrio',
            'pais',
            'departamento',
            'ciudad',
            'telefono',
            'extencion',
            'imagen',
            'asesor',
            'regional',
        ]

        widgets = {
            'first_name': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "Nombres", 'required': 'required'}),
            'last_name': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "Apellidos", 'required': 'required'}),
            'tipo_identificacion': forms.Select(attrs={'class': 'form-control', 'required': "required"}),
            'numero_documento': forms.TextInput(attrs={'class': "form-control", 'placeholder': "numero_documento"}),
            'telefono': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "Telefono", 'required': "required"}),
            'fecha_nacimiento': forms.DateInput(
                attrs={'class': "form-control", 'placeholder': "fecha_nacimiento", 'required': 'required'}),
            'email': forms.TextInput(attrs={'class': "form-control", 'placeholder': "email", 'required': 'required'}),
            'tipo_via': forms.Select(attrs={'class': "form-control"}),
            'numero_via': forms.NumberInput(attrs={'class': "form-control", 'placeholder': 'Numero', 'min': "1"}),
            'prefijo_via': forms.Select(attrs={'class': "form-control"}),
            'bis_via': forms.CheckboxInput(),
            'cuadrante_via': forms.Select(attrs={'class': "form-control"}),
            'numero_via_generadora': forms.NumberInput(attrs={'class': "form-control", 'min': "1"}),
            'prefijo_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'bis_via_generadora': forms.CheckboxInput(),
            'numero_placa': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Placa'}),
            'cuadrante_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'complemento': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Complemento'}),
            'barrio': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Barrio'}),
            'contacto': forms.TextInput(
                attrs={'class': "form-control", 'placeholder': "contacto", 'min': '7', 'max': '10'}),
            'extencion': forms.TextInput(attrs={'class': "form-control", 'placeholder': "extencion"}),
            'pais': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'departamento': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'ciudad': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'asesor': forms.Select(attrs={'class': "form-control"}),
            'Punto_Operacion': forms.SelectMultiple(),
        }

    def __init__(self, *args, **kwargs):
        login_user = kwargs.pop('login_user')
        super().__init__(*args, **kwargs)
        try:
            clientelogin = login_user.cliente_set.all()[0]
            # print(login_user.cliente_set.all()[0])
        except:
            # print('excpetion SE ROMPE ? ')
            clientelogin = 'clienteN'
            pass

        choices = [("", "Seleccione Opcion")]
        choices2 = [("", "Seleccione Opcion")]

        for pt in Regional.objects.filter(cliente=clientelogin).order_by("regional"):
            choices.append([pt.id, pt])

        for pt in Usuario.objects.filter(is_asesor=True, cliente=clientelogin).order_by("first_name"):
            concatenado = ("(" + str(pt.codigo) + ")  " + str(pt.first_name) + "  " + str(pt.last_name)).upper()
            choices2.append([pt.id, concatenado])

        self.fields['regional'].choices = choices
        self.fields['asesor'].choices = choices2


class PasswordResetForm2(forms.Form):
    """ Custom Pasword reset, mod from PasswordResetForm from django"""
    email = forms.EmailField(label="Email", max_length=254)

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        subject = render_to_string(subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = render_to_string(email_template_name, context)

        email_message = EmailMessage(subject, body, from_email, [to_email])
        if html_email_template_name is not None:
            html_email = render_to_string(html_email_template_name, context)
            email_message = EmailMessage(subject, html_email, from_email, [to_email])
            email_message.content_subtype = "html"
            email_message.mixed_subtype = "related"

            img_data_1 = open("static/img/fondo-cumple.png", 'rb').read()
            img_data_2 = open("static/img/logo-tn.png", 'rb').read()
            img_data_3 = open("static/img/logo-rp.png", 'rb').read()

            imgs_data_list = [img_data_1, img_data_2, img_data_3]
            url_list = ['/static/img/fondo-cumple.png', '/static/img/logo-tn.png', '/static/img/logo-rp.png']
            imgs_dict = dict(zip(imgs_data_list, url_list))

            for img_data in imgs_dict:
                image = MIMEImage(img_data, 'png')
                image.add_header('Content-ID', '<{}>'.format(imgs_dict[img_data]))
                image.add_header("Content-Disposition", "inline", filename=imgs_dict[img_data])
                email_message.attach(image)
        email_message.send()



    def get_users(self, email):
        """Given an email, return matching user(s) who should receive a reset.

        This allows subclasses to more easily customize the default policies
        that prevent inactive users and users with unusable passwords from
        resetting their password.
        """
        active_users = Usuario._default_manager.filter(**{
            '%s__iexact' % Usuario.get_email_field_name(): email,
            'is_active': True,
        })
        return (u for u in active_users if u.has_usable_password())

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):
        """
        Generate a one-use only link for resetting password and send it to the
        user.
        """
        email = self.cleaned_data["email"]
        for user in self.get_users(email):
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            context = {
                'email': email,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
                **(extra_email_context or {}),
            }
            self.send_mail(
                subject_template_name, email_template_name, context, from_email,
                email, html_email_template_name=html_email_template_name,
            )


