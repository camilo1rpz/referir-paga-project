# Generated by Django 2.2 on 2019-05-10 16:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0012_auto_20190510_1058'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='fecha_nacimiento',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
