from django.db import models
from django.contrib.auth.models import AbstractUser
from apps.clientes.models import Pais, Departamento, Ciudad
from .models import *
from string import ascii_uppercase, ascii_lowercase
import random
import string

TIPOS_VIA = (
    ('CARRERA', 'CARRERA'),
    ('CALLE', 'CALLE'),
    ('DIAGONAL', 'DIAGONAL'),
    ('TRANSVERSAL', 'TRANSVERSAL'),
)

PREFIJO_VIA = [('', 'Prefijo')]
for x in string.ascii_uppercase:
    PREFIJO_VIA.append((x, x))

PUNTO_CARDINAL = (
    ('', 'Orientacion'),
    ('SUR', 'SUR'),
    ('NORTE', 'NORTE'),
    ('ESTE', 'ESTE'),
    ('OESTE', 'OESTE'),
    ('SURESTE', 'SURESTE'),
    ('SUROESTE', 'SUROESTE'),
    ('NORESTE', 'NORESTE'),
    ('NOROESTE', 'NOROESTE')
)


class TipoIdentificacion(models.Model):
    nombre = models.CharField(max_length=50)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta():
        verbose_name_plural = "TipoIdentificaciones"


class Usuario(AbstractUser):
    estado = models.BooleanField(default=True)
    eliminado = models.BooleanField(default=False)
    codigo = models.CharField(max_length=4, null=True, blank=True)
    fecha_nacimiento = models.DateField(null=True, blank=True)
    is_super_administrador = models.BooleanField(default=False)
    is_administrador = models.BooleanField(default=False)
    is_administrador_punto = models.BooleanField(default=False)
    is_responsable_campana = models.BooleanField(default=False)
    is_asesor = models.BooleanField(default=False)
    is_persona_natural = models.BooleanField(default=False)
    serial = models.CharField(max_length=50, null=True, blank=True)
    modelo = models.CharField(max_length=50, null=True, blank=True)
    fabricante = models.CharField(max_length=100, null=True, blank=True)
    plataforma = models.CharField(max_length=100, null=True, blank=True)
    version_plataforma = models.CharField(max_length=100, null=True, blank=True)
    md5 = models.CharField(max_length=50, blank=True)
    imagen = models.ImageField(upload_to='imagen-cliente/', default='imagen-cliente/user2.svg', null=True, blank=True)
    numero_documento = models.CharField(max_length=200)
    asesor = models.ForeignKey('self', related_name='fk_asesor', on_delete=models.SET_NULL, null=True, blank=True)
    padre = models.ForeignKey('self', related_name='fk_padre', on_delete=models.SET_NULL, null=True, blank=True)
    asesor_to_responsable_campana = models.ForeignKey('self', related_name='asesor_responsable_campana',
                                                      on_delete=models.SET_NULL, null=True, blank=True)
    tipo_identificacion = models.ForeignKey(TipoIdentificacion, on_delete=models.SET_NULL, null=True, blank=True)

    telefono = models.CharField(max_length=10, null=True, blank=True, )
    extencion = models.CharField(max_length=6, null=True, blank=True, )

    numero_via = models.IntegerField(null=True, blank=True)
    tipo_via = models.CharField(max_length=12, null=True, blank=True, choices=TIPOS_VIA, )
    prefijo_via = models.CharField(max_length=12, null=True, blank=True, choices=PREFIJO_VIA, )
    bis_via = models.BooleanField(default=False, blank=True)
    cuadrante_via = models.CharField(max_length=12, null=True, blank=True, choices=PUNTO_CARDINAL, )
    numero_via_generadora = models.IntegerField(null=True, blank=True)
    prefijo_via_generadora = models.CharField(max_length=12, null=True, blank=True, choices=PREFIJO_VIA, )
    bis_via_generadora = models.BooleanField(default=False, blank=True)
    numero_placa = models.CharField(max_length=4, null=True, blank=True)
    cuadrante_via_generadora = models.CharField(max_length=12, null=True, blank=True, choices=PUNTO_CARDINAL, )
    complemento = models.CharField(max_length=512, null=True, blank=True, )
    barrio = models.CharField(max_length=512, null=True, blank=True, )

    pais = models.ForeignKey(Pais, on_delete=models.SET_NULL, null=True, blank=True)
    departamento = models.ForeignKey(Departamento, on_delete=models.SET_NULL, null=True, blank=True)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.SET_NULL, null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)
    empresa_idCliente = models.CharField(max_length=10, null=True, blank=True)

    # Flags
    email_sent = models.BooleanField(auto_created=True, default=False)
    first_login = models.BooleanField(auto_created=True, default=True)