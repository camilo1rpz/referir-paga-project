from django.contrib import admin
from .models import TipoIdentificacion, Usuario
# Register your models here.

modelos= [
	Usuario,
	TipoIdentificacion
]

admin.site.register(modelos)