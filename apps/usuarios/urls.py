"""Referir_Paga URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from . import views

app_name = "usuarios"
urlpatterns = [
    path('nuevo/', views.CrearUsuario.as_view(), name="usuario-nuevo"),
    path('', views.ListarUsuario.as_view(), name="usuario-index"),
    path('consultar/<int:pk>/<str:delete>/', views.ConsultarUsuario.as_view(), name="usuario-consultar"),
    path('eliminar/', views.EliminarUsuario.as_view(), name="usuario-eliminar"),
    path('actualizar/<int:pk>/', views.ActualizarUsuario.as_view(), name="usuario-actualizar"),
    path('crudfail/', views.crud_fail, name="usuario-crudfail"),
    # path('desactivar/<int:pk>/',views.DesactivarClientes, name="cliente-desactivar"),
    # path('activar/<int:pk>/',views.ActivarClientes, name="cliente-activar"),
    # path('get-departamento/',views.departamento, name="get-departamento"),
    # path('get-ciudad/',views.ciudad, name="get-ciudad"),

]
