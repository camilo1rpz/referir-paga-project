"""Referir_Paga URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, include
from apps.sedes import views

app_name="sedes"
urlpatterns = [
	path('get_pto/',views.Get_pto, name="pto-get"),
	path('',views.ListarRegional.as_view(), name="regional-index"),
	path('nuevo/',views.CrearRegional.as_view(), name="regional-nuevo"),
    path('actualizar/<int:pk>/',views.ActualizarRegional, name="regional-actualizar"),
    path('consultar/<int:pk>/<str:delete>/',views.ConsultaRegional.as_view(),name="regional-consultar"),
    path('eliminar/',views.EliminarRegional.as_view(), name="regional-eliminar"),
    #PtOperacion
    path('nuevo-pto/',views.CrearPuntoOperacion.as_view(), name="pto-nuevo"),
    path('listar-pto/',views.ListarPuntoOperacion.as_view(), name="pto-index"),
    path('actualizar-pto/<int:pk>/',views.ActualizarPuntoOperacion.as_view(), name="pto-actualizar"),
    path('consultar-pto/<int:pk>/<str:delete>/',views.ConsultaPto.as_view(),name="pto-consultar"),
    path('eliminar-pto/',views.EliminarPto.as_view(), name="pto-eliminar"),
]
