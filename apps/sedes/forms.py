from django import forms
from .models import Regional, PtoOperacion


class RegionalForm(forms.ModelForm):
	class Meta:
		model = Regional
		
		fields = [
			'regional',
			'descripcion',
            'estado',
		]

		widgets = {
            'regional': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Regional", 'required': 'required'}),
            'descripcion': forms.Textarea(attrs={'class': "form-control", 'placeholder': "Descripcion", 'required': 'required'}),
        }



class RegionalUpdateForm(forms.ModelForm):
    class Meta:
        model = Regional
        
        fields = [
            'estado',
            'regional',
            'descripcion',
        ]

        widgets = {
            'regional': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Regional", 'required': 'required'}),
            'descripcion': forms.Textarea(attrs={'class': "form-control", 'placeholder': "Descripcion", 'required': 'required'}),
        }

#PtOperacion
class PtOperacionForm(forms.ModelForm):
    class Meta:
        model = PtoOperacion
        
        fields = [
            'estado',
            'nombre',
            'regional',
            'imagen',
            'telefono1',
            'extencion1',
            'telefono2',
            'extencion2',
            'numero_via',
            'tipo_via',
            'prefijo_via',
            'bis_via',
            'cuadrante_via',
            'numero_via_generadora',
            'prefijo_via_generadora',
            'bis_via_generadora',
            'numero_placa',
            'cuadrante_via_generadora',
            'complemento',
            'barrio',
            'pais',
            'departamento',
            'ciudad',
            #'usuarios',
            #'fecha_creacion',
            #'fecha_actualizacion',
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Nombre", 'required': 'required'}),
            'extencion1': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Extencion1", }),
            'telefono1': forms.TextInput(attrs={'minlength':7, 'class': "form-control", 'placeholder': "Telefono1", 'required': 'required'}),
            'telefono2': forms.TextInput(attrs={'minlength':7, 'class': "form-control", 'placeholder': "Telefono2"}),
            'extencion2': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Extencion2" }),
            'tipo_via': forms.Select(attrs={'class': "form-control"}),
            'regional': forms.Select(attrs={'class': "form-control"}),
            'numero_via': forms.NumberInput(attrs={'class': "form-control", 'placeholder': 'Numero', 'min': "1"}),
            'prefijo_via': forms.Select(attrs={'class': "form-control"}),
            'bis_via': forms.CheckboxInput(),
            'cuadrante_via': forms.Select(attrs={'class': "form-control"}),
            'numero_via_generadora': forms.NumberInput(attrs={'class': "form-control", 'min': "1"}),
            'prefijo_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'bis_via_generadora': forms.CheckboxInput(),
            'numero_placa': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Placa'}),
            'cuadrante_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'complemento': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Complemento'}),
            'barrio': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Barrio'}),
            'pais': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'departamento': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'ciudad': forms.Select(attrs={'class': "form-control", 'required': "required"}),

        }


    def __init__(self, *args, **kwargs):
        login_user = kwargs.pop('login_user')
        super().__init__(*args, **kwargs)

        #clientelogin = login_user.cliente_set.all()[0]
        clientelogin = login_user.cliente_set.first()
        choices = [("", "Seleccione Regional")]

        for pt in Regional.objects.filter(cliente=clientelogin,estado=True ).order_by("regional"):
            choices.append([pt.id, pt])

        self.fields['regional'].choices=choices
        self.fields['pais'].empty_label = "Seleccione Una Opción"
        self.fields['ciudad'].empty_label = "Seleccione Una Opción"
        self.fields['departamento'].empty_label = "Seleccione Una Opción"

