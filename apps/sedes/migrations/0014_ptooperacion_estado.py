# Generated by Django 2.2 on 2019-06-10 17:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sedes', '0013_auto_20190610_1043'),
    ]

    operations = [
        migrations.AddField(
            model_name='ptooperacion',
            name='estado',
            field=models.BooleanField(default=True),
        ),
    ]
