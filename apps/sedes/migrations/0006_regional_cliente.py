# Generated by Django 2.2 on 2019-05-15 22:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clientes', '0029_auto_20190514_0947'),
        ('sedes', '0005_remove_regional_cliente'),
    ]

    operations = [
        migrations.AddField(
            model_name='regional',
            name='cliente',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='clientes.Cliente'),
        ),
    ]
