# Generated by Django 2.2 on 2019-06-04 16:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sedes', '0011_regional_estado'),
    ]

    operations = [
        migrations.AddField(
            model_name='regional',
            name='descripcion',
            field=models.TextField(null=True),
        ),
    ]
