# Generated by Django 2.2 on 2019-05-21 13:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sedes', '0008_auto_20190515_1733'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ptooperacion',
            name='ciudad',
        ),
        migrations.AlterField(
            model_name='ptooperacion',
            name='regional',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='sedes.Regional'),
        ),
    ]
