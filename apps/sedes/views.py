import logging

from django.conf import settings
from django.contrib import messages
from django.views.generic import ListView, CreateView, DetailView, DeleteView, UpdateView
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect

from apps.clientes.models import Cliente
from .forms import RegionalForm, RegionalUpdateForm, PtOperacionForm
from .models import PtoOperacion, Regional
from django.urls import reverse_lazy
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core import serializers
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
import json

loggerRegion = logging.getLogger('regional_log')
loggerPto = logging.getLogger('pto_ope_log')


# Create your views here.
@login_required
def Get_pto(request):
    pto = []
    if request.is_ajax() or request.GET:
        id = request.GET.get("id")
        pto = PtoOperacion.objects.filter(regional=id).order_by('nombre')
    pto = serializers.serialize('json', pto)

    return HttpResponse(json.dumps(pto), content_type='application/json')


class CrearRegional(LoginRequiredMixin, CreateView):
    """docstring for CrearRegional"""
    model = Regional
    template_name = 'regional/nuevo.html'
    success_url = reverse_lazy('sedes:regional-index')
    form_class = RegionalForm

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        model = form.save(commit=False)
        model.cliente_id = self.request.user.cliente_set.all()[0].pk
        model.save()
        loggerRegion.info('Regional: {} ha sido AGREGADO por usuario con email: {}'.
                          format(model.regional, Cliente.objects.get(id=self.cliente_id).email))
        return super().form_valid(form)

    def __init__(self):
        super(CrearRegional, self).__init__()


class ListarRegional(LoginRequiredMixin, ListView):
    """docstring for ListarRegional"""
    model = Regional
    template_name = 'regional/index.html'
    paginate_by = 50

    def get(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Cliente.objects.get(id=as_user['as_usuario'])
        else:
            cliente = request.user.cliente_set.all()[0]
        search = request.GET.get('q')
        if search:
            filtro = self.model.objects.filter(Q(cliente=cliente) &
                                               (
                                                       Q(regional__icontains=search) |
                                                       Q(descripcion__icontains=search)
                                               )
                                               ).order_by('regional')

            self.object_list = filtro
        else:
            self.object_list = self.model.objects.filter(cliente=cliente).order_by('regional')

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        return self.render_to_response(context)

    def __init__(self):
        super(ListarRegional, self).__init__()


class ConsultaRegional(LoginRequiredMixin, DetailView):
    model = Regional
    template_name = 'regional/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'Eliminar' in request.get_full_path():
            context = self.get_context_data(object=self.object, eliminar=True)
        return self.render_to_response(context)


class EliminarRegional(LoginRequiredMixin, DeleteView):
    model = Regional
    model2 = PtoOperacion

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id

        borrar = request.POST.get('id')
        valores = self.model2.objects.filter(regional=borrar)
        regional = self.model.objects.get(pk=borrar)

        if not valores:
            regional.delete()
            loggerRegion.info('Regional: {}, ha sido ELIMINADA por usuario con email: {}'.
                              format(regional.regional, Cliente.objects.get(id=self.cliente_id).email))
            dato = 1
        else:
            loggerRegion.error('Regional: {}, NO PUDO SER ELIMINADO por usuario con email: {}. Existen PTOs de '
                               'operaciones asociados a esta regional'.
                               format(regional.regional, Cliente.objects.get(id=self.cliente_id).email))
            dato = 0

        return HttpResponse(json.dumps(dato), content_type='application/json')


@login_required
def ActualizarRegional(request, pk):
    regional = Regional.objects.get(pk=pk)

    if request.method == 'GET':
        form = RegionalUpdateForm(instance=regional)
    else:
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente_id = as_user['as_usuario']
        else:
            cliente_id = request.user.cliente_set.all()[0].id

        form = RegionalUpdateForm(request.POST, instance=regional)
        if form.is_valid():
            if "estado" in form.changed_data:
                try:
                    estado = form.data['estado']
                except:
                    if PtoOperacion.objects.filter(regional=regional):
                        loggerRegion.error(
                            'Regional: {}, NO PUDO SER DESACTIVADA por usuario con email: {}. Existen PTOs de '
                            'operaciones asociados a esta regional'.
                                format(regional.regional, Cliente.objects.get(id=cliente_id).email))
                        messages.info(request, 'No puede inactivar, existen puntos de operacion activos')
                        form.save(commit=False)
                        return render(request, 'regional/nuevo.html', {'form': form, })
            form.save()
            loggerRegion.info('Regional: {}, ha sido MODIFICADA por usuario con email: {}'.
                              format(regional.regional, Cliente.objects.get(id=cliente_id).email))
            return redirect('sedes:regional-index')
    return render(request, 'regional/nuevo.html', {'form': form, })


# Punto Operación
class CrearPuntoOperacion(LoginRequiredMixin, CreateView):
    """docstring for CrearRegional"""
    model = PtoOperacion
    template_name = 'ptooperacion/nuevo.html'
    success_url = reverse_lazy('sedes:pto-index')
    form_class = PtOperacionForm

    def get_form_kwargs(self):
        kwargs = super(CrearPuntoOperacion, self).get_form_kwargs()
        kwargs.update({'login_user': self.request.user})
        return kwargs

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        model = form.save(commit=False)
        cliente_id = self.request.user.cliente_set.all()[0].pk
        model.cliente_id = cliente_id
        regional = form.data['regional']
        nombre = form.data['nombre']
        print(dir(regional))

        if self.model.objects.filter(cliente=cliente_id, nombre=nombre, regional=regional):
            print("ok")
            print(form.data['nombre'])
            print(cliente_id)
            print(form.data['regional'])

            print(self.model.objects.filter(Q(cliente=cliente_id) and Q(nombre=form.data['nombre']) and
                                            Q(regional=form.data['regional'])))
            messages.info(self.request, 'No puede Crear punto de operacion ya existe en esta regional')
            loggerPto.error('Punto de operaciones: {}, NO HA PODIDO SER AGREGADO por usuario con email: {}.'
                            ' Pto de operaciones con el mismo nombre ya existe en esta regional'.
                            format(model.nombre, Cliente.objects.get(id=cliente_id).email))
            return self.render_to_response(self.get_context_data(form=form))
        model.save()
        loggerPto.info('Punto de operaciones: {}, ha sido AGREGADO por usuario con email: {}'.
                       format(model.nombre, Cliente.objects.get(id=cliente_id).email))
        return super().form_valid(form)


class ListarPuntoOperacion(LoginRequiredMixin, ListView):
    """docstring for ListarPuntoOperacion"""
    model = PtoOperacion
    template_name = 'ptooperacion/index.html'
    paginate_by = 50

    def get(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Cliente.objects.get(id=as_user['as_usuario'])
        else:
            cliente = request.user.cliente_set.all()[0]
        search = request.GET.get('q')

        if search:
            pass
            filtro = self.model.objects.filter(Q(cliente=cliente) &
                                               (
                                                       Q(nombre__icontains=search) |
                                                       Q(regional__regional__icontains=search) |
                                                       Q(telefono1__icontains=search) |
                                                       Q(extencion1__icontains=search) |
                                                       Q(telefono2__icontains=search) |
                                                       Q(extencion2__icontains=search) |
                                                       Q(complemento__icontains=search) |
                                                       Q(barrio__icontains=search) |
                                                       Q(ciudad__nombre__icontains=search) |
                                                       Q(departamento__nombre__icontains=search) |
                                                       Q(numero_via__icontains=search) |
                                                       Q(tipo_via__icontains=search) |
                                                       Q(prefijo_via__icontains=search) |
                                                       Q(cuadrante_via__icontains=search) |
                                                       Q(numero_via_generadora__icontains=search) |
                                                       Q(prefijo_via_generadora__icontains=search) |
                                                       Q(numero_placa__icontains=search) |
                                                       Q(cuadrante_via_generadora__icontains=search) |
                                                       Q(pais__nombre__icontains=search)

                                               )
                                               ).order_by('nombre')

            self.object_list = filtro
        else:
            self.object_list = self.model.objects.filter(cliente=cliente).order_by('nombre')

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        return self.render_to_response(context)

    def __init__(self):
        super(ListarPuntoOperacion, self).__init__()


class ActualizarPuntoOperacion(LoginRequiredMixin, UpdateView):
    """docstring for updatePuntoOperacion"""
    model = PtoOperacion
    template_name = 'ptooperacion/nuevo.html'
    success_url = reverse_lazy('sedes:pto-index')
    form_class = PtOperacionForm

    def get_form_kwargs(self):
        kwargs = super(ActualizarPuntoOperacion, self).get_form_kwargs()
        kwargs.update({'login_user': self.request.user})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.pk = kwargs['pk']
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        img = self.model.objects.get(pk=self.pk).imagen
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()
            kwargs['img'] = str(img)
            return super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id

    def form_valid(self, form):
        loggerPto.info('Punto de operaciones: {}, ha sido MODIFICADO por usuario con email: {}'.
                       format(self.model.nombre, Cliente.objects.get(id=self.cliente_id).email))

    def __init__(self, ):
        super(ActualizarPuntoOperacion, self).__init__()


class ConsultaPto(LoginRequiredMixin, DetailView):
    model = PtoOperacion
    template_name = 'ptooperacion/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'Eliminar' in request.get_full_path():
            context = self.get_context_data(object=self.object, eliminar=True)
        return self.render_to_response(context)


class EliminarPto(LoginRequiredMixin, DeleteView):
    model = PtoOperacion

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id

        borrar = request.POST.get('id')
        valores = self.model.objects.get(pk=borrar).usuarios.count()
        pto_ope = self.model.objects.get(pk=borrar)
        if not valores:
            pto_ope.delete()
            loggerPto.info('Punto de operaciones: {}, ha sido ELIMINADO por usuario con email: {}'.
                           format(pto_ope.nombre, Cliente.objects.get(id=self.cliente_id).email))
            dato = 1
        else:
            dato = 0
            loggerPto.error('Punto de operaciones: {}, NO HA PODIDO SER ELIMINADO por usuario con email: {}. '
                            'Existen usuarios asociados a este punto de operaciones'.
                            format(pto_ope.nombre, Cliente.objects.get(id=self.cliente_id).email))

        return HttpResponse(json.dumps(dato), content_type='application/json')
