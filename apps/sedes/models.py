from django.db import models
from django.conf import settings
from apps.clientes.models import *
from apps.ubicacion.models import *
from apps.usuarios.models import TIPOS_VIA, PREFIJO_VIA, PUNTO_CARDINAL


class Regional(models.Model):
	estado = models.BooleanField(default=True)
	regional = models.CharField(max_length=250,blank=True)
	cliente = models.ForeignKey(Cliente,on_delete=models.SET_NULL, null=True)#De 1 a *
	descripcion = models.TextField(null=True)
	fecha_creacion = models.DateTimeField(auto_now_add=True)
	fecha_actualizacion = models.DateTimeField(auto_now=True)

	def __str__(self):
		return '{0}'.format((self.regional).upper())

	class Meta():
		verbose_name_plural = "Regionales"


class PtoOperacion(models.Model):
	cliente = models.ForeignKey(Cliente,on_delete=models.SET_NULL, null=True)
	estado = models.BooleanField(default=True)
	nombre = models.CharField(max_length=250)
	regional = models.ForeignKey(Regional, on_delete=models.SET_NULL, null=True)#ForeignKey 1 a m
	imagen = models.ImageField(upload_to = 'imagen-ptoperacion/', default = 'imagen-cliente/user2.svg', null=True, blank=True)
	telefono1 = models.CharField(max_length=10, null=True , blank=True, )
	extencion1 = models.CharField(max_length=6, null=True , blank=True, )
	telefono2 = models.CharField(max_length=10, null=True , blank=True, )
	extencion2 = models.CharField(max_length=6, null=True , blank=True, )
	numero_via = models.IntegerField( null=True , blank=True)
	tipo_via = models.CharField(max_length=12, null=True , blank=True, choices=TIPOS_VIA,)
	prefijo_via = models.CharField(max_length=12, null=True , blank=True, choices=PREFIJO_VIA,)
	bis_via = models.BooleanField(default=False, blank=True)
	cuadrante_via = models.CharField(max_length=12, null=True , blank=True, choices=PUNTO_CARDINAL,)
	numero_via_generadora = models.IntegerField( null=True , blank=True)
	prefijo_via_generadora = models.CharField(max_length=12, null=True , blank=True, choices=PREFIJO_VIA,)
	bis_via_generadora = models.BooleanField(default=False, blank=True)
	numero_placa = models.CharField(max_length=4, null=True , blank=True)
	cuadrante_via_generadora = models.CharField(max_length=12, null=True , blank=True, choices=PUNTO_CARDINAL,)
	complemento = models.CharField(max_length=512, null=True , blank=True,)
	barrio = models.CharField(max_length=512, null=True , blank=True,)
	pais = models.ForeignKey(Pais, on_delete=models.SET_NULL, null=True,blank=True) 
	departamento = models.ForeignKey(Departamento, on_delete=models.SET_NULL, null=True, blank=True) 
	ciudad = models.ForeignKey(Ciudad, on_delete=models.SET_NULL, null=True, blank=True) 
	usuarios = models.ManyToManyField(settings.AUTH_USER_MODEL)
	fecha_creacion = models.DateTimeField(auto_now_add=True)
	fecha_actualizacion = models.DateTimeField(auto_now=True)

	def __str__(self):
		nombre=(self.nombre).upper()
		return '{0}'.format(nombre)   

	class Meta():
		verbose_name_plural = "PtoOperaciones"