"""Referir_Paga URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from . import views

app_name = "clientes"
urlpatterns = [
    path('', views.ListarClientes.as_view(), name="cliente-index"),
    # path('eliminar/<int:pk>/',views.EliminarClientes, name="cliente-eliminar"),
    path('delete-cliente', views.DeleteCliente.as_view(), name="cliente-delete"),
    path('desactivar/<int:pk>/', views.DesactivarClientes, name="cliente-desactivar"),
    path('activar/<int:pk>/', views.ActivarClientes, name="cliente-activar"),
    path('nuevo/', views.CrearCliente.as_view(), name="cliente-nuevo"),
    path('actualizar/<int:pk>/', views.ActualizarCliente, name="cliente-actualizar"),
    path('get-identificacion/', views.identificacion, name="get-identificacion"),
    path('get-departamento/', views.departamento, name="get-departamento"),
    path('get-ciudad/', views.ciudad, name="get-ciudad"),
    path('consultar/<int:pk>/<str:delete>/', views.ConsultaCliente.as_view(), name="cliente-consultar"),
]
