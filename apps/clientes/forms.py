from django import forms
from .models import Cliente
from apps.ubicacion.models import Pais, Departamento, Ciudad
from apps.validators import emailexiste
from apps.usuarios.models import *


class ClienteForm(forms.ModelForm):
    email = forms.EmailField(required=True,
                             validators=[emailexiste],
                             widget=forms.EmailInput(
                                 attrs={'class': "form-control", 'placeholder': 'emailempreas@empresa.em'}
                             )
                             )
    ConfirmarEmail = forms.EmailField(required=True,
                                      widget=forms.EmailInput(
                                          attrs={'class': "form-control", 'placeholder': 'emailempreas@empresa.em'}
                                      )
                                      )

    def clean(self):
        cleaned_data = super(ClienteForm, self).clean()
        e1 = []
        e2 = []
        if cleaned_data.get('identificacion') != '' and cleaned_data.get('tipo_identificacion') != '':
            datos = {'numero_documento': cleaned_data.get('identificacion'),
                     'tipo_identificacion__nombre': str(cleaned_data.get('tipo_identificacion')),
                     'email': cleaned_data.get('email'),
                     }

            if Usuario.objects.filter(**datos).count() > 0:
                e1 = "Error en su documento"

        if cleaned_data.get('ConfirmarEmail') != cleaned_data.get('email'):
            e2 = 'Confirmación de correo errada'

        if e1 or e2:
            error_list = [e1, e2]
            raise forms.ValidationError(error_list)

    class Meta:
        model = Cliente
        fields = [
            'id',
            'nombre_o_razon',
            'apellido',
            'nombre_app',
            'tipo_identificacion',
            'identificacion',
            'telefono',
            'extencion',
            'email',
            'nombre_contacto',
            'pais',
            'departamento',
            'ciudad',
            'tipo_sociedad',
            'tipo_organizacion',
            'regimenfiscal',
            'logo',
            'tipo_via',
            'numero_via',
            'prefijo_via',
            'bis_via',
            'cuadrante_via',
            'numero_via_generadora',
            'prefijo_via_generadora',
            'bis_via_generadora',
            'numero_placa',
            'cuadrante_via_generadora',
            'complemento',
            'barrio',
            'ConfirmarEmail',
            'estado'
        ]

        widgets = {
            'nombre_o_razon': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'nombre_o_razon'}),
            'nombre_app': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'nombre de app', 'minlength': 2}),
            'apellido': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Apellido'}),
            'identificacion': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Identificacion - 000'}),
            'tipo_identificacion': forms.Select(attrs={'class': 'form-control'}),
            'tipo_organizacion': forms.Select(attrs={'class': 'form-control'}),
            'pais': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'departamento': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'ciudad': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'tipo_sociedad': forms.Select(attrs={'class': "form-control"}),
            'regimenfiscal': forms.Select(attrs={'class': "form-control"}),
            'telefono': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Telefono'}),
            'extencion': forms.TextInput(attrs={'class': "form-control", 'placeholder': "extension"}),
            'nombre_contacto': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Nombre contacto'}),
            'tipo_via': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'numero_via': forms.NumberInput(attrs={'class': "form-control", 'placeholder': 'Numero', 'min': "1"}),
            'prefijo_via': forms.Select(attrs={'class': "form-control"}),
            'bis_via': forms.CheckboxInput(),
            'cuadrante_via': forms.Select(attrs={'class': "form-control"}),
            'numero_via_generadora': forms.NumberInput(
                attrs={'class': "form-control", 'min': "1", 'required': "required"}),
            'prefijo_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'bis_via_generadora': forms.CheckboxInput(),
            'numero_placa': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Placa'}),
            'cuadrante_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'complemento': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Complemento'}),
            'barrio': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Barrio'}),
            'estado': forms.CheckboxInput(),

        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nombre_o_razon'].required = True
        self.fields['nombre_app'].required = True
        self.fields['email'].required = True
        self.fields['ConfirmarEmail'].required = True
        self.fields['nombre_app'].required = True
        self.fields['tipo_organizacion'].empty_label = "Seleccione Una Opción"
        self.fields['tipo_identificacion'].empty_label = "Seleccione Una Opción"
        self.fields['pais'].empty_label = "Seleccione Una Opción"
        self.fields['ciudad'].empty_label = "Seleccione Una Opción"
        self.fields['departamento'].empty_label = "Seleccione Una Opción"
        self.fields['tipo_sociedad'].empty_label = "Seleccione Una Opción"
        self.fields['regimenfiscal'].empty_label = "Seleccione Una Opción"


class ClienteUpdateForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = [
            'estado',
            'nombre_o_razon',
            'apellido',
            'nombre_app',
            'tipo_identificacion',
            'identificacion',
            'telefono',
            'extencion',
            'email',
            'nombre_contacto',
            'pais',
            'departamento',
            'ciudad',
            'tipo_sociedad',
            'tipo_organizacion',
            'regimenfiscal',
            'logo',
            'tipo_via',
            'numero_via',
            'prefijo_via',
            'bis_via',
            'cuadrante_via',
            'numero_via_generadora',
            'prefijo_via_generadora',
            'bis_via_generadora',
            'numero_placa',
            'cuadrante_via_generadora',
            'complemento',
            'barrio',

        ]

        widgets = {
            'estado': forms.CheckboxInput(),
            'nombre_o_razon': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'nombre_o_razon'}),
            'apellido': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Apellido'}),
            'nombre_app': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'nombre de app'}),
            'identificacion': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Identificacion - 000'}),
            'tipo_identificacion': forms.Select(attrs={'class': 'form-control'}),
            'tipo_organizacion': forms.Select(attrs={'class': 'form-control'}),
            'pais': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'departamento': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'ciudad': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'tipo_sociedad': forms.Select(attrs={'class': "form-control"}),
            'regimenfiscal': forms.Select(attrs={'class': "form-control"}),
            'telefono': forms.TextInput(attrs={'class': "form-control", 'maxlength': '15', 'placeholder': 'Telefono'}),
            'extencion': forms.TextInput(attrs={'class': "form-control", 'placeholder': "extension"}),
            'nombre_contacto': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Nombre contacto'}),
            'tipo_via': forms.Select(attrs={'class': "form-control"}),
            'numero_via': forms.NumberInput(attrs={'class': "form-control", 'placeholder': 'Numero'}),
            'prefijo_via': forms.Select(attrs={'class': "form-control"}),
            'bis_via': forms.CheckboxInput(),
            'cuadrante_via': forms.Select(attrs={'class': "form-control"}),
            'numero_via_generadora': forms.NumberInput(attrs={'class': "form-control"}),
            'prefijo_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'bis_via_generadora': forms.CheckboxInput(),
            'numero_placa': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Placa'}),
            'cuadrante_via_generadora': forms.Select(attrs={'class': "form-control"}),
            'complemento': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Complemento'}),
            'barrio': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Barrio'}),
            'email': forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Ingrese Un Correo'}),

        }

    # Select Html

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nombre_o_razon'].required = True
        self.fields['email'].required = False
        self.fields['tipo_organizacion'].empty_label = "Seleccione Una Opción"
        self.fields['tipo_identificacion'].empty_label = "Seleccione Una Opción"
        self.fields['pais'].empty_label = "Seleccione Una Opción"
        self.fields['ciudad'].empty_label = "Seleccione Una Opción"
        self.fields['departamento'].empty_label = "Seleccione Una Opción"
        self.fields['tipo_sociedad'].empty_label = "Seleccione Una Opción"
        self.fields['regimenfiscal'].empty_label = "Seleccione Una Opción"
