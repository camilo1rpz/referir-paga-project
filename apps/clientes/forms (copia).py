from django import forms 
from .models import Cliente
from apps.ubicacion.models import Pais, Departamento, Ciudad


class ClienteForm(forms.ModelForm):
	email = forms.EmailField(widget=forms.EmailInput(attrs={'class':"form-control",'placeholder':'emailempreas@empresa.em'}))  
	

	class Meta:
		model = Cliente 
		fields = [
				'nombre_o_razon',
				'apellido',
				'direccion',
				'tipo_identificacion',
				'identificacion',
				'telefono',
				'email',
				'nombre_contacto',
				'pais',
				'departamento',
				'ciudad',
				'tipo_sociedad',
				'tipo_organizacion',
				'regimenfiscal',
				'logo',
		]

		widgets= {
			'nombre_o_razon':forms.TextInput(attrs={'class':"form-control" ,'placeholder':'nombre_o_razon'}),
			'apellido':forms.TextInput(attrs={'class':"form-control" ,'placeholder':'Apellido'}),
			'direccion': forms.TextInput(attrs={'class':'form-control'  ,'placeholder':'Direccion'}),
			'identificacion':forms.TextInput(attrs={'class':'form-control' ,'placeholder':'Identificacion - 000'}),
			'tipo_identificacion':forms.Select(attrs={'class':'form-control'}),
			'tipo_organizacion':forms.Select(attrs={'class':'form-control'}),
			'pais':forms.Select(attrs={'class':"form-control"}),
			'departamento':forms.Select(attrs={'class':"form-control"}),
			'ciudad':forms.Select(attrs={'class':"form-control"}),
			'tipo_sociedad':forms.Select(attrs={'class':"form-control"}),
			'regimenfiscal':forms.Select(attrs={'class':"form-control"}),
			'telefono':forms.TextInput(attrs={'class':"form-control", 'placeholder':'Telefono'}),
			'nombre_contacto':forms.TextInput(attrs={'class':"form-control" ,'placeholder':'Nombre contacto'}),
		}	 

	#Select Html
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['tipo_organizacion'].empty_label = "Seleccione Una Opción"
		self.fields['tipo_identificacion'].empty_label = "Seleccione Una Opción"
		self.fields['pais'].empty_label = "Seleccione Una Opción"
		self.fields['ciudad'].empty_label = "Seleccione Una Opción"
		self.fields['departamento'].empty_label = "Seleccione Una Opción"
		self.fields['tipo_sociedad'].empty_label = "Seleccione Una Opción"
		self.fields['regimenfiscal'].empty_label = "Seleccione Una Opción"
		self.fields['departamento'].queryset = Departamento.objects.none()

