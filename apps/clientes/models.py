from django.core.exceptions import ValidationError

from .models import *
from django.db import models
from django.conf import settings
from apps.ubicacion.models import Ciudad, Pais, Departamento
from apps.usuarios.models import TipoIdentificacion, Usuario
from apps.validators import TIPOS_VIA, PREFIJO_VIA, PUNTO_CARDINAL


# Crudo
class TipoOrganizacion(models.Model):
    nombre = models.CharField(max_length=50)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta():
        verbose_name_plural = "TipoOrganizaciones"


# Crudo
class TipoSociedad(models.Model):
    nombre = models.CharField(max_length=250)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta():
        verbose_name_plural = "TipoSociedades"


# Crudo
class RegimenFiscal(models.Model):
    nombre = models.CharField(max_length=50)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta():
        verbose_name_plural = "RegimenFiscales"


def validate_image(fieldfile_obj):
    filesize = fieldfile_obj.file.size
    megabyte_limit = 1
    if filesize > megabyte_limit * 1024 * 1024:
        raise ValidationError("la imagen es muy grande, maximo %sMB" % str(megabyte_limit))


class Cliente(models.Model):
    estado = models.BooleanField(default=True)
    eliminado = models.BooleanField(default=False)
    nombre_app = models.CharField(max_length=30)
    nombre_o_razon = models.CharField(max_length=25)
    apellido = models.CharField(max_length=25, null=True, blank=True)

    tipo_via = models.CharField(max_length=12, null=True, blank=True, choices=TIPOS_VIA, )
    numero_via = models.IntegerField(null=True, blank=True)
    prefijo_via = models.CharField(max_length=12, null=True, blank=True, choices=PREFIJO_VIA, )
    bis_via = models.BooleanField(default=False, blank=True)
    cuadrante_via = models.CharField(max_length=12, null=True, blank=True, choices=PUNTO_CARDINAL, )
    numero_via_generadora = models.IntegerField(null=True, blank=True)
    prefijo_via_generadora = models.CharField(max_length=12, null=True, blank=True, choices=PREFIJO_VIA, )
    bis_via_generadora = models.BooleanField(default=False, blank=True)
    numero_placa = models.CharField(max_length=4, null=True, blank=True)
    cuadrante_via_generadora = models.CharField(max_length=12, null=True, blank=True, choices=PUNTO_CARDINAL, )
    complemento = models.CharField(max_length=512, null=True, blank=True, )
    barrio = models.CharField(max_length=512, null=True, blank=True, )

    tipo_identificacion = models.ForeignKey(TipoIdentificacion, on_delete=models.SET_NULL, null=True)
    identificacion = models.CharField(max_length=25)
    telefono = models.CharField(max_length=10)
    extencion = models.CharField(max_length=6, null=True, blank=True)
    email = models.EmailField()
    nombre_contacto = models.CharField(max_length=30, null=True, blank=True)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.SET_NULL, null=True, blank=True)
    pais = models.ForeignKey(Pais, on_delete=models.SET_NULL, null=True, blank=True)
    departamento = models.ForeignKey(Departamento, on_delete=models.SET_NULL, null=True, blank=True)
    tipo_sociedad = models.ForeignKey(TipoSociedad, on_delete=models.SET_NULL, null=True, blank=True)
    tipo_organizacion = models.ForeignKey(TipoOrganizacion, on_delete=models.SET_NULL, null=True)
    regimenfiscal = models.ForeignKey(RegimenFiscal, on_delete=models.SET_NULL, null=True, blank=True)
    usuario = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)
    logo = models.ImageField(upload_to='imagen-cliente/', default='imagen-cliente/user2.svg', null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre_o_razon

    class Meta():
        verbose_name_plural = "Clientes"


