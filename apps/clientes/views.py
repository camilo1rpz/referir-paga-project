from email.mime.image import MIMEImage

from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.list import ListView
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.urls import reverse_lazy
from apps.clientes.models import Cliente
from apps.usuarios.models import Usuario, TipoIdentificacion
from apps.sedes.models import Regional, PtoOperacion
from apps.gestores.models import Enlace, Publicidad, Servicio

from apps.ubicacion.models import *
from django.core.cache import cache
from .forms import ClienteForm, ClienteUpdateForm
import json
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from apps.validators import contrasena
from django.core.mail import EmailMessage, send_mail
from django.template.loader import render_to_string
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
import logging

logger = logging.getLogger(__name__)


class ListarClientes(LoginRequiredMixin, ListView):
    model = Cliente
    template_name = 'clientes/index.html'
    queryset = Cliente.objects.filter(eliminado=False).order_by('-estado')
    paginate_by = 50

    def get(self, request, *args, **kwargs):
        search = request.GET.get('q')
        if search:
            clientes = Cliente.objects.filter(Q(eliminado=False) & (
                    Q(nombre_o_razon__icontains=search) |
                    Q(apellido__icontains=search) |
                    Q(tipo_via__icontains=search) |
                    Q(numero_via__icontains=search) |
                    Q(prefijo_via__icontains=search) |
                    Q(cuadrante_via__icontains=search) |
                    Q(prefijo_via_generadora__icontains=search) |
                    Q(numero_placa__icontains=search) |
                    Q(cuadrante_via_generadora__icontains=search) |
                    Q(complemento__icontains=search) |
                    Q(barrio__icontains=search) |
                    Q(tipo_identificacion__nombre__icontains=search) |
                    Q(telefono__icontains=search) |
                    Q(email__icontains=search) |
                    Q(nombre_contacto__icontains=search) |
                    Q(ciudad__nombre__icontains=search) |
                    Q(nombre_app__icontains=search) |
                    Q(tipo_organizacion__nombre__icontains=search) |
                    Q(regimenfiscal__nombre__icontains=search) |
                    Q(tipo_sociedad__nombre__icontains=search)
            )).order_by('-estado')

            self.object_list = clientes
        else:
            self.object_list = self.get_queryset()

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        return self.render_to_response(context)


class DeleteCliente(LoginRequiredMixin, DeleteView):
    def post(self, request, *args, **kwargs):
        model = Usuario
        valores = ''
        reg = ''
        ptOpe = ''
        enl = ''
        publ = ''
        servi = ''
        pk = request.POST.get('idCliente')
        cliente = Cliente.objects.get(pk=pk)
        usu = cliente.usuario.all()
        idUsu = ''
        num = 0
        mailUsu = ''
        for x in usu:
            if num == 0:
                idUsu = x.pk  # Id Usuario
                mailUsu = str(x)  # Email Usuario
                pass
            num = 1
            pass
        usuExistente = Usuario.objects.filter(padre_id=idUsu).count()  # Sin .count() Return Nombre
        region = Regional.objects.filter(cliente_id=pk).count()  # Sin .count() Return Nombre
        pto = PtoOperacion.objects.filter(cliente_id=pk).count()
        enlaces = Enlace.objects.filter(cliente_id=pk).count()
        pub = Publicidad.objects.filter(cliente_id=pk).count()
        serv = Servicio.objects.filter(cliente_id=pk).count()
        if usuExistente > 0:
            valores = 'Usuario'  # Tiene Un Usuario Creado
            pass

        if region > 0:
            reg = 'Regional'  # Tiene una Regional
            pass

        if pto > 0:
            ptOpe = 'PtoOperacion'  # Tiene un PtoOperacion
            pass

        if enlaces > 0:
            enl = 'Enlace'  # Tiene una Enlace
            pass

        if pub > 0:
            publ = 'Publicidad'  # Tiene una Publicidad
            pass

        if serv > 0:
            servi = 'Servicio'  # Tiene un Servicio
            pass

        if (usuExistente == 0) & (region == 0) & (pto == 0) & (enlaces == 0) & (pub == 0) & (serv == 0):
            # Cliente Eliminado
            user_logged = self.request.user
            logger.info('Cliente: {}, con email: {}, ha sido ELIMINADO por super administrador con email: {}'.format
                        (cliente.nombre_o_razon, cliente.email, user_logged))
            cliente.eliminado = True
            cliente.email = cliente.email + '-ELIMINADO'
            cliente.save()
            # Usuario Eliminado
            usuario = Usuario.objects.get(pk=idUsu)
            usuElimando = mailUsu + '-ELIMINADO'
            usuario.username = usuElimando
            usuario.eliminado = True
            usuario.email = usuElimando
            usuario.save()
            pass
        return HttpResponse(
            json.dumps({'usr': valores, 'reg': reg, 'ptO': ptOpe, 'enl': enl, 'publ': publ, 'ser': servi}),
            content_type='application/json')


@login_required
def DesactivarClientes(request, pk):
    print(' *******  DesactivarClientes *********')
    cliente = Cliente.objects.get(pk=pk)
    if request.method == "POST":
        print(' ******* DesactivarClientes ELIMINADO *********')
        cliente.estado = False
        cliente.save()

        return redirect('clientes:cliente-index')
    return render(request, 'confirmar_eliminacion.html', {'object': cliente})


class CrearCliente(LoginRequiredMixin, CreateView):
    #model = Cliente
    template_name = "clientes/nuevo.html"
    success_url = reverse_lazy('clientes:cliente-index')
    form_class = ClienteForm

    def form_invalid(self, form):
        print("invalid form")
        dato = 1
        target = list(form.errors.values())
        error_string = ' '.join([' '.join(x for x in l) for l in target])
        print(form.errors.values())
        return HttpResponse(json.dumps({'data': dato, 'errors': error_string}), content_type='application/json')

    def post(self, request, *args, **kwargs):
        form = ClienteUpdateForm(request.POST, request.FILES)
        duplicate = Cliente.objects.filter(tipo_identificacion_id=form.data['tipo_identificacion'],
                                         identificacion=form.data['identificacion'])
        duplicate_email = Usuario.objects.filter(email=form.data['email'])
        if form.is_valid() and not duplicate and not duplicate_email:
            print('got_valid')
            new_client = form.save(commit=False)
            try:
                imagen = request.FILES['logo']
                new_client.logo = request.FILES['logo']
            except:
                imagen = 'imagen-cliente/user2.svg'
                print("error logo")
            self.logo_url = 'imagen-cliente/' + str(imagen)
            new_client.save()
            user_logged = self.request.user
            logger.info('Cliente: {}, ha sido CREADO por super administrador con email: {}'.format
                        (new_client.nombre_o_razon, user_logged))
            return self.form_valid(form)
        else:
            if duplicate_email:
                form.add_error(None, "Email ya esta registrado")
            if duplicate:
                form.add_error(None, "Documento ya esta registrado")
            return self.form_invalid(form)

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        # form.save()
        # print("valid form")
        # print(form.data)
        print("***********")
        print(self.logo_url)
        # imagen = form.logo
        # logo = 'imagen-cliente/' + str(imagen)

        apellido = form.data['apellido']
        mail = form.data['email']
        nombre = form.data['nombre_o_razon']
        numDocumento = str(form.data['identificacion'])
        padre = self.request.user.pk
        tipoIdentificacion = int(form.data['tipo_identificacion'])

        ingreso_tipo_via = form.data["tipo_via"]
        ingreso_numero_via = form.data["numero_via"]
        ingreso_prefijo_via = form.data["prefijo_via"]
        try:
            form.data["bis_via"]
            ingreso_bis_via = True
        except:
            ingreso_bis_via = False

        try:
            form.data["bis_via_generadora"]
            ingreso_bis_via_generadora = True
        except:
            ingreso_bis_via_generadora = False

        ingreso_cuadrante_via = form.data["cuadrante_via"]
        ingreso_numero_via_generadora = form.data["numero_via_generadora"]
        ingreso_prefijo_via_generadora = form.data["prefijo_via_generadora"]
        ingreso_numero_placa = form.data["numero_placa"]
        ingreso_cuadrante_via_generadora = form.data["cuadrante_via_generadora"]
        ingreso_complemento = form.data["complemento"]
        ingreso_barrio = form.data["barrio"]

        passw = contrasena()

        Usuario.objects.create_user(username=mail,
                                    email=mail,
                                    password=passw,
                                    is_administrador=True,
                                    numero_documento=numDocumento,
                                    last_name=apellido,
                                    first_name=nombre,
                                    padre_id=padre,
                                    tipo_identificacion_id=tipoIdentificacion,
                                    tipo_via=ingreso_tipo_via,
                                    numero_via=ingreso_numero_via,
                                    prefijo_via=ingreso_prefijo_via,
                                    bis_via=ingreso_bis_via,
                                    cuadrante_via=ingreso_cuadrante_via,
                                    numero_via_generadora=ingreso_numero_via_generadora,
                                    prefijo_via_generadora=ingreso_prefijo_via_generadora,
                                    bis_via_generadora=ingreso_bis_via_generadora,
                                    numero_placa=ingreso_numero_placa,
                                    cuadrante_via_generadora=ingreso_cuadrante_via_generadora,
                                    complemento=ingreso_complemento,
                                    barrio=ingreso_barrio,
                                    imagen=self.logo_url).save()

        x = Usuario.objects.get(username=mail).pk
        cliente = Cliente.objects.get(email=mail)
        cliente.usuario.add(x)

        data = {'user': str(nombre), 'rol': "Administrador", 'username': str(mail), 'password': passw,
                'url': 'https://dev.referirpaga.com/'}
        subject, from_email, to = 'welcome', 'noreply@mail.tnfactor.com.co', str(mail)
        html_content = render_to_string('otros/template_email_registro.html', {'data': data})
        msg = EmailMessage(subject, html_content, from_email, [to])  # ????
        msg.content_subtype = "html"  # Main content is now text/html
        msg.mixed_subtype = "related"

        img_data_1 = open("static/img/fondo-cumple.png", 'rb').read()
        img_data_2 = open("static/img/logo-tn.png", 'rb').read()
        img_data_3 = open("static/img/logo-rp.png", 'rb').read()

        imgs_data_list = [img_data_1, img_data_2, img_data_3]
        url_list = ['/static/img/fondo-cumple.png', '/static/img/logo-tn.png', '/static/img/logo-rp.png']
        imgs_dict = dict(zip(imgs_data_list, url_list))

        for img_data in imgs_dict:
            image = MIMEImage(img_data, 'png')
            image.add_header('Content-ID', '<{}>'.format(imgs_dict[img_data]))
            image.add_header("Content-Disposition", "inline", filename=imgs_dict[img_data])
            msg.attach(image)
        msg.send()
        dato = 0
        return HttpResponse(json.dumps({'data': dato, 'errors': " "}), content_type='application/json')


class ConsultaCliente(LoginRequiredMixin, DetailView):
    model = Cliente
    img = model.logo
    template_name = 'clientes/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        context = self.get_context_data(object=self.object)
        print(dir(context))
        return self.render_to_response(context)


@login_required
def ActualizarCliente(request, pk):
    cliente = get_object_or_404(Cliente, pk=pk)
    user_logged = request.user
    img = cliente.logo
    if request.method == 'POST':
        form = ClienteUpdateForm(request.POST, request.FILES, instance=cliente)
        if form.is_valid():
            form.save()
            apellido = request.POST.get('apellido')
            mail = request.POST.get('email')
            nombre = request.POST.get('nombre_o_razon')
            numDocumento = request.POST.get('identificacion')
            logger.info('Cliente: {}, ha sido MODIFICADO por super administrador con email: {}'.format
                        (nombre, user_logged))
            padre = request.user.pk
            try:
                logo = request.FILES['logo']
            except:
                logo = img
            tipoIdentificacion = int(request.POST.get('tipo_identificacion'))
            x = Usuario.objects.get(username=mail)
            x.numero_documento = numDocumento
            x.last_name = apellido
            x.first_name = nombre
            x.padre_id = padre
            x.tipo_identificacion_id = tipoIdentificacion
            x.logo = logo
            x.save()
            return redirect('clientes:cliente-index')
        else:
            print(form.errors)
            # print("************************")
    else:
        form = ClienteUpdateForm(instance=cliente)
        return render(request, 'clientes/nuevo.html', {'form': form, 'img': img})


@login_required
def ActivarClientes(request, pk):
    cliente = Cliente.objects.get(pk=pk)
    if request.method == "POST":
        cliente.estado = True
        cliente.save()
        return redirect('clientes:cliente-index')
    return render(request, 'confirmar_activacion.html', {'object': cliente})


@login_required
def identificacion(request):
    if request.is_ajax() and request.GET:
        try:
            ids = int(request.GET.get("idOrg"))
            # print (ids )
            if ids == 1:
                datos = TipoIdentificacion.objects.filter(id__in=[4, 7])
                valores = serializers.serialize('json', datos)
            elif ids == 2:
                datos = TipoIdentificacion.objects.filter(id__in=[1, 2, 3, 5, 6])  #
                valores = serializers.serialize('json', datos)
                pass
        except:
            valores = 0
    return HttpResponse(json.dumps(valores), content_type='application/json')


@login_required
def departamento(request):
    if request.is_ajax() and request.GET:
        id = request.GET.get("id")
        try:
            valor = Departamento.objects.filter(pais=id).order_by('nombre')
            valores = serializers.serialize('json', valor)
        except:
            valores = 0
    return HttpResponse(json.dumps(valores), content_type='application/json')


@login_required
def ciudad(request):
    if request.is_ajax() and request.GET:
        idDepartamento = request.GET.get("id")
        try:
            valor = Ciudad.objects.filter(departamento=idDepartamento).order_by('nombre')
            valores = serializers.serialize('json', valor)
        except:
            valores = 0
    return HttpResponse(json.dumps(valores), content_type='application/json')
