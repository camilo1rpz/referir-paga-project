from django.contrib import admin
from .models import *

# Register your models here.
modelos = [
	TipoOrganizacion,
	TipoSociedad,
	RegimenFiscal,
	Cliente,
]

admin.site.register(modelos)