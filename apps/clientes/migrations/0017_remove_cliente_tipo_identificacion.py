# Generated by Django 2.2 on 2019-04-25 16:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clientes', '0016_cliente_estado'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cliente',
            name='tipo_identificacion',
        ),
    ]
