# Generated by Django 2.2 on 2019-04-25 16:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0006_auto_20190425_1108'),
        ('clientes', '0018_delete_tipoidentificacion'),
    ]

    operations = [
        migrations.AddField(
            model_name='cliente',
            name='tipo_identificacion',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='usuarios.TipoIdentificacion'),
        ),
    ]
