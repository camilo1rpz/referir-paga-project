# Generated by Django 2.2 on 2019-05-03 16:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clientes', '0023_auto_20190502_1148'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cliente',
            name='direccion',
        ),
        migrations.AddField(
            model_name='cliente',
            name='eliminado',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='bis_via',
            field=models.BooleanField(blank=True, default=False),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='bis_via_generadora',
            field=models.BooleanField(blank=True, default=False),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='cuadrante_via',
            field=models.CharField(blank=True, choices=[('', 'Orientacion'), ('SUR', 'SUR'), ('NORTE', 'NORTE'), ('ESTE', 'ESTE'), ('OESTE', 'OESTE'), ('SURESTE', 'SURESTE'), ('SUROESTE', 'SUROESTE'), ('NORESTE', 'NORESTE'), ('NOROESTE', 'NOROESTE')], max_length=12, null=True),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='cuadrante_via_generadora',
            field=models.CharField(blank=True, choices=[('', 'Orientacion'), ('SUR', 'SUR'), ('NORTE', 'NORTE'), ('ESTE', 'ESTE'), ('OESTE', 'OESTE'), ('SURESTE', 'SURESTE'), ('SUROESTE', 'SUROESTE'), ('NORESTE', 'NORESTE'), ('NOROESTE', 'NOROESTE')], max_length=12, null=True),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='prefijo_via',
            field=models.CharField(blank=True, choices=[('', 'Prefijo'), ('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E'), ('F', 'F'), ('G', 'G'), ('H', 'H'), ('I', 'I'), ('J', 'J'), ('K', 'K'), ('L', 'L'), ('M', 'M'), ('N', 'N'), ('O', 'O'), ('P', 'P'), ('Q', 'Q'), ('R', 'R'), ('S', 'S'), ('T', 'T'), ('U', 'U'), ('V', 'V'), ('W', 'W'), ('X', 'X'), ('Y', 'Y'), ('Z', 'Z')], max_length=12, null=True),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='prefijo_via_generadora',
            field=models.CharField(blank=True, choices=[('', 'Prefijo'), ('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E'), ('F', 'F'), ('G', 'G'), ('H', 'H'), ('I', 'I'), ('J', 'J'), ('K', 'K'), ('L', 'L'), ('M', 'M'), ('N', 'N'), ('O', 'O'), ('P', 'P'), ('Q', 'Q'), ('R', 'R'), ('S', 'S'), ('T', 'T'), ('U', 'U'), ('V', 'V'), ('W', 'W'), ('X', 'X'), ('Y', 'Y'), ('Z', 'Z')], max_length=12, null=True),
        ),
    ]
