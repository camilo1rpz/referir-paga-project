# Generated by Django 2.2.7 on 2019-11-25 16:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clientes', '0033_auto_20191114_0752'),
    ]

    operations = [
        migrations.AddField(
            model_name='cliente',
            name='nombre_app',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
