from django.contrib.auth.tokens import default_token_generator
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

# Create your views here.
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from apps.validators import contrasena


@login_required
def index(request):
    user = request.user
    if not user.first_login:
        return render(request, 'index.html')
    else:
        user.password = contrasena()
        user.first_login = False
        user.save()
        url = 'https://dev.referirpaga.com/reset/'
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        token = default_token_generator.make_token(user)
        return redirect(url + uid + '/' + token)


def logout_view(request):
    logout(request)

