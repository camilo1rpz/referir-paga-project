import re
from django.utils.translation import ugettext as _
from django.core.exceptions import ValidationError
from apps.usuarios.models import Usuario
from string import ascii_uppercase, ascii_lowercase
import random
import string
from django.contrib import messages


# def alertas(mensaje):
#        messages.debug(request, '%s SQL statements were executed.' % mesaje)
#        #messages.info(request, 'Three credits remain in your account.')
#        #messages.success(request, 'Profile details updated.')
#        #messages.warning(request, 'Your account expires in three days.')
#        #messages.error(request, 'Document deleted.')
#        #alerta=mensaje


def emailexiste(email):
    if Usuario.objects.filter(email=email):
        raise ValidationError("Email %s ya exisiste valide su informacion !!" % email)


def codigo():
    codigo = random.sample(ascii_uppercase, 1) + random.sample('1234567890', 3)
    return ''.join(map(str, codigo))


def codes(letters, numbers):
    code = random.sample(ascii_uppercase, letters) + random.sample('1234567890', numbers)
    return ''.join(map(str, code))


def contrasena():
    uno = random.sample(ascii_uppercase + ascii_lowercase, 4)
    lista2 = ['$', '-', '*', '_', '+', '#', '/', '!', ':', '@']
    dos = random.sample(lista2, 1)
    aleatorios = [random.randint(0, 9) for _ in range(3)]
    valor = uno + dos + aleatorios
    dato = random.sample(valor, 8)
    contrasena = ''.join(map(str, dato))
    print(contrasena, ' **contrasena**')
    return contrasena


TIPOS_VIA = (
    ('CARRERA', 'CARRERA'),
    ('CALLE', 'CALLE'),
    ('DIAGONAL', 'DIAGONAL'),
    ('TRANSVERSAL', 'TRANSVERSAL'),
)

PREFIJO_VIA = [('', 'Prefijo')]
for x in string.ascii_uppercase:
    PREFIJO_VIA.append((x, x))

PUNTO_CARDINAL = (
    ('', 'Orientacion'),
    ('SUR', 'SUR'),
    ('NORTE', 'NORTE'),
    ('ESTE', 'ESTE'),
    ('OESTE', 'OESTE'),
    ('SURESTE', 'SURESTE'),
    ('SUROESTE', 'SUROESTE'),
    ('NORESTE', 'NORESTE'),
    ('NOROESTE', 'NOROESTE')
)


class SymbolValidator(object):
    def validate(self, password, user=None):
        if not re.findall('[()[\]{}|\\`~!@#$%^&*_\-+=;:\'",<>./?]', password):
            raise ValidationError(
                _("Su contraseña debe tener al menos 1 carácter especial: " + "()[]{}|\`~!@#$%^&*_-+=;:'\",<>./?"),
                code='password_no_symbol',
            )

    def get_help_text(self):
        return _("Su contraseña debe tener al menos 1 símbolo: " +
                 "()[]{}|\`~!@#$%^&*_-+=;:'\",<>./?"
                 )
