from django.db import models

# Create your models here.
class Etapa(models.Model):
	nombre = models.CharField(max_length=250)
	fecha_creacion = models.DateTimeField(auto_now_add=True)
	fecha_actualizacion = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.nombre

	class Meta():
		verbose_name_plural = "Etapas"


class Motivo(models.Model):
	nombre = models.CharField(max_length=250)
	etapa = models.ForeignKey(Etapa,on_delete=models.SET_NULL,null=True)
	fecha_creacion = models.DateTimeField(auto_now_add=True)
	fecha_actualizacion = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.nombre

	class Meta():
		verbose_name_plural = "Motivos"


class Homologacion(models.Model):
	motivo = models.OneToOneField(Motivo,on_delete=models.SET_NULL,null=True)
	categoria = models.IntegerField(null=True, blank=True)
	tipo_contacto = models.IntegerField(null=True, blank=True)
	tipificacion = models.IntegerField(null=True, blank=True)
	fecha_creacion = models.DateTimeField(auto_now_add=True)
	fecha_actualizacion = models.DateTimeField(auto_now=True)
	
	class Meta():
		verbose_name_plural = "Homologaciones"
