from django.contrib import admin
from .models import *
# Register your models here.
modelos = [
	Etapa,
	Motivo,
	Homologacion,
]

admin.site.register(modelos)