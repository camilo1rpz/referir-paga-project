from django.apps import AppConfig


class TipificacionesConfig(AppConfig):
    name = 'tipificaciones'
