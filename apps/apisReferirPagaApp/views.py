from django.contrib.auth import get_user_model, authenticate
from apps.ubicacion.models import Pais, Ciudad
from apps.usuarios.models import TipoIdentificacion
from apps.usuarios.models import Usuario
from apps.gestores.models import Campana, Servicio
from apps.clientes.models import Cliente
from apps.gestores.models import ReferidosApp, Enlace
from rest_framework import viewsets
from apps.apisReferirPagaApp.serializers import *
from django.http import JsonResponse
from django.core import serializers
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
from django.shortcuts import get_object_or_404
import json
import random
import requests

class PaisSerializer(viewsets.ModelViewSet):
    queryset = Pais.objects.all()
    serializer_class = PaiSerializer


class CiudadSerializer(viewsets.ModelViewSet):
    queryset = Ciudad.objects.all()
    serializer_class = CiudaSerializer


class DocumentoSerializer(viewsets.ModelViewSet):
    queryset = TipoIdentificacion.objects.all()
    serializer_class = TipoDocumentoSerializer


class ServiciosSerializer(viewsets.ModelViewSet):
    queryset = Servicio.objects.filter(estado=True)
    serializer_class = ServicioSerializer


class EnlacesClienteSerializer(viewsets.ModelViewSet):
    queryset = Enlace.objects.filter(estado=True)
    serializer_class = EnlaceSerializer


class ClienteEmpresa(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer


class EmpresaCliente(viewsets.ModelViewSet):
    def list(self, request):
        if request.method == 'GET':
            nom = ''
            img = ''
            cor = ''
            valor = []
            datos = Cliente.objects.filter(estado=True, eliminado=False).all()
            for i in datos:
                nom = i.nombre_o_razon
                img = ''.join(['http://', get_current_site(request).domain, i.logo.path])
                cor = i.email
                tel = i.telefono
                print(img)
                valor.append(
                    {"gender": "male", "name": {"title": "Mr", "telefono": tel, "first": "Herman", "last": nom},
                     "email": cor,
                     # "picture":{"thumbnail":"https://randomuser.me/api/portraits/thumb/men/1.jpg"},
                     "picture": {"thumbnail": img},
                     "nat": "AU"}
                )
                pass
        data = {"results": valor, "info": {"seed": "efa593bb8c5e7917", "results": 1, "page": 1, "version": "1.3"}}
        return JsonResponse(data)
        # return JsonResponse( {"results":[{"gender":"female","name":{"title":"Miss","first":"Cheverny",
        # "last":"Richardson"},"location":{"street":{"number":3581,"name":"De Chalmotweg"},"city":"Oud-Alblas",
        # "state":"Limburg","country":"Netherlands","postcode":39344,"coordinates":{"latitude":"37.4043",
        # "longitude":"144.3365"},"timezone":{"offset":"-11:00","description":"Midway Island, Samoa"}},
        # "email":"Cheverny.Richardson@example.com","login":{"uuid":"292155a6-599d-4b1d-9527-098097626a89",
        # "username":"heavybird617","password":"biteme1","salt":"IM1Rh2Zt","md5":"eeea7de81e8d4074648cc45dfee1f2a1",
        # "sha1":"e155938fd7bde5a714bd31733b5c63f8f768b32d",
        # "sha256":"6e99ce429354c81cef3f551d3218182678af49c0927752f53c28862414d169fc"},
        # "dob":{"date":"1972-07-03T09:14:47.639Z","age":47},"registered":{"date":"2003-02-08T13:02:36.166Z",
        # "age":16},"phone":"(441)-365-6394","cell":"(622)-766-3343","id":{"name":"BSN","value":"76361600"},
        # "picture":{"large":"https://randomuser.me/api/portraits/women/59.jpg",
        # "medium":"https://randomuser.me/api/portraits/med/women/59.jpg",
        # "thumbnail":"https://randomuser.me/api/portraits/thumb/women/59.jpg"},"nat":"NL"},{"gender":"male",
        # "name":{"title":"Mr","first":"Zélio","last":"Nogueira"},"location":{"street":{"number":8054,"name":"Rua
        # Principal"},"city":"Trindade","state":"Mato Grosso do Sul","country":"Brazil","postcode":48165,
        # "coordinates":{"latitude":"-20.8225","longitude":"161.2067"},"timezone":{"offset":"+3:30",
        # "description":"Tehran"}},"email":"Zelio.Nogueira@example.com","login":{
        # "uuid":"7934caa5-566c-4a5a-aa00-15fa9c8e6661","username":"sadtiger857","password":"playing",
        # "salt":"zf57AEqG","md5":"be051378f1386a720591031014112904",
        # "sha1":"69d6b7a0f269f0e22888dce92320827659622244",
        # "sha256":"29a04f1fd50f937b20df90055dd3e012538ffaeb3e2474f63facce60d2d84caf"},
        # "dob":{"date":"1998-02-19T20:22:16.693Z","age":21},"registered":{"date":"2005-09-06T10:51:36.000Z",
        # "age":14},"phone":"(60) 4613-2414","cell":"(44) 0944-1001","id":{"name":"","value":"null"},"picture":{
        # "large":"https://randomuser.me/api/portraits/men/57.jpg",
        # "medium":"https://randomuser.me/api/portraits/med/men/57.jpg",
        # "thumbnail":"https://randomuser.me/api/portraits/thumb/men/57.jpg"},"nat":"BR"},{"gender":"male",
        # "name":{"title":"Mr","first":"Tim","last":"Renaud"},"location":{"street":{"number":1854,"name":"Rue
        # Dugas-Montbel"},"city":"Avignon","state":"Lot-et-Garonne","country":"France","postcode":33461,
        # "coordinates":{"latitude":"74.9063","longitude":"159.3207"},"timezone":{"offset":"0:00",
        # "description":"Western Europe Time, London, Lisbon, Casablanca"}},"email":"Tim.Renaud@example.com",
        # "login":{"uuid":"6748bbed-f39b-417a-a546-27771f772002","username":"heavyfish163","password":"april1",
        # "salt":"XajHlGx2","md5":"7347d71308e363d154c0be3783edd5de",
        # "sha1":"46cf9c6df71e886713d62d679dfd5a05513ee973",
        # "sha256":"c795b8adba6c9ffcef6e4ff93b9c42780238dedebe85a7a2b3761d0984d23b28"},
        # "dob":{"date":"1960-01-06T22:41:26.677Z","age":59},"registered":{"date":"2008-01-13T00:42:22.843Z",
        # "age":11},"phone":"04-58-32-60-13","cell":"06-97-50-54-36","id":{"name":"INSEE","value":"1NNaN41256310
        # 76"},"picture":{"large":"https://randomuser.me/api/portraits/men/82.jpg",
        # "medium":"https://randomuser.me/api/portraits/med/men/82.jpg",
        # "thumbnail":"https://randomuser.me/api/portraits/thumb/men/82.jpg"},"nat":"FR"},{"gender":"male",
        # "name":{"title":"Mr","first":"Andreias","last":"Barros"},"location":{"street":{"number":6337,"name":"Rua
        # Onze "},"city":"São Vicente","state":"Paraná","country":"Brazil","postcode":51789,"coordinates":{
        # "latitude":"1.3838","longitude":"-18.8016"},"timezone":{"offset":"+2:00","description":"Kaliningrad,
        # South Africa"}},"email":"Andreias.Barros@example.com","login":{
        # "uuid":"ed72d294-c028-4286-9174-145db9d63a12","username":"bluepanda308","password":"parola",
        # "salt":"ftUtkU28","md5":"8b19f148bedaf1ab17e83b0d605e70a4",
        # "sha1":"f5627779bd2227874a5b95c6719b88cdb001709d",
        # "sha256":"ff10ee06b898d5f3a783c6c10c0c1825dfc66b26a029b323b5f65b5c277141a2"},
        # "dob":{"date":"1957-12-01T16:12:41.805Z","age":62},"registered":{"date":"2006-09-05T22:40:59.570Z",
        # "age":13},"phone":"(34) 0820-8167","cell":"(68) 1766-4143","id":{"name":"","value":"null"},"picture":{
        # "large":"https://randomuser.me/api/portraits/men/42.jpg",
        # "medium":"https://randomuser.me/api/portraits/med/men/42.jpg",
        # "thumbnail":"https://randomuser.me/api/portraits/thumb/men/42.jpg"},"nat":"BR"},{"gender":"male",
        # "name":{"title":"Mr","first":"Phil","last":"Frazier"},"location":{"street":{"number":9535,"name":"Station
        # Road"},"city":"Cambridge","state":"Central","country":"United Kingdom","postcode":"WL92 9GW",
        # "coordinates":{"latitude":"-29.7655","longitude":"175.4023"},"timezone":{"offset":"+4:00",
        # "description":"Abu Dhabi, Muscat, Baku, Tbilisi"}},"email":"Phil.Frazier@example.com",
        # "login":{"uuid":"b8087597-238a-4530-8c16-118a6e8714b5","username":"greenswan265","password":"888888",
        # "salt":"cp3mLd1g","md5":"e5df1d0f33eee723d825ac2a2be4f33c",
        # "sha1":"47d88cf365fae6b6f60dba984b508c3ce7709469",
        # "sha256":"321ab5324244bfd7d6c13c758d694001d956aa9c35ec2f5b330859206194442f"},
        # "dob":{"date":"1979-07-21T04:31:35.687Z","age":40},"registered":{"date":"2017-05-20T01:30:46.741Z",
        # "age":2},"phone":"017684 99458","cell":"0708-670-779","id":{"name":"NINO","value":"HP 17 14 13 T"},
        # "picture":{"large":"https://randomuser.me/api/portraits/men/87.jpg",
        # "medium":"https://randomuser.me/api/portraits/med/men/87.jpg",
        # "thumbnail":"https://randomuser.me/api/portraits/thumb/men/87.jpg"},"nat":"GB"},{"gender":"female",
        # "name":{"title":"Mrs","first":"Rosana","last":"Jongkind"},"location":{"street":{"number":5720,"name":"Henri
        # Didonweg"},"city":"Nijemirdum","state":"Noord-Brabant","country":"Netherlands","postcode":48516,
        # "coordinates":{"latitude":"-60.5326","longitude":"91.2907"},"timezone":{"offset":"+10:00",
        # "description":"Eastern Australia, Guam, Vladivostok"}},"email":"Rosana.Jongkind@example.com",
        # "login":{"uuid":"dfa2b334-6660-4d40-92ac-ad2caa626593","username":"tinyrabbit577","password":"323232",
        # "salt":"7niRFUqI","md5":"2011ecab0d8991d3ec30666570b2f66a",
        # "sha1":"f59c665a5c5b5b2b41e6c53068e0b1a27d07b73e",
        # "sha256":"d462a518dab0eb53a1b653f8bf58b0263a7178e45c6ff6edb5940d6db2cb5a0f"},
        # "dob":{"date":"1958-01-27T21:31:11.892Z","age":61},"registered":{"date":"2003-12-29T18:10:04.516Z",
        # "age":16},"phone":"(932)-194-4607","cell":"(359)-927-3104","id":{"name":"BSN","value":"36430413"},
        # "picture":{"large":"https://randomuser.me/api/portraits/women/16.jpg",
        # "medium":"https://randomuser.me/api/portraits/med/women/16.jpg",
        # "thumbnail":"https://randomuser.me/api/portraits/thumb/women/16.jpg"},"nat":"NL"}],
        # "info":{"seed":"008d2def08e0d6a8","results":6,"page":1,"version":"1.3"}} )


class ListarUsuario(viewsets.ModelViewSet):
    def list(self, request):
        if request.method == 'GET':
            idUsuario = request.GET.get('idUsuario')
            nom = ''
            ape = ''
            fec = ''
            tel = ''
            ema = ''
            pai = ''
            ciu = ''

            datos = Usuario.objects.filter(id=idUsuario).all()
            for i in datos:
                nom = i.first_name
                ape = i.last_name
                fec = i.fecha_nacimiento
                tel = i.telefono
                ema = i.email
                pai = str(i.pais)
                ciu = str(i.ciudad)
                pass
        pass
        return JsonResponse({'nom': nom, 'ape': ape, 'fec': fec, 'tel': tel, 'ema': ema, 'pai': pai, 'ciu': ciu},
                            safe=False)


class LoginSerializer(viewsets.ModelViewSet):  # PENDIENTE == EN CONTRASEÑA ENCRIPTADA
    def list(self, request):
        if request.method == 'GET':
            if (request.GET.get('mail')) and (request.GET.get('pass')):
                email = request.GET.get('mail')
                password = request.GET.get('pass')
                user = authenticate(username=email, password=password)
                if user is not None:
                    return JsonResponse(
                        {'id': user.id, 'nom': user.first_name,
                         'ape': user.last_name, 'fec': user.fecha_nacimiento,
                         'tel': user.telefono, 'mai': user.email,
                         'pai': user.pais.pk, 'ciu': user.ciudad.pk}, safe=False)
                else:
                    return JsonResponse('credenciles incorrectas', safe=False)
        return JsonResponse('error en petición', safe=False)


class CodigoSerializer(viewsets.ModelViewSet):
    def list(self, request):
        if request.method == 'GET':
            if request.GET.get('celular'):
                numeroMovil = str(request.GET.get('celular'))
                # Genero Codigo
                n = [random.randint(0, 9) for _ in range(6)]
                codigo = (str(n)[1:-1]).replace(', ', '')
                # Envio MSJ
                url = "http://api.messaging-service.com/sms/1/text/query"
                querystring = {"username": "TNCOLOMBIA", "password": "Colombia2020", "to": "57" + numeroMovil,
                               "text": "Referir Paga \nTu codigo de verificacion Es: " + codigo}
                response = requests.request("GET", url, params=querystring)

                return JsonResponse(codigo, safe=False)
                pass
            pass
        return JsonResponse('***Error Numero Celular ***', safe=False)


class ValidacionCodigo(viewsets.ModelViewSet):
    def list(self, request):
        if request.method == 'GET':
            if request.GET.get('codigoMovil'):
                codigoMovil = request.GET.get('codigoMovil')
                codigo = request.GET.get('valor')

                if codigoMovil == codigo:
                    print('CODIGO GIEN')
                    return JsonResponse('Codigo Bien', safe=False)
                else:
                    print('Error En Codigo')
                    return JsonResponse('Codigo Incorrecto, Vuelva a intentarlo', safe=False)
                    pass

                pass
            pass
        return JsonResponse('*** Se ha Genero una Exception ***', safe=False)


# class VerReferidoAppSerializer(viewsets.ModelViewSet):
#    queryset = ReferidosApp.objects.all()
#    serializer_class = ReferidoAppRegistro


class ReferidoAppSerializer(viewsets.ModelViewSet):
    def list(self, request):
        if request.method == 'GET':
            if (request.GET.get('name')) and (request.GET.get('username')):
                serializador = ReferidoAppRegistro(
                    data={"nombre": request.GET.get('name'), "celular": request.GET.get('movil'),
                          "pais": int(request.GET.get('country')), "ciudad": int(request.GET.get('city')),
                          "referidor": int(request.GET.get('username')), "servicios": request.GET.get('service')})
                if serializador.is_valid():
                    serializador.save()
                    obj = serializador.save()
                    return JsonResponse(obj.id, safe=False)  # Id
                pass
            pass
        return JsonResponse('***Error al Referir ****', safe=False)


class UsuarioSerializer(viewsets.ModelViewSet):
    def list(self, request):
        if request.method == 'GET':
            if (request.GET.get('docu')) and (request.GET.get('fec')):
                codigoExiste = ''
                campania = Campana.objects.filter(estado=True)
                asesores = Usuario.objects.filter(is_asesor=True)
                codigoUser = request.GET.get('cod').upper()
                print(codigoUser)
                if codigoUser:
                    if campania:
                        for i in campania:
                            if i.codigoCampana == codigoUser:
                                codigoExiste = 'Codigo Igual'
                            pass
                        pass

                    if asesores:
                        for x in asesores:
                            if x.codigo == codigoUser:
                                codigoExiste = 'Codigo Igual'
                            pass
                        pass

                else:
                    # print('****NO ENVIO CODIGO****')
                    serializador = UserSerializer(
                        data={"password": request.GET.get('pass'), "username": request.GET.get('correo'),
                              "email": request.GET.get('correo'), "tipo_identificacion": request.GET.get('docu'),
                              "numero_documento": request.GET.get('numI'), "first_name": request.GET.get('nom'),
                              "last_name": request.GET.get('ape'), "fecha_nacimiento": str(request.GET.get('fec')),
                              "telefono": int(request.GET.get('cel')), "pais": int(request.GET.get('pa')),
                              "ciudad": int(request.GET.get('ciu')), "codigo": '', "is_persona_natural": 1,
                              "empresa_idCliente": ''})
                    if serializador.is_valid():
                        obj = serializador.save(first_login=False)
                        return JsonResponse(obj.id, safe=False)  # Id
                    pass

                if codigoExiste == 'Codigo Igual':
                    serializador = UserSerializer(
                        data={"password": request.GET.get('pass'), "username": request.GET.get('correo'),
                              "email": request.GET.get('correo'), "tipo_identificacion": request.GET.get('docu'),
                              "numero_documento": request.GET.get('numI'), "first_name": request.GET.get('nom'),
                              "last_name": request.GET.get('ape'), "fecha_nacimiento": str(request.GET.get('fec')),
                              "telefono": int(request.GET.get('cel')), "pais": int(request.GET.get('pa')),
                              "ciudad": int(request.GET.get('ciu')), "codigo": request.GET.get('cod').upper(),
                              "is_persona_natural": 1, "empresa_idCliente": request.GET.get('empres')})
                    if serializador.is_valid():
                        obj = serializador.save(first_login=False)
                        return JsonResponse(obj.id, safe=False)  # Id
                    else:
                        return JsonResponse('Error al Guardar Codigo - Correo Ya Registrado', safe=False)
                    pass
            pass
        return JsonResponse(
            'Código ingresado No pertenece a Un Asesor o a Una Campaña. Por favor, Compruebe el código y vuelva a '
            'intentarlo, (Asesor: Una letra y Tres Digitos / Campaña: Dos letras y dos Números',
            safe=False)


# Update
class UsuarioUpdateSerializer(viewsets.ModelViewSet):
    def list(self, request):
        if request.method == 'GET':
            idUsuario = Usuario.objects.get(id=request.GET.get('id'))
            # existeDato = Usuario.objects.get( telefono = request.GET.get('cel') )
            # print ( existeDato )
            print('__existeDato__')

            registrar = UserUpdate(idUsuario,
                                   data={"first_name": request.GET.get('name'), "last_name": request.GET.get('ape'),
                                         "telefono": request.GET.get('cel'), "email": request.GET.get('cor'),
                                         "username": request.GET.get('cor')})
            if registrar.is_valid():
                obj = registrar.save()
                return JsonResponse(obj.id, safe=False)
            pass
        return JsonResponse('Error al Actualizar Datos', safe=False)
