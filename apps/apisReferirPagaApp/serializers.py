from apps.ubicacion.models import Pais, Ciudad
from apps.usuarios.models import TipoIdentificacion
from apps.usuarios.models import Usuario
from apps.gestores.models import ReferidosApp, Enlace
from apps.clientes.models import Cliente
from apps.gestores.models import Campana, Servicio
from rest_framework import serializers


class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['email', 'password']


class PaiSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pais
        fields = ['id', 'nombre']


class CiudaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ciudad
        fields = ['id', 'nombre']


class TipoDocumentoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TipoIdentificacion
        fields = ['id', 'nombre', ]


class ClienteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cliente
        fields = ['id', 'nombre_o_razon', 'logo']


class UserListarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['first_name', 'last_name', 'fecha_nacimiento', 'telefono', 'email', 'pais', 'ciudad']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['password', 'username', 'email', 'tipo_identificacion', 'numero_documento', 'first_name', 'last_name',
                  'fecha_nacimiento', 'telefono', 'pais', 'ciudad', 'codigo', 'is_persona_natural', 'empresa_idCliente']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """create a new user with encrypted password and return it"""
        return Usuario.objects.create_user(**validated_data)
# Update
class UserUpdate(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['id', 'first_name', 'last_name', 'telefono', 'email', 'username']


# Registro App
class ReferidoAppRegistro(serializers.ModelSerializer):
    class Meta:
        model = ReferidosApp
        fields = ['nombre', 'celular', 'pais', 'ciudad', 'referidor', 'servicios']


class ServicioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Servicio
        fields = ['id', 'nombre', 'icono', 'color_icono', 'color_fondo']


class EnlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enlace
        fields = ['nombre', 'url', 'cliente']
