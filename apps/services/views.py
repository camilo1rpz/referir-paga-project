from django.shortcuts import render
from .services import get_token, request_ucontact, save_to_db


def ucontact_query(requests, phone):
    token = get_token()
    data = request_ucontact(token, phone)
    db = save_to_db(data)

    context = {
        'token': token,
        'data': data,
        'db': db,
    }
    return render(requests, 'otros/hello_user.html', context)




