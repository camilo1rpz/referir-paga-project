from django.urls import path, include
from apps.services import views

app_name = "services"
urlpatterns = [
    path('get_service/<int:phone>/', views.ucontact_query, name="get-service"),
]
