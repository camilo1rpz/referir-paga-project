import urllib
from datetime import datetime
import requests
from django.shortcuts import get_object_or_404

from apps.gestores.models import ReferidosApp
from apps.services.models import UcontactDb


def generate_request(url, headers={}, params={}):
    response = requests.request("POST", url, params=params, headers=headers)
    print(response.status_code)
    print(response)
    if response.status_code == 200:
        return response.json()


def get_token():
    querystring = {"user": "Apluser", "password": "Icell2019"}
    headers = {
        'Content-Type': "application/x-www-form-urlencoded",
        'charset': "UTF-8",
    }
    response = generate_request('https://icell.ucontactcloud.com/Integra/resources/auth/UserLogin',
                                headers, querystring)
    if response:
        print(response)
        return response[-1]
    return 'error'


def request_ucontact(authorization, phone):
    query = 'SELECT idcliente, fecha, res1, res2, res3, comentarios' \
            ' FROM CRM_Gestiones WHERE telMarcado={}'.format(phone)

    query_parse = urllib.parse.quote(query)
    payload = "query={}&dsn=".format(query_parse, safe='/')
    # payload = "query=SELECT%20idcliente%20FROM%20CRM_Gestiones%20WHERE%20telMarcado%3D3213383116&dsn="
    auth = "Basic {}".format(authorization)
    print("authorization = {}".format(auth))
    print("body = {}".format(payload))

    headers = {
        'Authorization': auth,
        'Content-Type': "application/x-www-form-urlencoded",
    }
    response = requests.request("POST", "https://icell.ucontactcloud.com/Integra/resources/forms/FormGet",
                                data=payload,
                                headers=headers)

    data = response.json()[0]
    # data['telefono'] = phone
    data['telefono'] = phone
    data['fecha'] = datetime.strptime(data['fecha'], "%b %d, %Y %I:%M:%S %p").date()
    return data


def save_to_db(data):
    referir_app = get_object_or_404(ReferidosApp, celular=data['telefono'])
    referidor = referir_app.referidor_id

    print("referidor: {}".format(referidor))
    ucontact = UcontactDb.objects.update_or_create(referidor=referidor, fecha=data['fecha'],
                                                 idcliente=data['idcliente'], res1=data['res1'],
                                                 res2=data['res2'], res3=data['res3'],
                                                 comentarios=data['comentarios'])
    return ucontact
