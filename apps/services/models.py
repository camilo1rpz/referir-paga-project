from django.db import models

#idcliente, fecha, res1, res2, res3, comentarios
from apps.usuarios.models import Usuario


class UcontactDb(models.Model):
    referidor = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True, related_name='ucontact')
    fecha = models.DateField()
    idcliente = models.IntegerField()
    res1 = models.CharField(max_length=80)
    res2 = models.CharField(max_length=80)
    res3 = models.CharField(max_length=80)
    comentarios = models.TextField(max_length=200)

    def __str__(self):
        return "contacted idclient:{}".format(self.idcliente)

