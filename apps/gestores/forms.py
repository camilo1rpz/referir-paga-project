from django import forms
from django.forms import SelectDateWidget

from .widgets import BootstrapDateTimePickerInput
from .models import Servicio, Publicidad, Enlace, Campana, Producto
from apps.sedes.models import Regional
from apps.usuarios.models import *
# from apps.usuarios.models import Usuario
from apps.clientes.models import Cliente


class ServicioForm(forms.ModelForm):
    class Meta:
        model = Servicio

        fields = [
            "estado",
            "nombre",
            "icono",
            "color_icono",
            "color_fondo",
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Nombre", 'required': 'required'}),
            'icono': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Icono", 'required': 'required'}),
            'color_icono': forms.TextInput(
                attrs={'class': "form-control", 'type': 'color', 'placeholder': "Color Icono", 'required': 'required'}),
            'color_fondo': forms.TextInput(
                attrs={'class': "form-control", 'type': 'color', 'placeholder': "Color Fondo", 'required': 'required'}),
        }


class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto

        fields = [
            "estado",
            "servicio",
            "nombre",
            "valor_comision",
            "valor_comision_str",
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Nombre", 'required': 'required'}),
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        self.fields['servicio'] = forms.ModelChoiceField(queryset=Servicio.objects.filter(cliente=user),
                                                         empty_label='Seleccione un servicio')

    def clean_nombre(self):
        nombre_producto = self.cleaned_data['nombre']
        service = Servicio.objects.get(nombre=self.cleaned_data['servicio'])
        instance = getattr(self, 'instance', None)
        print(instance)
        if Producto.objects.filter(nombre=nombre_producto, servicio=service) and not instance:
            raise forms.ValidationError("Producto con ese nombre ya existe")
        return nombre_producto


class PublicidadForm(forms.ModelForm):
    class Meta:
        model = Publicidad

        fields = [
            "estado",
            "nombre",
            "imagen",
            "url",
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Nombre", 'required': 'required'}),
            'url': forms.TextInput(
                attrs={'class': "form-control", 'type': 'url', 'placeholder': "Url", 'required': 'required'}),
        }


class EnlaceForm(forms.ModelForm):
    class Meta:
        model = Enlace

        fields = [
            "estado",
            "nombre",
            "url",
        ]

        widgets = {
            'nombre': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Nombre", 'required': 'required'}),
            'url': forms.TextInput(
                attrs={'class': "form-control", 'type': 'url', 'placeholder': "Url", 'required': 'required'}),
        }


class CampanaForm(forms.ModelForm):
    class Meta:
        model = Campana
        fields = [
            "estado",
            "nombre",
            # "codigoCampana",
            "descripcion",
            "responsableCampana",
            "fecha_inicio",
            "fecha_fin",
            # "asesor"
        ]
        widgets = {
            'nombre': forms.TextInput(attrs={'class': "form-control", 'placeholder': "Nombre", 'required': 'required'}),
            # 'codigoCampana': forms.CharField(widget=forms.TextInput(attrs={'readonly': 'readonly'})),
            # 'codigoCampana': forms.TextInput( attrs={ 'class': "form-control" }),
            'descripcion': forms.Textarea(
                attrs={'class': "form-control", 'placeholder': "Descripcion", 'required': 'required'}),
            'fecha_inicio': forms.DateInput(
                attrs={'class': "form-control fechaInicio", 'type': 'date', 'required': 'required'}),
            'fecha_fin': forms.DateInput(attrs={'class': "form-control", 'type': 'date', 'required': 'required'}),
            # 'asesor': forms.Select(attrs={'class': "form-control", "required": "required", })
        }

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['nombre'].widget.attrs['readonly'] = True
        # choices = [("", "Seleccione Una Opción")]
        # for pt in Usuario.objects.filter(empresa_idCliente=user).order_by("first_name"):
        #     concatenado = (str(pt.first_name) + "  " + str(pt.last_name)).upper()  # "("+str(pt.codigo)+")  "+
        #     choices.append([pt.id, concatenado])
        #
        # self.fields['asesor'].choices = choices
        choices2 = [("", "Seleccione Una Opción")]
        for pt in Usuario.objects.filter(is_responsable_campana=True, estado=True, cliente=user).order_by("first_name"):
            concatenado = (str(pt.first_name) + "  " + str(pt.last_name)).upper()  # "("+str(pt.codigo)+")  "+
            choices2.append([pt.id, concatenado])

        self.fields['responsableCampana'].choices = choices2
