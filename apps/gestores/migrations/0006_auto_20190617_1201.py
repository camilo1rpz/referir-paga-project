# Generated by Django 2.2 on 2019-06-17 17:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gestores', '0005_servicio_eliminado'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicio',
            name='cliente',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='clientes.Cliente'),
        ),
    ]
