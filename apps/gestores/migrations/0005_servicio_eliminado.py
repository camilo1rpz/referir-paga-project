# Generated by Django 2.2 on 2019-06-17 14:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestores', '0004_auto_20190614_1709'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicio',
            name='eliminado',
            field=models.BooleanField(default=False),
        ),
    ]
