# Generated by Django 2.2 on 2019-06-17 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestores', '0007_auto_20190617_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicio',
            name='nombre',
            field=models.CharField(max_length=50),
        ),
    ]
