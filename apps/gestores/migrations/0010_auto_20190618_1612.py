# Generated by Django 2.2 on 2019-06-18 21:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gestores', '0009_remove_enlace_tipo_enlace'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enlace',
            name='cliente',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='clientes.Cliente'),
        ),
    ]
