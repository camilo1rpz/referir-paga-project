import locale

from django.core.validators import RegexValidator, MinLengthValidator
from django.db import models
from datetime import datetime

from django.utils.text import slugify

from apps.clientes.models import Cliente
from apps.sedes.models import *
from apps.usuarios.models import Usuario
from django.contrib.auth.models import User
from django.conf import settings

from apps.validators import codes


class TipoEnlace(models.Model):
    nombre = models.CharField(max_length=25)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta():
        verbose_name_plural = "TipoEnlaces"


class Enlace(models.Model):
    estado = models.BooleanField(default=True)
    nombre = models.CharField(max_length=50)
    url = models.URLField(max_length=5050)
    cliente = models.ForeignKey(Cliente, on_delete=models.SET_NULL, null=True)

    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Enlaces"


class Servicio(models.Model):
    estado = models.BooleanField(default=True)
    nombre = models.CharField(max_length=50, )
    code = models.CharField(max_length=60, null=True, blank=True, default=000000)
    icono = models.CharField(max_length=50)
    color_icono = models.CharField(max_length=8)
    color_fondo = models.CharField(max_length=8)
    cliente = models.ForeignKey(Cliente, on_delete=models.SET_NULL, null=True)
    eliminado = models.BooleanField(default=False)

    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Servicios"

    def save(self, *args, **kwargs):
        if not self.code:
            while True:
                code_genereted = codes(0, 4)
                if not Servicio.objects.filter(code=code_genereted):
                    break
            self.code = code_genereted
        super(Servicio, self).save(*args, **kwargs)


class Publicidad(models.Model):
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(upload_to='imagenes/', null=False, )
    url = models.URLField(max_length=5050, null=True, blank=True)
    estado = models.BooleanField(default=True)
    cliente = models.ForeignKey(Cliente, on_delete=models.SET_NULL, null=True)  # cambiar ssxDe 1 a 1
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Publicidades"


alphanumeric = RegexValidator(r'^[0-9a-zA-Z]', 'solo valores alfanumericos permitidos.')


class Campana(models.Model):
    nombre = models.CharField(max_length=25, validators=[alphanumeric,
                                                         MinLengthValidator(3, message='Nombre muy corto')])
    codigoCampana = models.CharField(max_length=5, null=True, blank=True)
    descripcion = models.TextField(null=True, max_length=50,
                                   validators=[MinLengthValidator(5, message='Descripción muy corta')])
    fecha_inicio = models.DateField(null=True, blank=True)
    fecha_fin = models.DateField(null=True, blank=True)
    estado = models.BooleanField(default=True)
    responsableCampana = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True)
    asesor = models.ForeignKey(Usuario, on_delete=models.SET_NULL, related_name='asesores', null=True)
    cliente = models.ForeignKey(Cliente, on_delete=models.SET_NULL, related_name='clientes', null=True)  # De 1 a *

    # pto_operacion = models.ManyToManyField(PtoOperacion)
    # usuario = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Campanias"


class Producto(models.Model):
    nombre = models.CharField(max_length=40)
    nombre_slug = models.CharField(max_length=60)
    code = models.CharField(max_length=60, null=True, blank=True, default=000000)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE, related_name='productos')
    valor_comision = models.DecimalField(max_digits=9, decimal_places=2)
    estado = models.BooleanField(default=True)
    valor_comision_str = models.CharField(max_length=20)

    def save(self, *args, **kwargs):
        if not self.nombre_slug:
            self.nombre_slug = slugify(self.nombre)
        if not self.valor_comision_str:
            self.valor_comision_str = str(self.valor_comision)
        if not self.code:
            while True:
                code_genereted = codes(4, 0)
                code_servicio = self.servicio.code
                if not Producto.objects.filter(code=code_servicio + code_genereted):
                    break
            self.code = code_servicio + code_genereted
        super(Producto, self).save(*args, **kwargs)

    def __str__(self):
        return self.nombre


class ReferidosApp(models.Model):
    nombre = models.CharField(max_length=50)
    celular = models.CharField(max_length=15, null=True, blank=True, )
    pais = models.ForeignKey(Pais, on_delete=models.SET_NULL, null=True, blank=True)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.SET_NULL, null=True, blank=True)
    servicios = models.CharField(max_length=100, null=True, blank=True)
    referidor = models.ForeignKey(Usuario, on_delete=models.SET_NULL, null=True, blank=True)
    asesor = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "ReferidosApp"
