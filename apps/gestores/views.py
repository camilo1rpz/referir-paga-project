import mimetypes

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.db.models.functions import ExtractMonth
from django.shortcuts import render, redirect
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.dateparse import parse_date

from apps.clientes.models import Cliente
from .models import Servicio, Publicidad, Enlace, Campana, Producto
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from .forms import ServicioForm, PublicidadForm, EnlaceForm, CampanaForm, ProductoForm
from django.urls import reverse_lazy, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
import datetime
import json
import random
import string
from django.views.generic import TemplateView
from apps.usuarios.models import Usuario
from .tasks import user_birthday, code_to_user
import logging
import csv, io

logger = logging.getLogger(__name__)
loggerProd = logging.getLogger('prod_log')
loggerServ = logging.getLogger('serv_log')


class ListarServicio(LoginRequiredMixin, ListView):
    """docstring for ClassName"""
    model = Servicio
    template_name = 'servicios/index.html'
    paginate_by = 50

    def get(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Cliente.objects.get(id=as_user['as_usuario'])
        else:
            cliente = request.user.cliente_set.all()[0]
        search = request.GET.get('q')

        if search:
            filtro = self.model.objects.filter(Q(cliente=cliente) & Q(eliminado=False) &
                                               (
                                                       Q(nombre__icontains=search) |
                                                       Q(icono__icontains=search) |
                                                       Q(color_icono__icontains=search) |
                                                       Q(color_fondo__icontains=search)
                                               )
                                               ).order_by('nombre')

            self.object_list = filtro
        else:
            self.object_list = self.model.objects.filter(
                cliente=cliente, eliminado=False).order_by('nombre')

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        return self.render_to_response(context)


class CrearServicio(LoginRequiredMixin, CreateView):
    """docstring for CrearServicio"""
    model = Servicio
    template_name = 'servicios/nuevo.html'
    success_url = reverse_lazy('gestores:servicio-index')
    form_class = ServicioForm

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        model = form.save(commit=False)
        cliente = self.request.user.cliente_set.all()[0].pk
        model.cliente_id = cliente
        nombre = form.data['nombre']
        if self.model.objects.filter(cliente=cliente, nombre=nombre, eliminado=False):
            messages.info(
                self.request, 'ya existe algun servicio creado con este nombre')
            return self.render_to_response(self.get_context_data(form=form))
        else:
            model.save()
            loggerServ.info('Servicio: {}, con codigo: {}, ha sido AGREGADO por usuario con email: {}'.
                            format(model.nombre, model.code, Cliente.objects.get(id=self.cliente_id).email))
        return super().form_valid(form)

    def __init__(self, ):
        super(CrearServicio, self).__init__()


class ActualizarServicio(LoginRequiredMixin, UpdateView):
    """docstring for ClassName"""

    model = Servicio
    template_name = 'servicios/nuevo.html'
    success_url = reverse_lazy('gestores:servicio-index')
    form_class = ServicioForm

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        model = form.save(commit=False)
        cliente = self.request.user.cliente_set.all()[0].pk
        model.cliente_id = cliente
        nombre = form.data['nombre']
        if 'nombre' in form.changed_data:
            if self.model.objects.filter(cliente=cliente, nombre=nombre, eliminado=False):
                messages.info(
                    self.request, 'ya existe algun servicio creado con este nombre')
                return self.render_to_response(self.get_context_data(form=form))
        else:
            model.save()
            loggerServ.info('Servicio: {}, con codigo: {}, ha sido MODIFICADO por usuario con email: {}'.
                            format(model.nombre, model.code, Cliente.objects.get(id=self.cliente_id).email))
        return super().form_valid(form)


class ConsultaServicio(LoginRequiredMixin, DetailView):
    """docstring for ClassName"""
    model = Servicio
    template_name = 'servicios/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'Eliminar' in request.get_full_path():
            context = self.get_context_data(object=self.object, eliminar=True)
        return self.render_to_response(context)

    def __init__(self):
        super(ConsultaServicio, self).__init__()


class EliminarServicio(LoginRequiredMixin, DeleteView):
    """docstring for ClassName"""
    model = Servicio

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id

        borrar = request.POST.get('id')
        servicio = self.model.objects.get(pk=borrar)
        if servicio.delete():
            loggerServ.info('Servicio: {}, con codigo: {}, ha sido ELIMINADO por usuario con email: {}'.
                            format(servicio.nombre, servicio.code, Cliente.objects.get(id=self.cliente_id).email))
            dato = 1
        else:
            dato = 0
        return HttpResponse(json.dumps(dato), content_type='application/json')

    def __init__(self):
        super(EliminarServicio, self).__init__()


class ListarPublicidad(LoginRequiredMixin, ListView):
    """docstring for ClassName"""
    model = Publicidad
    template_name = 'publicidad/index.html'
    paginate_by = 50

    def get(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Cliente.objects.get(id=as_user['as_usuario'])
        else:
            cliente = request.user.cliente_set.all()[0]
        print('cliente')
        print(cliente)
        print('cliente')
        search = request.GET.get('q')

        if search:
            filtro = self.model.objects.filter(Q(cliente=cliente) & (
                    Q(nombre__icontains=search) |
                    Q(url__icontains=search)
            )
                                               ).order_by('nombre')

            self.object_list = filtro
        else:
            self.object_list = self.model.objects.filter(
                cliente=cliente).order_by('nombre')

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        return self.render_to_response(context)

    def __init__(self):
        super(ListarPublicidad, self).__init__()


class CrearPublicidad(LoginRequiredMixin, CreateView):
    """docstring for CrearServicio"""

    model = Publicidad
    template_name = 'publicidad/nuevo.html'
    success_url = reverse_lazy('gestores:publicidad-index')
    form_class = PublicidadForm

    def form_valid(self, form):
        model = form.save(commit=False)
        cliente = self.request.user.cliente_set.all()[0].pk
        model.cliente_id = cliente
        model.save()
        return super().form_valid(form)

    def __init__(self, ):
        super(CrearPublicidad, self).__init__()


class ActualizarPublicidad(LoginRequiredMixin, UpdateView):
    """docstring for ClassName"""
    model = Publicidad

    template_name = 'publicidad/nuevo.html'
    success_url = reverse_lazy('gestores:publicidad-index')
    form_class = PublicidadForm

    def form_valid(self, form):
        model = form.save(commit=False)
        cliente = self.request.user.cliente_set.all()[0].pk
        model.cliente_id = cliente
        model.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()
            kwargs['img'] = self.object.imagen
        return super().get_context_data(**kwargs)

    def __init__(self):
        super(ActualizarPublicidad, self).__init__()


class ConsultaPublicidad(LoginRequiredMixin, DetailView):
    """docstring for ClassName"""
    model = Publicidad
    template_name = 'publicidad/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'Eliminar' in request.get_full_path():
            context = self.get_context_data(object=self.object, eliminar=True)
        return self.render_to_response(context)

    def __init__(self):
        super(ConsultaPublicidad, self).__init__()


class EliminarPublicidad(LoginRequiredMixin, DeleteView):
    """docstring for ClassName"""
    model = Publicidad

    def post(self, request, *args, **kwargs):
        borrar = request.POST.get('id')

        if self.model.objects.get(pk=borrar).delete():
            dato = 1
        else:
            dato = 0

        return HttpResponse(json.dumps(dato), content_type='application/json')

    def __init__(self):
        super(EliminarPublicidad, self).__init__()


class ListarEnlace(LoginRequiredMixin, ListView):
    """docstring for ClassName"""
    model = Enlace
    template_name = 'enlace/index.html'
    paginate_by = 50

    def get(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Cliente.objects.get(id=as_user['as_usuario'])
        else:
            cliente = request.user.cliente_set.all()[0]
        search = request.GET.get('q')

        if search:
            filtro = self.model.objects.filter(Q(cliente=cliente) & (
                    Q(nombre__icontains=search) |
                    Q(url__icontains=search)
            )
                                               ).order_by('nombre')

            self.object_list = filtro
        else:
            self.object_list = self.model.objects.filter(
                cliente=cliente).order_by('nombre')

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        return self.render_to_response(context)

    def __init__(self):
        super(ListarEnlace, self).__init__()


class CrearEnlace(LoginRequiredMixin, CreateView):
    """docstring for CrearServicio"""

    model = Enlace
    template_name = 'enlace/nuevo.html'
    success_url = reverse_lazy('gestores:enlace-index')
    form_class = EnlaceForm

    def form_valid(self, form):
        model = form.save(commit=False)
        cliente = self.request.user.cliente_set.all()[0].pk
        model.cliente_id = cliente
        model.save()
        return super().form_valid(form)

    def __init__(self, ):
        super(CrearEnlace, self).__init__()


class ActualizarEnlace(LoginRequiredMixin, UpdateView):
    """docstring for ClassName"""

    model = Enlace
    template_name = 'enlace/nuevo.html'
    success_url = reverse_lazy('gestores:enlace-index')
    form_class = EnlaceForm

    def form_valid(self, form):
        model = form.save(commit=False)
        cliente = self.request.user.cliente_set.all()[0].pk
        model.cliente_id = cliente
        model.save()
        return super().form_valid(form)

    def __init__(self):
        super(ActualizarEnlace, self).__init__()


class ConsultaEnlace(LoginRequiredMixin, DetailView):
    """docstring for ClassName"""
    model = Enlace
    template_name = 'enlace/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'Eliminar' in request.get_full_path():
            context = self.get_context_data(object=self.object, eliminar=True)
        return self.render_to_response(context)

    def __init__(self):
        super(ConsultaEnlace, self).__init__()


class EliminarEnlace(LoginRequiredMixin, DeleteView):
    """docstring for ClassName"""
    model = Enlace

    def post(self, request, *args, **kwargs):
        borrar = request.POST.get('id')
        if self.model.objects.get(pk=borrar).delete():
            dato = 1
        else:
            dato = 0

        return HttpResponse(json.dumps(dato), content_type='application/json')

    def __init__(self):
        super(EliminarEnlace, self).__init__()


class Cumpleanhos(LoginRequiredMixin, ListView):
    """docstring for Cumpleanhos"""
    model = Usuario
    template_name = 'otros/index-cumpleanhos.html'
    paginate_by = 50
    query = None

    def get(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Cliente.objects.get(id=as_user['as_usuario'])
        else:
            cliente = request.user.cliente_set.all()[0]
        search = request.GET.get('q')
        date = request.GET.get('date')
        if date:
            self.object_list = self.model.objects.filter(eliminado=False, cliente=cliente,
                                                         fecha_nacimiento=date).order_by('-fecha_nacimiento')
            date = parse_date(date)
        else:
            date = datetime.date.today()
            if search:
                filtro = self.model.objects.filter(Q(eliminado=False) & Q(cliente=cliente) &
                                                   (
                                                           Q(last_name__icontains=search) |
                                                           Q(barrio__icontains=search) |
                                                           Q(ciudad__nombre__icontains=search) |
                                                           Q(codigo__icontains=search) |
                                                           Q(complemento__icontains=search) |
                                                           Q(cuadrante_via__icontains=search) |
                                                           Q(cuadrante_via_generadora__icontains=search) |
                                                           Q(numero_placa__icontains=search) |
                                                           Q(email__icontains=search) |
                                                           Q(extencion__icontains=search) |
                                                           Q(telefono__icontains=search) |
                                                           Q(first_name__icontains=search) |
                                                           Q(fecha_nacimiento__icontains=search) |
                                                           Q(numero_documento__icontains=search)
                                                   )
                                                   ).order_by('fecha_nacimiento')
                self.object_list = filtro
            else:
                self.object_list = self.model.objects.filter(eliminado=False, cliente=cliente).order_by(
                    'fecha_nacimiento')

        # allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        context['month'] = date
        return self.render_to_response(context)


class CrearCampania(LoginRequiredMixin, CreateView):
    """docstring for CrearServicio"""
    model = Campana
    template_name = 'campania/nuevo.html'
    success_url = reverse_lazy('gestores:campania-index')
    form_class = CampanaForm

    def form_valid(self, form):
        model = form.save(commit=False)
        cliente = self.request.user.cliente_set.all()[0].pk
        responsable = Usuario.objects.get(id=form.data['responsableCampana'])
        model.asesor = responsable.asesor
        codigo = self.codi()
        consulta = self.model.objects.filter().all()  # Datos Campania
        codigos = [campana.codigoCampana for campana in consulta]
        # ******* avoid duplicated codes *******
        while codigo in codigos:
            codigo = self.codi()
        # ******* asign code and save model *******
        model.cliente_id = cliente
        model.codigoCampana = codigo
        model.save()
        user_logged = self.request.user
        # ******* add to logger *******
        logger.info('campana con codigo: {}, ha sido CREADA por usuario con email: {}'.format(codigo, user_logged))
        # ******* send email *****
        print(responsable.id)
        code_to_user.delay(responsable.id, codigo)  # sends email to responsable with code location:tasks.py
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(CrearCampania, self).get_form_kwargs()
        as_user = self.request.session.get(settings.USER_SESSION_ID)
        if as_user:
            kwargs['user'] = as_user['as_usuario']
        else:
            kwargs['user'] = self.request.user.cliente_set.all()[0].pk
        return kwargs

    def codi(self):  # 67000 combinations
        letras = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                  'U', 'V', 'W', 'X', 'Y', 'Z']
        valor = random.sample(letras, 2) + random.sample('0123456789', 2)
        codigo = ''.join(map(str, valor))
        return codigo

    def __init__(self):
        super(CrearCampania, self).__init__()


# Campaña 12
class ListarCampania(LoginRequiredMixin, ListView):
    """docstring for ClassName"""
    model = Campana
    template_name = 'campania/index.html'
    paginate_by = 50

    def get(self, request, *args, **kwargs):
        set_empty = 1
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Cliente.objects.get(id=as_user['as_usuario'])
        else:
            cliente = request.user.cliente_set.all()[0]
        search = request.GET.get('q')
        if search:
            filtro = self.model.objects.filter(Q(cliente=cliente) &
                                               (
                                                       Q(nombre__icontains=search) |
                                                       Q(codigoCampana__icontains=search) |
                                                       Q(descripcion__icontains=search) |
                                                       Q(fecha_inicio__icontains=search) |
                                                       Q(fecha_fin__icontains=search) |
                                                       Q(estado__icontains=search)
                                               )
                                               ).order_by('nombre')
            self.object_list = filtro
        else:
            self.object_list = self.model.objects.filter(cliente=cliente) \
                .order_by('nombre')
            set_empty = 0

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        context['empty_set'] = set_empty
        return self.render_to_response(context)


class ActualizarCampana(LoginRequiredMixin, UpdateView):
    """docstring for ClassName"""
    model = Campana
    template_name = 'campania/nuevo.html'
    success_url = reverse_lazy('gestores:campania-index')
    form_class = CampanaForm

    def form_valid(self, form):
        model = form.save(commit=False)
        today = datetime.datetime.now().strftime('%Y-%m-%d')
        end_date = self.request.POST['fecha_fin']
        codigo = model.codigoCampana
        user_logged = self.request.user
        print(codigo)
        cliente = self.request.user.cliente_set.all()[0].pk
        model.cliente_id = cliente
        # ******* check if campana still active *******
        if end_date > today:
            model.estado = True
        else:
            model.estado = False
        # ******* add to logger *******
        model.cliente_id = cliente
        model.save()
        logger.info('campana {} con codigo: {}, ha sido MODIFICADA por usuario con email: {}'.
                    format(model.nombre, codigo, user_logged))
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(ActualizarCampana, self).get_form_kwargs()
        kwargs['user'] = self.request.user.cliente_set.all()[0].pk
        return kwargs

    def __init__(self):
        super(ActualizarCampana, self).__init__()


class ConsultarCampana(LoginRequiredMixin, DetailView):
    """docstring for ClassName"""
    model = Campana
    template_name = 'campania/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'Eliminar' in request.get_full_path():
            context = self.get_context_data(object=self.object, eliminar=True)
        return self.render_to_response(context)

    def __init__(self):
        super(ConsultarCampana, self).__init__()


class EliminarCampana(LoginRequiredMixin, DeleteView):
    """docstring for ClassName"""
    model = Campana

    def post(self, request, *args, **kwargs):
        id_borrar = request.POST.get('id')
        campana = self.model.objects.get(id=id_borrar)
        valor = ""
        if not campana.responsableCampana:
            valor = str(campana.responsableCampana)
            dato = 0
        else:
            user_logged = self.request.user
            campana.delete()
            dato = 1
            # ******* add to logger *******
            logger.info('campana con codigo: {}, ha sido ELIMINADA por usuario con email: {}'.
                        format(campana.codigoCampana, user_logged))

        envio = [{'val': dato, 'name': valor}]
        return HttpResponse(json.dumps(envio), content_type='application/json')

    def __init__(self):
        super(EliminarCampana, self).__init__()


class ListarProducto(LoginRequiredMixin, ListView):
    """docstring for ClassName"""
    model = Producto
    template_name = 'productos/index.html'
    paginate_by = 50

    def get(self, request, *args, **kwargs):
        set_empty = 1
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente_id = as_user['as_usuario']
        else:
            cliente_id = request.user.cliente_set.all()[0].id
        search = request.GET.get('q')
        if search:
            filtro = self.model.objects.filter(Q(servicio__cliente__id=cliente_id) &
                                               (
                                                       Q(nombre__icontains=search) |
                                                       Q(nombre_slug__icontains=search) |
                                                       Q(valor_comision__icontains=search)
                                               )).order_by('nombre')

            self.object_list = filtro
        else:
            self.object_list = self.model.objects.filter(servicio__cliente__id=cliente_id).order_by('nombre')
            if len(self.object_list) == 0:
                set_empty = 0

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        context['empty_set'] = set_empty
        return self.render_to_response(context)


class CrearProducto(LoginRequiredMixin, CreateView):
    """docstring for CrearProducto"""
    model = Producto
    template_name = 'productos/nuevo.html'
    success_url = reverse_lazy('gestores:producto-index')
    form_class = ProductoForm

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(CrearProducto, self).get_form_kwargs()
        kwargs['user'] = self.request.user.cliente_set.all()[0].pk
        return kwargs

    def form_valid(self, form):
        if form.is_valid():
            prod = form.save()
            loggerProd.info('Producto: {}, con codigo: {}, ha sido AGREGADO por usuario con email: {}'.
                        format(prod.nombre, prod.code, Cliente.objects.get(id=self.cliente_id).email))
            return super().form_valid(form)

    def __init__(self, ):
        super(CrearProducto, self).__init__()


class ConsultarProducto(LoginRequiredMixin, DetailView):
    model = Producto
    template_name = 'productos/detalle.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'Eliminar' in request.get_full_path():
            context = self.get_context_data(object=self.object, eliminar=True)
        return self.render_to_response(context)

    def __init__(self):
        super(ConsultarProducto, self).__init__()


class ActualizarProducto(LoginRequiredMixin, UpdateView):
    """docstring for ClassName"""
    model = Producto
    template_name = 'productos/nuevo.html'
    success_url = reverse_lazy('gestores:producto-index')
    form_class = ProductoForm

    def get_form_kwargs(self):
        kwargs = super(ActualizarProducto, self).get_form_kwargs()
        kwargs['user'] = self.request.user.cliente_set.all()[0].pk
        return kwargs

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        if form.is_valid():
            prod = form.save()
            loggerProd.info('Producto: {}, con codigo: {}, ha sido MODIFICADO por usuario con email: {}'.
                            format(prod.nombre, prod.code, Cliente.objects.get(id=self.cliente_id).email))
            return super().form_valid(form)

class EliminarProducto(LoginRequiredMixin, DeleteView):
    model = Producto

    def post(self, request, *args, **kwargs):
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            self.cliente_id = as_user['as_usuario']
        else:
            self.cliente_id = request.user.cliente_set.all()[0].id
        prod = self.model.objects.get(id=request.POST.get('id'))
        prod.delete()
        try:
            _prod_del = self.model.objects.get(id=request.POST.get('id'))
            loggerProd.error('*ERROR** Producto: {}, con codigo: {}, NO PUDO SER ELIMINADO por usuario con email: {}'.
                            format(prod.nombre, prod.code, Cliente.objects.get(id=self.cliente_id).email))
        except ObjectDoesNotExist:
            loggerProd.info('Producto: {}, con codigo: {}, ha sido ELIMINADO por usuario con email: {}'.
                            format(prod.nombre, prod.code, Cliente.objects.get(id=self.cliente_id).email))
        dato = 1
        return HttpResponse(json.dumps(dato), content_type='application/json')

    def __init__(self):
        super(EliminarProducto, self).__init__()


def product_upload(request):
    template = 'productos/cargar.html'
    prompt = {'orden': 'El orden de las columnas debe ser: Producto || Valor comision (sin decimales, ej. $12,000) || '
                       'Valor Comision (sin formato ej. 12000). Por favor incluir encabezados'
              }
    if request.method == "GET":
        as_user = request.session.get(settings.USER_SESSION_ID)
        if as_user:
            cliente = Usuario.objects.get(id=as_user['as_usuario'])
            services = Servicio.objects.filter(cliente=cliente)
        else:
            cliente = request.user.cliente_set.all()[0]
            services = Servicio.objects.filter(cliente=cliente)
        prompt['services'] = services
        return render(request, template, prompt)

    if request.method == "POST":
        csv_file = request.FILES['file']
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'El archivo debe ser formato .CVS')
            print("file error")
            return redirect(reverse('gestores:producto-upload'))
        data_set = csv_file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string)
        products = csv.reader(io_string, delimiter=",", quotechar="|")
        if not request.is_ajax():
            service = request.POST['service']
            servicio = Servicio.objects.get(id=service)

            for column in products:
                try:
                    obj = Producto.objects.get(servicio=servicio, nombre=column[0])
                    obj.valor_comision_str = column[1]
                    obj.valor_comision = int(column[2])
                    obj.save()
                    loggerProd.info('Producto: {}, con codigo: {}, ha sido MODIFICADO por usuario con email: {}'.
                                    format(obj.nombre, obj.code, request.user))
                except Producto.DoesNotExist:
                    obj = Producto(servicio=servicio, nombre=column[0],
                                   valor_comision_str=column[1], valor_comision=int(column[2]))
                    obj.save()
                    loggerProd.info('Producto: {}, con codigo: {}, ha sido AGREGADO por usuario con email: {}'.
                                    format(obj.nombre, obj.code, request.user))
            return HttpResponseRedirect(reverse('gestores:producto-index'))
        data = {}
        i = 0
        for prod in products:
            data['a'+str(i)] = [prod[0], prod[1], prod[2]]
            i += 1
        return HttpResponse(json.dumps(data), content_type='application/json')
    context = {}
    return render(request, template, context)


def birth_mail(request, user_id):
    user = Usuario.objects.get(id=user_id)
    user.email_sent = True
    user.save()
    user_birthday.delay(user_id)
    return HttpResponseRedirect(reverse('gestores:cumpleanhos'))


def download_file(request, filepath):
    # fill these variables with real values
    fl_path = 'media/{}'.format(filepath)
    filename = 'formato.csv'
    fl = open(fl_path, 'r')
    mime_type, _ = mimetypes.guess_type(fl_path)
    response = HttpResponse(fl, content_type=mime_type)
    response['Content-Disposition'] = "attachment; filename=%s" % filename
    return response