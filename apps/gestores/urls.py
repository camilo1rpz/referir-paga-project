"""Referir_Paga URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views
from .views import *

app_name = "gestores"
urlpatterns = [
    path('nuevo-servicio/',CrearServicio.as_view(), name="servicio-nuevo"),
    path('listar-servicio/',ListarServicio.as_view(), name="servicio-index"),
    path('actualizar-servicio/<int:pk>/',ActualizarServicio.as_view(), name="servicio-actualizar"),
    path('consultar-servicio/<int:pk>/<str:delete>/',ConsultaServicio.as_view(), name="servicio-consultar"),
    path('eliminar-servicio/',EliminarServicio.as_view(), name="servicio-eliminar"),

    path('nuevo-producto/',CrearProducto.as_view(), name="producto-nuevo"),
    path('listar-producto/',ListarProducto.as_view(), name="producto-index"),
    path('subir-producto/',views.product_upload, name="producto-upload"),
    path('actualizar-producto/<int:pk>/',ActualizarProducto.as_view(), name="producto-actualizar"),
    path('consultar-producto/<int:pk>/<str:delete>/',ConsultarProducto.as_view(), name="producto-consultar"),
    path('eliminar-producto/',EliminarProducto.as_view(), name="producto-eliminar"),
    path('productos/<str:filepath>/', views.download_file, name="producto-formato"),

    path('nuevo-publicidad/',CrearPublicidad.as_view(), name="publicidad-nuevo"),
    path('listar-publicidad/',ListarPublicidad.as_view(), name="publicidad-index"),
    path('actualizar-publicidad/<int:pk>/',ActualizarPublicidad.as_view(), name="publicidad-actualizar"),
    path('consultar-publicidad/<int:pk>/<str:delete>/',ConsultaPublicidad.as_view(), name="publicidad-consultar"),
    path('eliminar-publicidad/',EliminarPublicidad.as_view(), name="publicidad-eliminar"),

    path('nuevo-enlace/',CrearEnlace.as_view(), name="enlace-nuevo"),
    path('listar-enlace/',ListarEnlace.as_view(), name="enlace-index"),
    path('actualizar-enlace/<int:pk>/',ActualizarEnlace.as_view(), name="enlace-actualizar"),
    path('consultar-enlace/<int:pk>/<str:delete>/',ConsultaEnlace.as_view(), name="enlace-consultar"),
    path('eliminar-enlace/',EliminarEnlace.as_view(), name="enlace-eliminar"),
    
    path('cumpleaños/', Cumpleanhos.as_view(),  name="cumpleanhos"),
    path('cumpleaños/<int:user_id>/', views.birth_mail, name="birth-mail"),
    #Campania
    path('nuevo-campania/',CrearCampania.as_view(), name="campania-nuevo"),
    path('listar-campania/',ListarCampania.as_view(), name="campania-index"),
    path('actualizar-campania/<int:pk>/',ActualizarCampana.as_view(), name="campania-actualizar"),
    path('eliminar-campania/',EliminarCampana.as_view(), name="campania-eliminar"),
    path('consultar-campania/<int:pk>/<str:delete>/',ConsultarCampana.as_view(), name="campania-consultar"),

]

