import datetime
from celery import task
from celery.schedules import crontab
from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from email.mime.image import MIMEImage
from django.utils.dateparse import parse_date
from apps.gestores.models import Campana
from apps.usuarios.models import Usuario


@task
def user_birthday(user_id):
    """
    Task to send an email when it is users' birthday
    """
    user = Usuario.objects.get(id=user_id)
    data = {'user_name': user.first_name}
    html_content = render_to_string('otros/email_cumple.html', {'data': data})
    subject = 'Feliz cumpleanhos {}!!'.format(user.first_name.title())
    msg = EmailMessage(subject, html_content, 'noreply@mail.tnfactor.com.co', [user.email])
    msg.content_subtype = "html"
    msg.mixed_subtype = "related"

    img_data_1 = open("static/img/fondo-cumple.png", 'rb').read()
    img_data_2 = open("static/img/cumpleanhos-RP.png", 'rb').read()
    img_data_3 = open("static/img/logo-tn.png", 'rb').read()
    img_data_4 = open("static/img/logo-rp.png", 'rb').read()

    imgs_data_list = [img_data_1, img_data_2, img_data_3, img_data_4]
    url_list = ['/static/img/fondo-cumple.png', '/static/img/cumpleanhos-RP.png',
                '/static/img/logo-tn.png', '/static/img/logo-rp.png']
    imgs_dict = dict(zip(imgs_data_list, url_list))

    for img_data in imgs_dict:
        image = MIMEImage(img_data, 'png')
        image.add_header('Content-ID', '<{}>'.format(imgs_dict[img_data]))
        image.add_header("Content-Disposition", "inline", filename=imgs_dict[img_data])
        msg.attach(image)
    msg.send()
    print("email_sent")


@task
def code_to_user(responsable_id, code):
    """
    Task to send an email to responsable campana when a new campana is created
    """
    responsable = Usuario.objects.get(id=responsable_id)
    data = {'user_name': responsable.first_name, 'code': code}
    html_content = render_to_string('otros/email_code_campana.html', {'data': data})
    subject = 'codigo de campania {}'.format(responsable.first_name)
    msg = EmailMessage(subject, html_content, 'noreply@mail.tnfactor.com.co', [responsable.email])
    msg.content_subtype = "html"
    msg.mixed_subtype = "related"

    img_data_1 = open("static/img/fondo-cumple.png", 'rb').read()
    img_data_2 = open("static/img/logo-tn.png", 'rb').read()
    img_data_3 = open("static/img/logo-rp.png", 'rb').read()

    imgs_data_list = [img_data_1, img_data_2, img_data_3]
    url_list = ['/static/img/fondo-cumple.png', '/static/img/logo-tn.png', '/static/img/logo-rp.png']
    imgs_dict = dict(zip(imgs_data_list, url_list))

    for img_data in imgs_dict:
        image = MIMEImage(img_data, 'png')
        image.add_header('Content-ID', '<{}>'.format(imgs_dict[img_data]))
        image.add_header("Content-Disposition", "inline", filename=imgs_dict[img_data])
        msg.attach(image)
    msg.send()
    print("email_sent")


@task(name='see_you_now')
def see_you_now():
    print("See you now!")

@task(name='update_campaign')
def update_campaign():
    campaings = Campana.objects.all()
    today = datetime.datetime.today().strftime('%Y-%m-%d')
    today_date = parse_date(today)
    for campaing in campaings:
        if campaing.fecha_fin < today_date:
            campaing.estado = False
            campaing.save()
    print("Campaigns have been updated!")


@task(name='hello')
def hello():
    print("Hello there! it works, wow!")


