from django.apps import AppConfig


class GestoresConfig(AppConfig):
    name = 'gestores'
