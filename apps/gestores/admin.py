from django.contrib import admin
from .models import *
# Register your models here.

modelos = [
	TipoEnlace,
	Enlace,
	Servicio,
	Publicidad,
	Campana,
	Producto,
	ReferidosApp,
]

admin.site.register(modelos)