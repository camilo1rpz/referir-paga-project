from django.shortcuts import render
#from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from apps.gestores.models import Campana
import datetime
# Create your views here.



class FechaCampana(ListView):
    model = Campana
    template_name = 'tareasProgramadas/index.html'

    def get(self, request, *args, **kwargs):
        self.object_list = self.model.objects.filter(id=3).order_by('nombre')
        
        #print(datetime.datetime.now().strftime('%Y-%m-%d'))
        fechaActual = datetime.datetime.now().strftime('%Y-%m-%d')
        dato = self.model.objects.filter(fecha_fin__lt=fechaActual)
        for x in dato:
           x.estado = False
           x.save()
           pass

        allow_empty = self.get_allow_empty()
        context = self.get_context_data()
        return self.render_to_response(context)
        
    def __init__(self):
        super(FechaCampana, self).__init__()


