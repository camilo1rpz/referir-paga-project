from django.apps import AppConfig


class TareasprogamadasConfig(AppConfig):
    name = 'tareasProgamadas'
