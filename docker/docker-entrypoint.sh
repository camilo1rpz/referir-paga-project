#!/bin/bash
set -e

# read docker secrets into env variables
export DJANGO_SUPERUSER_PASSWORD=$(cat /run/secrets/django_superuser_password)
export DJANGO_SUPERUSER_MAIL=$(cat /run/secrets/django_superuser_mail)
export DB_PASSWORD=$(cat /run/secrets/db_password)
export SECRET_KEY=$(cat /run/secrets/secret_key)
export EMAIL_HOST_USER=$(cat /run/secrets/email_host_user)
export EMAIL_HOST_PASSWORD=$(cat /run/secrets/email_host_password)

# run db migrations (retry on error)
while ! python3 /opt/referir-paga-project/manage.py migrate 2>&1; do
  sleep 5
done

# Create Super if required
if [ "$DJANGO_SKIP_SUPERUSER" == "true" ]; then
  echo "↩️ Skip creating the superuser"
else
  if [ -z ${DJANGO_SUPERUSER_NAME+x} ]; then
    DJANGO_SUPERUSER_NAME='admin'
  fi
  if [ -z ${DJANGO_SUPERUSER_MAIL+x} ]; then
    DJANGO_SUPERUSER_MAIL='rpadmin@tncolombia.com'
  fi
  if [ -z ${DJANGO_SUPERUSER_PASSWORD+x} ]; then
    if [ -f "/run/secrets/django_superuser_password" ]; then
      DJANGO_SUPERUSER_PASSWORD=$DJANGO_SUPERUSER_PASSWORD
    else
      DJANGO_SUPERUSER_PASSWORD='admin'
    fi
  fi

python3 /opt/referir-paga-project/manage.py shell << END
from django.contrib.auth import get_user_model
if not get_user_model().objects.filter(username='${DJANGO_SUPERUSER_NAME}'):
  u = get_user_model().objects.create_superuser('${DJANGO_SUPERUSER_NAME}', '${DJANGO_SUPERUSER_MAIL}', '${DJANGO_SUPERUSER_PASSWORD}')
END
echo "💡 Superuser Username: ${DJANGO_SUPERUSER_NAME}, E-Mail: ${DJANGO_SUPERUSER_MAIL}"
fi

# run python3 manage.py collectstatic
python3 /opt/referir-paga-project/manage.py collectstatic --noinput
# move to referir directory
cd /opt/referir-paga-project/
#start daphne
#daphne -b referir_paga -p 8080 Referir_Paga.asgi:application

#run celery
celery -A Referir_Paga worker --loglevel=info &
celery -A Referir_Paga beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler &


#start unicorn
# start unicorn
gunicorn --log-level debug -c /opt/referir-paga-project/gunicorn_config.py Referir_Paga.wsgi