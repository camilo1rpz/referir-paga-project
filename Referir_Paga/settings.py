"""
Django settings for Referir_Paga project.

Generated by 'django-admin startproject' using Django 2.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ad_)646og8&)=i*8=9()spg-=o%ul=n5z_z=1jtm4-6&^zs^t%'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
    'django_tables2',
    'apps.clientes',
    'apps.gestores',
    'apps.notificaciones',
    'apps.recursos',
    'apps.reportes',
    'apps.sedes',
    'apps.tipificaciones',
    'apps.ubicacion',
    'apps.usuarios',
    'apps.logginas',
    'apps.services',
    'apps.apisReferirPagaApp',
    'apps.tareasProgramadas',
    'sorting_bootstrap',
    'redis',
    'django_celery_results',
    'django_celery_beat',
    # API
    'rest_framework',
    'corsheaders',

]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',  # #Api
    'django.middleware.common.CommonMiddleware',  # #Api
    'django.middleware.security.SecurityMiddleware',  # Api

    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

USER_SESSION_ID = 'view_as'

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_METHODS = (
    'GET',
    'POST',
    'PUT',
    'PATCH',
    'DELETE',
    'OPTIONS'
)

CORS_ALLOW_CREDENTIALS = True

# Django rest framework
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    # 'DEFAULT_PARSER_CLASSES': (
    #    'rest_framework.parsers.JSONParser',
    # ),
    # 'DEFAULT_PERMISSION_CLASSES': (
    #    'rest_framework.permissions.AllowAny'
    # 'rest_framework.permissions.DjangoModelPermissions',
    # ),
    # 'DEFAULT_AUTHENTICATION_CLASSES': (
    #    'rest_framework.authentication.SessionAuthentication',
    #    'rest_framework.authentication.TokenAuthentication',
    # ),
    'DATETIME_FORMAT': "% m /% d /% Y% H:% M:% S",

}

ROOT_URLCONF = 'Referir_Paga.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Referir_Paga.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

# DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': 'mydatabase',
#    }
# }


# DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.postgresql',
#        'NAME': 'referirpaga',
#        'USER': 'postgres',
#        'PASSWORD': 'tncolombia',
#        'HOST': '127.0.0.1',
#        'PORT': '5432',
#    }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DB_NAME', 'test'),
        'USER': os.environ.get('DB_USER', 'test'),
        'PASSWORD': os.environ.get('DB_PASSWORD', 'test'),
        'HOST': os.environ.get('DB_HOST', ''),
        'PORT': '5432',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
    {
        'NAME': 'apps.validators.SymbolValidator',
    },
]

AUTH_USER_MODEL = 'usuarios.Usuario'

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code


LANGUAGE_CODE = 'es-co'
TIME_ZONE = 'America/Bogota'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/


STATIC_URL = '/static/'  # config staticos
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
#STATICFILES_DIRS = [os.path.join(BASE_DIR, "static"), ]  # directorio Staticos

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')  # Sbuir I

LOGIN_URL = "login"
LOGIN_REDIRECT_URL = "index"  # ruta despues de login
LOGOUT_REDIRECT_URL = "login"

DATE_INPUT_FORMATS = ('%d-%m-%Y', '%Y-%m-%d', '%d/%m/%Y', '%Y/%m/%d',)

##email settings
#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#EMAIL_FILE_PATH = os.path.join(BASE_DIR, "sent_emails")
#DEFAULT_FROM_EMAIL = 'noreply@mail.tnfactor.com.co'
EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"
SENDGRID_API_KEY = os.environ.get('EMAIL_HOST_PASSWORD', '')
# Toggle sandbox mode (when running in DEBUG mode)
SENDGRID_SANDBOX_MODE_IN_DEBUG = False

# celery settings
from celery.schedules import crontab

CELERY_TIMEZONE = 'America/Bogota'
CELERY_RESULT_BACKEND = 'django-db'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_CACHE_BACKEND = 'django-cache'

# Let's make things happen
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers.DatabaseScheduler'
CELERY_BEAT_SCHEDULE = {
    "campana-activo-actualizar": {
        "task": "update_campaign",
        "schedule": crontab(hour=17, minute=55)
    },
    'hello': {
        'task': 'hello',
        'schedule': crontab()  # execute every minute
    }
}

from datetime import datetime

# LOGGING CONFIGURATION
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },

    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './logs/campana_crud_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
        'file2': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './logs/cliente_crud_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
        'file3': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './logs/usuarios_crud_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
        'file4': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './logs/productos_crud_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
        'errors': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': './logs/gestores_error_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
        'file5': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './logs/services_crud_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
        'file6': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './logs/regional_crud_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
        'errors_sedes': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': './logs/sedes_error_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
        'file7': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './logs/pto_ope_crud_{:%m-%Y}.log'.format(datetime.now()),
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'apps.gestores.views': {
            'handlers': ['file', 'errors'],
            'level': 'INFO',
            'propagate': True,
        },
        'apps.clientes.views': {
            'handlers': ['file2', 'errors'],
            'level': 'INFO',
            'propagate': True,
        },
        'apps.usuarios.views': {
            'handlers': ['file3', 'errors'],
            'level': 'INFO',
            'propagate': True,
        },
        'prod_log': {
            'handlers': ['file4', 'errors'],
            'level': 'INFO',
            'propagate': True,
        },
        'serv_log': {
            'handlers': ['file5', 'errors'],
            'level': 'INFO',
            'propagate': True,
        },
        'regional_log': {
            'handlers': ['file6', 'errors_sedes'],
            'level': 'INFO',
            'propagate': True,
        },
        'pto_ope_log': {
            'handlers': ['file7', 'errors_sedes'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}
