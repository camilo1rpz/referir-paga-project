"""Referir_Paga URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from rest_framework import routers
from apps.apisReferirPagaApp import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# from myapp.views import UserViewSet

# Apis
from apps.usuarios.forms import PasswordResetForm2

router = routers.SimpleRouter()
router.register(r'loginApp', views.LoginSerializer, basename='loginApp'),
router.register(r'usuarioApp', views.UsuarioSerializer, basename='usuarioApp'),
router.register(r'usuarioReg', views.UsuarioUpdateSerializer, basename='usuarioReg'),
router.register(r'pais', views.PaisSerializer),
router.register(r'ciudad', views.CiudadSerializer),
router.register(r'documento', views.DocumentoSerializer, basename='documento'),
router.register(r'listaEmpresa', views.ClienteEmpresa, basename='listaEmpresa'),
router.register(r'empresa', views.EmpresaCliente, basename='empresa'),
router.register(r'codigo', views.CodigoSerializer, basename='codigo'),
router.register(r'validaCodigo', views.ValidacionCodigo, basename='validaCodigo'),
router.register(r'servicio', views.ServiciosSerializer, basename='servicio'),
router.register(r'enlace', views.EnlacesClienteSerializer, basename='enlace'),
router.register(r'lisUsuario', views.ListarUsuario, basename='lisUsuario'),
router.register(r'referir', views.ReferidoAppSerializer, basename='referir'),

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('i18n/', include('django.conf.urls.i18n')),
                  path('', include('apps.recursos.urls')),
                  path('loggin/', include('apps.logginas.urls', namespace="logginas"),),
                  path('cliente/', include('apps.clientes.urls', namespace="clientes"), ),
                  path('usuario/', include('apps.usuarios.urls', namespace="usuarios"), ),
                  path('sede/', include('apps.sedes.urls', namespace="sedes"), ),
                  path('services/', include('apps.services.urls', namespace="services"), ),
                  path('gestores/', include('apps.gestores.urls', namespace="gestores"), ),
                  path('reportes/', include('apps.reportes.urls', namespace="reportes"), ),
                  path('password_reset/', auth_views.PasswordResetView.as_view(template_name='otros/send_email.html',
                                                                               html_email_template_name='otros/template_email.html',
                                                                               form_class=PasswordResetForm2),
                       name="password_reset"),
                  path('password_reset/done/',
                       auth_views.PasswordResetDoneView.as_view(template_name='otros/password_reset_done.html'),
                       name="password_reset_done"),
                  path('reset/<uidb64>/<token>/',
                       auth_views.PasswordResetConfirmView.as_view(template_name="otros/password_reset_confirm.html"),
                       name="password_reset_confirm"),
                  path('reset/done/',
                       auth_views.PasswordResetCompleteView.as_view(template_name="otros/password_reset_complete.html"),
                       name="password_reset_complete"),

                  # APis
                  path('', include(router.urls)),
                  path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
                  # TareasProgramadas
                  path('tarea/', include('apps.tareasProgramadas.urls', namespace="tareaPro"), )


              ]
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)