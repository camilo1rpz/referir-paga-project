/*
//Ocultar y Desabilitar Campos al Editar
if( $("#editarInfo").val() == 'editar' ){
    $("#id_tipo_identificacion").css('pointer-events','none');
    $("#id_identificacion").attr('readonly','readonly');
    $("#id_email").attr('readonly','readonly');
   
}else{
    $("#btnLimpiar").css('display','block');
    $("#divLogo").show();    

    function archivo(evt) {
        var files = evt.target.files; 
        //Obtenemos la imagen del campo "file". 
        for (var i = 0, f; f = files[i]; i++) {         
           //Solo admitimos imágenes.
           if (!f.type.match('image.*')) {
                continue;
           }
           var reader = new FileReader();
           reader.onload = (function(theFile) {
               return function(e) {
               // Creamos la imagen.
               document.getElementById("verImagen").innerHTML = ['<img class="img-circle" style="border: 4px inset #878587;" width="130" height="110" src="', e.target.result,'" title="',escape(theFile.name),'"/>'].join('');
               };
           })(f);  
           reader.readAsDataURL(f);
        }
    }
    document.getElementById('id_logo').addEventListener('change', archivo, false);
}
*/

/*
var required = $('input,select').filter('[required]:visible');
var allRequired = true;
required.each(function(){
    if($(this).val() == ''){
        allRequired = false;
    }
});

if(!allRequired){
    //DO SOMETHING HERE... POPUP AN ERROR MESSAGE, ALERT , ETC.
}
*/



$("#id_tipo_identificacion").change(function() {
    if (($("#id_tipo_identificacion option:selected").val() == 4) || ($("#id_tipo_identificacion option:selected").val() == 7)) {
        $("#divTipoSociedad").css('display', 'block');
        $("#divRegimenfiscal").css('display', 'block')
        $("#divNameContacto ").css('display', 'block');
        $("#divApellido").css('display', 'none');
        
        //Pone Required
        $("#id_nombre_contacto").attr('required',true);
        $("#id_tipo_sociedad").attr('required',true);
        $("#id_regimenfiscal").attr('required',true);
    }else {
        $("#divTipoSociedad").css('display', 'none');
        $("#divRegimenfiscal").css('display', 'none');
        $("#divNameContacto ").css('display', 'none');
        $("#divApellido").removeAttr('style');
        //Campos Ocultos
        $("#id_nombre_contacto").val('');
        $("#id_tipo_sociedad option[value='']").prop('selected',true);
        $("#id_regimenfiscal option[value='']").prop('selected',true);

        //Quitar Required
        $("#id_nombre_contacto").attr('required',false);
        $("#id_tipo_sociedad").attr('required',false);
        $("#id_regimenfiscal").attr('required',false);
    }
    //$("input:not[(name=csrfmiddlewaretoken)] ").val('');
});

if ( ($("#id_tipo_identificacion option:selected").val() == 4) || ($("#id_tipo_identificacion option:selected").val() == 7)) {
    $("#divTipoSociedad").css('display', 'block');
    $("#divRegimenfiscal").css('display', 'block');
    $("#divNameContacto").css('display', 'block');

//    $("#id_nombre_contacto").addClass('selectValidate');
//    $("#id_tipo_sociedad").addClass('selectValidate');
//    $("#id_regimenfiscal").addClass('selectValidate');
    $("#divApellido").css('display', 'none');
}else{
    
}

//logo Tamaño,Extencion
$(document).on('change','input[type="file"]',function(){
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 1000000) {
        Swal.fire(
            'Advertencia ..!',
            'El archivo no debe superar 1MegaByte de Peso',
            'warning',
        )
        this.value = '';
        this.files[0].name = '';
    }else {
        var ext = fileName.split('.').pop();
        switch (ext) {
            case 'jpg':
            case 'jpeg':
            case 'png':
                break;
            default:
                Swal.fire(
                    'Advertencia ..!',
                    'Archivo No Tiene la Extensión Adecuada',
                    'warning',
                )
                this.value = '';
                this.files[0].name = '';
        }
    }
});
 

//Estado Actual
var estado = $("#id_estado").is(":checked");
if( estado ){
    $("#myonoffswitch").attr('checked',true);
}else{
    $("#myonoffswitch").attr('checked',false);
}

//New Estado
$("#myonoffswitch").change(function(){
    if($(this).prop("checked") == true){
        $("#id_estado").prop('checked',true);
    }else{
        $("#id_estado").attr('checked',false);
    }
});

$("#btnCerrarModal").click(function(){
    $("#exampleModal").modal('hide');
});

$("#id_via").change(function(){
    if( $("#id_via").is(":checked") ){
        $("#id_via").attr('value','Bis');
        $("#id_bis_via").prop('checked',true);//checked 1 Oculto
    }else{
        $("#id_via").attr('value','');
        $("#id_bis_via").prop('checked',false);
    }
}); 

$("#id_via2").change(function(){
    if( $("#id_via2").is(":checked") ){
        $("#id_via2").attr('value','Bis');
        $("#id_bis_via_generadora").prop('checked',true);//checked 2 Oculto
    }else{
        $("#id_via2").attr('value','');
        $("#id_bis_via_generadora").prop('checked',false);
    }
}); 

//Validacion Email
$("#id_email").focus(function(){
    $("#id_email").css("border-color","#b03535").css('box-shadow','0 0 5px #d45252').css("background", "transparent url('../../static/imagen-cliente/iconoRojo.png') no-repeat right");  
});
$("#id_ConfirmarEmail").focus(function(){
    $("#id_ConfirmarEmail").css("border-color","#b03535").css('box-shadow','0 0 5px #d45252').css("background", "transparent url('../../static/imagen-cliente/iconoRojo.png') no-repeat right");  
});

$("#id_email").blur(function(){
    var valor = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    if ( !valor.test( $('#id_email').val().trim() ) ) {
        Swal.fire(
            '¡Atención ...!',
            '¡No Es Un Correo, Validelo ..!',
            'warning',
        )
    }else{
        $("#id_email").css("border-color","#28921f").css('box-shadow','0 0 5px #5cd053').css("background", "transparent url('../../static/imagen-cliente/IconGreen.png') no-repeat right");
    } 

});

$("#id_ConfirmarEmail").blur(function(){
    var mail = $("#id_email").val();
    var mail2 = $("#id_ConfirmarEmail").val();
    if( mail != mail2 ){
       Swal.fire(
            '¡Atención ...!',
            '¡Correos Diferentes, Validelo ..!',
            'warning',
        )
    }else{
        $("#id_ConfirmarEmail").css("border-color","#28921f").css('box-shadow','0 0 5px #5cd053').css("background", "transparent url('../../static/imagen-cliente/IconGreen.png') no-repeat right");
    }
});

$("#id_ConfirmarEmail").attr('onpaste','return false');

//Modal
var formData;
$("#btnAceptarModal").click(function(){
    if( $("#idTipo").val() == '' ){
        Swal.fire(
            'Advertencia ..!',
            'Seleccione Un Tipo',
            'warning',
        )
    }else if( ( $("#numCalle").val().length == '' ) | ( $("#numCalle").val().length > 3 ) ){
        Swal.fire(
            'Advertencia 1-3 Numeros !',
            'Escriba El Numero De Dirección',
            'warning',
        )
        $("#numCalle").focus();
    }else if( ( $("#numCalle2").val().length == '' ) | ( $("#numCalle2").val().length > 3 ) ){
        Swal.fire(
            'Advertencia 1-3 Numeros !',
            'Escriba El Segundo Numero De Dirección',
            'warning',
        )
        $("#numCalle2").focus();
    }else if( ($("#idPlaca").val().length == '') | ( $("#idPlaca").val().length > 3 ) ){
        Swal.fire(
            'Advertencia 1-3 Digitos',
            'Escriba Una Placa ',
            'warning',
        )
        $("#idPlaca").focus();
    }else{
        formData = '';
        $('#formModal').find('input, textarea, select').each(function(i, field) {
            formData += field.value+' ';
            //console.log(formData)
        });
        $("#direccion2").val( formData );
        //Asigno Valores Campos Ocultos
        $("#id_tipo_via").val( $("#idTipo").val() );
        $("#id_numero_via").val( $("#numCalle").val() );
        $("#id_prefijo_via").val( $("#idPrefijoVia option:selected").val() );
        $("#id_cuadrante_via").val( $("#idCuadranteVia").val() );
        $("#id_numero_via_generadora").val( $("#numCalle2").val() );
        $("#id_prefijo_via_generadora").val( $("#idPrefijo2").val() );
        $("#id_numero_placa").val( $("#idPlaca").val() );
        $("#id_cuadrante_via_generadora").val( $("#idCuadrante").val() );
        $("#id_complemento").val( $("#ModalComplemento").val() );
        $("#id_barrio").val( $("#ModalBarrio").val() );
        
        //Asigno Valores Campos Ocultos
        $("#exampleModal").modal('hide');
        $("#spanComplemento").html( $("#id_complemento").val() );
        $("#spanBarrio").html( $("#id_barrio").val() );


        var dir = $("#id_tipo_via").val();
        var nu = $("#id_numero_via").val();
        var pre = $("#id_prefijo_via").val();
        var check='';
        var num2 = $("#id_numero_via_generadora").val();
        var numVia = $("#id_cuadrante_via").val();
        var pre2 = $("#id_prefijo_via_generadora").val();
        var check2 = '';
        var hash = '#';
        var ralla = '-';
        var plac = $("#id_numero_placa").val();
        var cuadra = $("#id_cuadrante_via_generadora").val();

        //Modal
        $("#idTipo").val( dir );
        $("#numCalle").val( nu );
        $("#idPrefijoVia").val( pre );
        $("#idCuadranteVia").val( numVia );
         
        $("#numCalle2").val( num2 );
        $("#idPrefijo2").val( pre2 );
        $("#idPlaca").val( plac );
        $("#idCuadrante").val( cuadra );
        $("#ModalBarrio").val( $("#id_barrio").val() );
        $("#ModalComplemento").val( $("#id_complemento").val() );

        if( $("#id_bis_via").is(':checked') ){
            check = 'Bis';
            $("#id_via").prop('checked',true);
            $("#id_via").val( check );
        }
        if( $("#id_bis_via_generadora").is(':checked') ){
            check2 = 'Bis';
            $("#id_via2").prop('checked',true);
            $("#id_via2").val( check2 );
        }

        $("#spanComplemento").html( $("#id_complemento").val() );
        $("#spanBarrio").html( $("#id_barrio").val() );

        $("#direccion2").val( dir+' '+nu+' '+pre+' '+check+' '+numVia+' '+hash+' '+num2+' '+pre2+' '+check2+' '+ralla+' '+plac+' '+cuadra );
        $("#direccion2").attr('readonly','readonly');
    }
});

//Limpiar Campos
$("#btnCerrarModal").click(function(){
   // $("#idTipo option:selected").text('Seleccione Tipo');
    $("#idTipo option[value='']").prop('selected',true);
    $("#numCalle").val('');
    $("#idPrefijoVia option[value='']").prop('selected',true);
    $("#id_via").prop('checked',false);
    $("#idCuadranteVia option[value='']").prop('selected',true);
    $("#numCalle2").val('');
    $("#idPrefijo2 option[value='']").prop('selected',true);
    $("#id_via2").prop('checked',false);
    $("#idPlaca").val('');
    $("#idCuadrante option[value='']").prop('selected',true);
    $("#ModalComplemento").val('');
    $("#ModalBarrio").val('');
    $("#id_bis_via_generadora").prop('checked',false);
    $("#id_bis_via").prop('checked',false);
});

$("#id_identificacion").blur(function(){
    var num = $("#id_identificacion").val();
    if( (num.length < 5 ) || ( num.length > 15 )){
        Swal.fire({
            title: '¡Atención!',
            text: "Indentificacion entre 5-15 Caracteres",
            type: 'warning',
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
               $("#id_form").focus();
            }
        });
    }
});

$("#id_nombre_o_razon").blur(function(){
    var num = $("#id_nombre_o_razon").val();
    if( num.length < 2 ){
        Swal.fire({
            title: '¡Atención!',
            text: "Nombre muy corto",
            type: 'warning',
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
               $("#id_form").focus();
            }
        });
    }
});

$("#id_nombre_app").blur(function(){
    var num = $("#id_nombre_app").val();
    if( num.length < 2 ){
        Swal.fire({
            title: '¡Atención!',
            text: "Nombre muy corto",
            type: 'warning',
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
               $("#id_form").focus();
            }
        });
    }
});

$("#id_apellido").blur(function(){
    var num = $("#id_apellido").val();
    if( num.length < 2 ){
        Swal.fire({
            title: '¡Atención!',
            text: "Apellido muy corto",
            type: 'warning',
            allowOutsideClick: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
        }).then((result) => {
            if (result.value) {
               $("#id_form").focus();
            }
        });
    }
});

