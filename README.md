# README #
1. crear entorno virtual virtualenv 		[ virtualenv -p python3 src ]
2. activar el entorno virtual  			[ source src/bin/activate ]
3. Instalar paquete dce requerimientos.txt 	[ pip install -r Requerimientos.txt ]
4. Montar base de datos ReferirPaga.sql 	[ con usuario postgrest checkear settings.py ]
5. correr servidor				[ python manage.py runserver ]
6. si es necesario ejecutar migraciones		[ python manage.py migrate ]
 
