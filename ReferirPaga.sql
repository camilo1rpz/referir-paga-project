--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Debian 11.4-1)
-- Dumped by pg_dump version 11.4 (Debian 11.4-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: clientes_cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clientes_cliente (
    id integer NOT NULL,
    nombre_o_razon character varying(50) NOT NULL,
    nombre_contacto character varying(50),
    identificacion character varying(50) NOT NULL,
    email character varying(254) NOT NULL,
    apellido character varying(50),
    logo character varying(100),
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    ciudad_id integer,
    regimenfiscal_id integer,
    tipo_organizacion_id integer,
    tipo_sociedad_id integer,
    telefono character varying(15) NOT NULL,
    departamento_id integer,
    pais_id integer,
    estado boolean NOT NULL,
    tipo_identificacion_id integer,
    barrio character varying(512),
    bis_via boolean NOT NULL,
    bis_via_generadora boolean NOT NULL,
    complemento character varying(512),
    cuadrante_via character varying(12),
    cuadrante_via_generadora character varying(12),
    numero_placa character varying(4),
    numero_via integer,
    numero_via_generadora integer,
    prefijo_via character varying(12),
    prefijo_via_generadora character varying(12),
    tipo_via character varying(12),
    eliminado boolean NOT NULL
);


ALTER TABLE public.clientes_cliente OWNER TO postgres;

--
-- Name: clientes_cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clientes_cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_cliente_id_seq OWNER TO postgres;

--
-- Name: clientes_cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clientes_cliente_id_seq OWNED BY public.clientes_cliente.id;


--
-- Name: clientes_cliente_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clientes_cliente_usuario (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    usuario_id integer NOT NULL
);


ALTER TABLE public.clientes_cliente_usuario OWNER TO postgres;

--
-- Name: clientes_cliente_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clientes_cliente_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_cliente_usuario_id_seq OWNER TO postgres;

--
-- Name: clientes_cliente_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clientes_cliente_usuario_id_seq OWNED BY public.clientes_cliente_usuario.id;


--
-- Name: clientes_regimenfiscal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clientes_regimenfiscal (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL
);


ALTER TABLE public.clientes_regimenfiscal OWNER TO postgres;

--
-- Name: clientes_regimenfiscal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clientes_regimenfiscal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_regimenfiscal_id_seq OWNER TO postgres;

--
-- Name: clientes_regimenfiscal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clientes_regimenfiscal_id_seq OWNED BY public.clientes_regimenfiscal.id;


--
-- Name: clientes_tipoorganizacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clientes_tipoorganizacion (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL
);


ALTER TABLE public.clientes_tipoorganizacion OWNER TO postgres;

--
-- Name: clientes_tipoorganizacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clientes_tipoorganizacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_tipoorganizacion_id_seq OWNER TO postgres;

--
-- Name: clientes_tipoorganizacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clientes_tipoorganizacion_id_seq OWNED BY public.clientes_tipoorganizacion.id;


--
-- Name: clientes_tiposociedad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clientes_tiposociedad (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL
);


ALTER TABLE public.clientes_tiposociedad OWNER TO postgres;

--
-- Name: clientes_tiposociedad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clientes_tiposociedad_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_tiposociedad_id_seq OWNER TO postgres;

--
-- Name: clientes_tiposociedad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clientes_tiposociedad_id_seq OWNED BY public.clientes_tiposociedad.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: gestores_campana; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gestores_campana (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    inicio timestamp with time zone NOT NULL,
    fin timestamp with time zone NOT NULL,
    descripcion text NOT NULL,
    estado boolean NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    cliente_id integer
);


ALTER TABLE public.gestores_campana OWNER TO postgres;

--
-- Name: gestores_campana_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gestores_campana_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gestores_campana_id_seq OWNER TO postgres;

--
-- Name: gestores_campana_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gestores_campana_id_seq OWNED BY public.gestores_campana.id;


--
-- Name: gestores_campana_pto_operacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gestores_campana_pto_operacion (
    id integer NOT NULL,
    campana_id integer NOT NULL,
    ptooperacion_id integer NOT NULL
);


ALTER TABLE public.gestores_campana_pto_operacion OWNER TO postgres;

--
-- Name: gestores_campana_pto_operacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gestores_campana_pto_operacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gestores_campana_pto_operacion_id_seq OWNER TO postgres;

--
-- Name: gestores_campana_pto_operacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gestores_campana_pto_operacion_id_seq OWNED BY public.gestores_campana_pto_operacion.id;


--
-- Name: gestores_campana_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gestores_campana_usuario (
    id integer NOT NULL,
    campana_id integer NOT NULL,
    usuario_id integer NOT NULL
);


ALTER TABLE public.gestores_campana_usuario OWNER TO postgres;

--
-- Name: gestores_campana_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gestores_campana_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gestores_campana_usuario_id_seq OWNER TO postgres;

--
-- Name: gestores_campana_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gestores_campana_usuario_id_seq OWNED BY public.gestores_campana_usuario.id;


--
-- Name: gestores_enlace; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gestores_enlace (
    id integer NOT NULL,
    url character varying(5050) NOT NULL,
    nombre character varying(50) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    cliente_id integer,
    estado boolean NOT NULL
);


ALTER TABLE public.gestores_enlace OWNER TO postgres;

--
-- Name: gestores_enlace_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gestores_enlace_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gestores_enlace_id_seq OWNER TO postgres;

--
-- Name: gestores_enlace_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gestores_enlace_id_seq OWNED BY public.gestores_enlace.id;


--
-- Name: gestores_publicidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gestores_publicidad (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    imagen character varying(100) NOT NULL,
    url character varying(5050),
    estado boolean NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    cliente_id integer
);


ALTER TABLE public.gestores_publicidad OWNER TO postgres;

--
-- Name: gestores_publicidad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gestores_publicidad_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gestores_publicidad_id_seq OWNER TO postgres;

--
-- Name: gestores_publicidad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gestores_publicidad_id_seq OWNED BY public.gestores_publicidad.id;


--
-- Name: gestores_servicio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gestores_servicio (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    icono character varying(50) NOT NULL,
    color_icono character varying(8) NOT NULL,
    color_fondo character varying(8) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    cliente_id integer,
    estado boolean NOT NULL,
    eliminado boolean NOT NULL
);


ALTER TABLE public.gestores_servicio OWNER TO postgres;

--
-- Name: gestores_servicio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gestores_servicio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gestores_servicio_id_seq OWNER TO postgres;

--
-- Name: gestores_servicio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gestores_servicio_id_seq OWNED BY public.gestores_servicio.id;


--
-- Name: gestores_tipoenlace; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gestores_tipoenlace (
    id integer NOT NULL,
    nombre character varying(25) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL
);


ALTER TABLE public.gestores_tipoenlace OWNER TO postgres;

--
-- Name: gestores_tipoenlace_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gestores_tipoenlace_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gestores_tipoenlace_id_seq OWNER TO postgres;

--
-- Name: gestores_tipoenlace_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gestores_tipoenlace_id_seq OWNED BY public.gestores_tipoenlace.id;


--
-- Name: sedes_ptooperacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sedes_ptooperacion (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    regional_id integer,
    barrio character varying(512),
    bis_via boolean NOT NULL,
    bis_via_generadora boolean NOT NULL,
    ciudad_id integer,
    complemento character varying(512),
    cuadrante_via character varying(12),
    cuadrante_via_generadora character varying(12),
    departamento_id integer,
    extencion1 character varying(6),
    extencion2 character varying(6),
    imagen character varying(100),
    numero_placa character varying(4),
    numero_via integer,
    numero_via_generadora integer,
    pais_id integer,
    prefijo_via character varying(12),
    prefijo_via_generadora character varying(12),
    telefono1 character varying(10),
    telefono2 character varying(10),
    tipo_via character varying(12),
    estado boolean NOT NULL,
    cliente_id integer
);


ALTER TABLE public.sedes_ptooperacion OWNER TO postgres;

--
-- Name: sedes_ptooperacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sedes_ptooperacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sedes_ptooperacion_id_seq OWNER TO postgres;

--
-- Name: sedes_ptooperacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sedes_ptooperacion_id_seq OWNED BY public.sedes_ptooperacion.id;


--
-- Name: sedes_ptooperacion_usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sedes_ptooperacion_usuarios (
    id integer NOT NULL,
    ptooperacion_id integer NOT NULL,
    usuario_id integer NOT NULL
);


ALTER TABLE public.sedes_ptooperacion_usuarios OWNER TO postgres;

--
-- Name: sedes_ptooperacion_usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sedes_ptooperacion_usuarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sedes_ptooperacion_usuarios_id_seq OWNER TO postgres;

--
-- Name: sedes_ptooperacion_usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sedes_ptooperacion_usuarios_id_seq OWNED BY public.sedes_ptooperacion_usuarios.id;


--
-- Name: sedes_regional; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sedes_regional (
    id integer NOT NULL,
    regional character varying(250) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    cliente_id integer,
    estado boolean NOT NULL,
    descripcion text,
    fecha_actualizacion timestamp with time zone NOT NULL
);


ALTER TABLE public.sedes_regional OWNER TO postgres;

--
-- Name: sedes_regional_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sedes_regional_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sedes_regional_id_seq OWNER TO postgres;

--
-- Name: sedes_regional_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sedes_regional_id_seq OWNED BY public.sedes_regional.id;


--
-- Name: tipificaciones_etapa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipificaciones_etapa (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL
);


ALTER TABLE public.tipificaciones_etapa OWNER TO postgres;

--
-- Name: tipificaciones_etapa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipificaciones_etapa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipificaciones_etapa_id_seq OWNER TO postgres;

--
-- Name: tipificaciones_etapa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipificaciones_etapa_id_seq OWNED BY public.tipificaciones_etapa.id;


--
-- Name: tipificaciones_homologacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipificaciones_homologacion (
    id integer NOT NULL,
    categoria integer,
    tipo_contacto integer,
    tipificacion integer,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    motivo_id integer
);


ALTER TABLE public.tipificaciones_homologacion OWNER TO postgres;

--
-- Name: tipificaciones_homologacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipificaciones_homologacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipificaciones_homologacion_id_seq OWNER TO postgres;

--
-- Name: tipificaciones_homologacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipificaciones_homologacion_id_seq OWNED BY public.tipificaciones_homologacion.id;


--
-- Name: tipificaciones_motivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipificaciones_motivo (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    etapa_id integer
);


ALTER TABLE public.tipificaciones_motivo OWNER TO postgres;

--
-- Name: tipificaciones_motivo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipificaciones_motivo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipificaciones_motivo_id_seq OWNER TO postgres;

--
-- Name: tipificaciones_motivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipificaciones_motivo_id_seq OWNED BY public.tipificaciones_motivo.id;


--
-- Name: ubicacion_ciudad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ubicacion_ciudad (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    departamento_id integer
);


ALTER TABLE public.ubicacion_ciudad OWNER TO postgres;

--
-- Name: ubicacion_ciudad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ubicacion_ciudad_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ubicacion_ciudad_id_seq OWNER TO postgres;

--
-- Name: ubicacion_ciudad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ubicacion_ciudad_id_seq OWNED BY public.ubicacion_ciudad.id;


--
-- Name: ubicacion_departamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ubicacion_departamento (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    pais_id integer
);


ALTER TABLE public.ubicacion_departamento OWNER TO postgres;

--
-- Name: ubicacion_departamento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ubicacion_departamento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ubicacion_departamento_id_seq OWNER TO postgres;

--
-- Name: ubicacion_departamento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ubicacion_departamento_id_seq OWNED BY public.ubicacion_departamento.id;


--
-- Name: ubicacion_pais; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ubicacion_pais (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    nomenclatura character varying(5) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL
);


ALTER TABLE public.ubicacion_pais OWNER TO postgres;

--
-- Name: ubicacion_pais_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ubicacion_pais_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ubicacion_pais_id_seq OWNER TO postgres;

--
-- Name: ubicacion_pais_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ubicacion_pais_id_seq OWNED BY public.ubicacion_pais.id;


--
-- Name: usuarios_tipoidentificacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios_tipoidentificacion (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL
);


ALTER TABLE public.usuarios_tipoidentificacion OWNER TO postgres;

--
-- Name: usuarios_tipoidentificacione_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_tipoidentificacione_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_tipoidentificacione_id_seq OWNER TO postgres;

--
-- Name: usuarios_tipoidentificacione_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_tipoidentificacione_id_seq OWNED BY public.usuarios_tipoidentificacion.id;


--
-- Name: usuarios_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios_usuario (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    codigo character varying(4),
    fecha_nacimiento date,
    is_super_administrador boolean NOT NULL,
    is_administrador boolean NOT NULL,
    is_responsable_campana boolean NOT NULL,
    is_asesor boolean NOT NULL,
    is_persona_natural boolean NOT NULL,
    serial character varying(50),
    modelo character varying(50),
    fabricante character varying(100),
    plataforma character varying(100),
    version_plataforma character varying(100),
    md5 character varying(50) NOT NULL,
    imagen character varying(100),
    numero_documento character varying(200) NOT NULL,
    fecha_creacion timestamp with time zone NOT NULL,
    fecha_actualizacion timestamp with time zone NOT NULL,
    padre_id integer,
    tipo_identificacion_id integer,
    barrio character varying(512),
    bis_via boolean NOT NULL,
    bis_via_generadora boolean NOT NULL,
    complemento character varying(512),
    numero_placa character varying(4),
    numero_via integer,
    numero_via_generadora integer,
    tipo_via character varying(12),
    cuadrante_via character varying(12),
    cuadrante_via_generadora character varying(12),
    prefijo_via character varying(12),
    prefijo_via_generadora character varying(12),
    is_administrador_punto boolean NOT NULL,
    ciudad_id integer,
    departamento_id integer,
    pais_id integer,
    eliminado boolean NOT NULL,
    estado boolean NOT NULL,
    telefono character varying(10),
    extencion character varying(6),
    asesor_id integer,
    asesor_to_responsable_campana_id integer
);


ALTER TABLE public.usuarios_usuario OWNER TO postgres;

--
-- Name: usuarios_usuario_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios_usuario_groups (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.usuarios_usuario_groups OWNER TO postgres;

--
-- Name: usuarios_usuario_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_usuario_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_usuario_groups_id_seq OWNER TO postgres;

--
-- Name: usuarios_usuario_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_usuario_groups_id_seq OWNED BY public.usuarios_usuario_groups.id;


--
-- Name: usuarios_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_usuario_id_seq OWNER TO postgres;

--
-- Name: usuarios_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_usuario_id_seq OWNED BY public.usuarios_usuario.id;


--
-- Name: usuarios_usuario_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios_usuario_user_permissions (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.usuarios_usuario_user_permissions OWNER TO postgres;

--
-- Name: usuarios_usuario_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_usuario_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_usuario_user_permissions_id_seq OWNER TO postgres;

--
-- Name: usuarios_usuario_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_usuario_user_permissions_id_seq OWNED BY public.usuarios_usuario_user_permissions.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: clientes_cliente id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente ALTER COLUMN id SET DEFAULT nextval('public.clientes_cliente_id_seq'::regclass);


--
-- Name: clientes_cliente_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente_usuario ALTER COLUMN id SET DEFAULT nextval('public.clientes_cliente_usuario_id_seq'::regclass);


--
-- Name: clientes_regimenfiscal id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_regimenfiscal ALTER COLUMN id SET DEFAULT nextval('public.clientes_regimenfiscal_id_seq'::regclass);


--
-- Name: clientes_tipoorganizacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_tipoorganizacion ALTER COLUMN id SET DEFAULT nextval('public.clientes_tipoorganizacion_id_seq'::regclass);


--
-- Name: clientes_tiposociedad id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_tiposociedad ALTER COLUMN id SET DEFAULT nextval('public.clientes_tiposociedad_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: gestores_campana id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana ALTER COLUMN id SET DEFAULT nextval('public.gestores_campana_id_seq'::regclass);


--
-- Name: gestores_campana_pto_operacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_pto_operacion ALTER COLUMN id SET DEFAULT nextval('public.gestores_campana_pto_operacion_id_seq'::regclass);


--
-- Name: gestores_campana_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_usuario ALTER COLUMN id SET DEFAULT nextval('public.gestores_campana_usuario_id_seq'::regclass);


--
-- Name: gestores_enlace id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_enlace ALTER COLUMN id SET DEFAULT nextval('public.gestores_enlace_id_seq'::regclass);


--
-- Name: gestores_publicidad id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_publicidad ALTER COLUMN id SET DEFAULT nextval('public.gestores_publicidad_id_seq'::regclass);


--
-- Name: gestores_servicio id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_servicio ALTER COLUMN id SET DEFAULT nextval('public.gestores_servicio_id_seq'::regclass);


--
-- Name: gestores_tipoenlace id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_tipoenlace ALTER COLUMN id SET DEFAULT nextval('public.gestores_tipoenlace_id_seq'::regclass);


--
-- Name: sedes_ptooperacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion ALTER COLUMN id SET DEFAULT nextval('public.sedes_ptooperacion_id_seq'::regclass);


--
-- Name: sedes_ptooperacion_usuarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion_usuarios ALTER COLUMN id SET DEFAULT nextval('public.sedes_ptooperacion_usuarios_id_seq'::regclass);


--
-- Name: sedes_regional id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_regional ALTER COLUMN id SET DEFAULT nextval('public.sedes_regional_id_seq'::regclass);


--
-- Name: tipificaciones_etapa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_etapa ALTER COLUMN id SET DEFAULT nextval('public.tipificaciones_etapa_id_seq'::regclass);


--
-- Name: tipificaciones_homologacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_homologacion ALTER COLUMN id SET DEFAULT nextval('public.tipificaciones_homologacion_id_seq'::regclass);


--
-- Name: tipificaciones_motivo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_motivo ALTER COLUMN id SET DEFAULT nextval('public.tipificaciones_motivo_id_seq'::regclass);


--
-- Name: ubicacion_ciudad id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_ciudad ALTER COLUMN id SET DEFAULT nextval('public.ubicacion_ciudad_id_seq'::regclass);


--
-- Name: ubicacion_departamento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_departamento ALTER COLUMN id SET DEFAULT nextval('public.ubicacion_departamento_id_seq'::regclass);


--
-- Name: ubicacion_pais id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_pais ALTER COLUMN id SET DEFAULT nextval('public.ubicacion_pais_id_seq'::regclass);


--
-- Name: usuarios_tipoidentificacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_tipoidentificacion ALTER COLUMN id SET DEFAULT nextval('public.usuarios_tipoidentificacione_id_seq'::regclass);


--
-- Name: usuarios_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario ALTER COLUMN id SET DEFAULT nextval('public.usuarios_usuario_id_seq'::regclass);


--
-- Name: usuarios_usuario_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_groups ALTER COLUMN id SET DEFAULT nextval('public.usuarios_usuario_groups_id_seq'::regclass);


--
-- Name: usuarios_usuario_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.usuarios_usuario_user_permissions_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add regimen fiscal	6	add_regimenfiscal
22	Can change regimen fiscal	6	change_regimenfiscal
23	Can delete regimen fiscal	6	delete_regimenfiscal
24	Can view regimen fiscal	6	view_regimenfiscal
25	Can add tipo identificacion	7	add_tipoidentificacion
26	Can change tipo identificacion	7	change_tipoidentificacion
27	Can delete tipo identificacion	7	delete_tipoidentificacion
28	Can view tipo identificacion	7	view_tipoidentificacion
29	Can add tipo organizacion	8	add_tipoorganizacion
30	Can change tipo organizacion	8	change_tipoorganizacion
31	Can delete tipo organizacion	8	delete_tipoorganizacion
32	Can view tipo organizacion	8	view_tipoorganizacion
33	Can add tipo sociedad	9	add_tiposociedad
34	Can change tipo sociedad	9	change_tiposociedad
35	Can delete tipo sociedad	9	delete_tiposociedad
36	Can view tipo sociedad	9	view_tiposociedad
37	Can add cliente	10	add_cliente
38	Can change cliente	10	change_cliente
39	Can delete cliente	10	delete_cliente
40	Can view cliente	10	view_cliente
41	Can add tipo enlace	11	add_tipoenlace
42	Can change tipo enlace	11	change_tipoenlace
43	Can delete tipo enlace	11	delete_tipoenlace
44	Can view tipo enlace	11	view_tipoenlace
45	Can add servicio	12	add_servicio
46	Can change servicio	12	change_servicio
47	Can delete servicio	12	delete_servicio
48	Can view servicio	12	view_servicio
49	Can add publicidad	13	add_publicidad
50	Can change publicidad	13	change_publicidad
51	Can delete publicidad	13	delete_publicidad
52	Can view publicidad	13	view_publicidad
53	Can add enlace	14	add_enlace
54	Can change enlace	14	change_enlace
55	Can delete enlace	14	delete_enlace
56	Can view enlace	14	view_enlace
57	Can add campana	15	add_campana
58	Can change campana	15	change_campana
59	Can delete campana	15	delete_campana
60	Can view campana	15	view_campana
61	Can add zona	16	add_zona
62	Can change zona	16	change_zona
63	Can delete zona	16	delete_zona
64	Can view zona	16	view_zona
65	Can add pto operacion	17	add_ptooperacion
66	Can change pto operacion	17	change_ptooperacion
67	Can delete pto operacion	17	delete_ptooperacion
68	Can view pto operacion	17	view_ptooperacion
69	Can add etapa	18	add_etapa
70	Can change etapa	18	change_etapa
71	Can delete etapa	18	delete_etapa
72	Can view etapa	18	view_etapa
73	Can add motivo	19	add_motivo
74	Can change motivo	19	change_motivo
75	Can delete motivo	19	delete_motivo
76	Can view motivo	19	view_motivo
77	Can add homologacion	20	add_homologacion
78	Can change homologacion	20	change_homologacion
79	Can delete homologacion	20	delete_homologacion
80	Can view homologacion	20	view_homologacion
81	Can add pais	21	add_pais
82	Can change pais	21	change_pais
83	Can delete pais	21	delete_pais
84	Can view pais	21	view_pais
85	Can add departamento	22	add_departamento
86	Can change departamento	22	change_departamento
87	Can delete departamento	22	delete_departamento
88	Can view departamento	22	view_departamento
89	Can add ciudad	23	add_ciudad
90	Can change ciudad	23	change_ciudad
91	Can delete ciudad	23	delete_ciudad
92	Can view ciudad	23	view_ciudad
93	Can add user	24	add_usuario
94	Can change user	24	change_usuario
95	Can delete user	24	delete_usuario
96	Can view user	24	view_usuario
97	Can add tipo identificacione	25	add_tipoidentificacione
98	Can change tipo identificacione	25	change_tipoidentificacione
99	Can delete tipo identificacione	25	delete_tipoidentificacione
100	Can view tipo identificacione	25	view_tipoidentificacione
101	Can add tipo identificacion	25	add_tipoidentificacion
102	Can change tipo identificacion	25	change_tipoidentificacion
103	Can delete tipo identificacion	25	delete_tipoidentificacion
104	Can view tipo identificacion	25	view_tipoidentificacion
105	Can add regional	26	add_regional
106	Can change regional	26	change_regional
107	Can delete regional	26	delete_regional
108	Can view regional	26	view_regional
\.


--
-- Data for Name: clientes_cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clientes_cliente (id, nombre_o_razon, nombre_contacto, identificacion, email, apellido, logo, fecha_creacion, fecha_actualizacion, ciudad_id, regimenfiscal_id, tipo_organizacion_id, tipo_sociedad_id, telefono, departamento_id, pais_id, estado, tipo_identificacion_id, barrio, bis_via, bis_via_generadora, complemento, cuadrante_via, cuadrante_via_generadora, numero_placa, numero_via, numero_via_generadora, prefijo_via, prefijo_via_generadora, tipo_via, eliminado) FROM stdin;
193	tncolombia	\N	01	tncolombia@tncolombia.com	tnsas	imagen-cliente/images_1_E3FfKiB.jpeg	2019-06-26 17:42:43.158472-05	2019-06-26 17:43:02.912629-05	433	\N	2	\N	310	94	1	t	1	barrio	t	f	complemento	SUR	NORTE	30	1	1	A	A	CARRERA	f
\.


--
-- Data for Name: clientes_cliente_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clientes_cliente_usuario (id, cliente_id, usuario_id) FROM stdin;
114	193	249
\.


--
-- Data for Name: clientes_regimenfiscal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clientes_regimenfiscal (id, nombre, fecha_creacion, fecha_actualizacion) FROM stdin;
1	Régimen Simple	2019-04-11 15:43:41.277637-05	2019-04-11 15:43:41.27766-05
2	Régimen Común	2019-04-11 15:43:49.24935-05	2019-04-11 15:43:49.249367-05
\.


--
-- Data for Name: clientes_tipoorganizacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clientes_tipoorganizacion (id, nombre, fecha_creacion, fecha_actualizacion) FROM stdin;
1	Persona Jurídica	2019-04-11 15:43:13.121389-05	2019-04-11 15:43:13.121429-05
2	Persona Natural	2019-04-11 15:43:21.566757-05	2019-04-11 15:43:21.566784-05
\.


--
-- Data for Name: clientes_tiposociedad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clientes_tiposociedad (id, nombre, fecha_creacion, fecha_actualizacion) FROM stdin;
1	Persona Natural Comerciante	2019-04-11 15:45:05.755823-05	2019-04-11 15:45:05.755895-05
2	Empresa Unipersonal	2019-04-11 15:45:13.212265-05	2019-04-11 15:45:13.212341-05
3	Sociedades Por Acciones Simplificadas (S.A.S)	2019-04-11 15:45:19.844861-05	2019-04-11 15:45:19.844893-05
4	Sociedad Colectiva	2019-04-11 15:45:25.439599-05	2019-04-11 15:45:25.439619-05
5	Sociedad Anónima (S.A.)	2019-04-11 15:45:30.954209-05	2019-04-11 15:45:30.954244-05
6	Sociedad De Responsabilidad Limitada (Ltda.)	2019-04-11 15:45:36.523238-05	2019-04-11 15:45:36.523361-05
7	Sociedad En Comandita Simple (S. En C.)	2019-04-11 15:45:41.886457-05	2019-04-11 15:45:41.886476-05
8	Sociedad Comandita Por Acciones (S.C.A.)	2019-04-11 15:45:47.920024-05	2019-04-11 15:45:47.920075-05
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
326	2019-06-20 11:24:02.164323-05	238	carolina	1	[{"added": {}}]	24	1
327	2019-07-10 13:05:15.959411-05	260	edwar	1	[{"added": {}}]	24	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	clientes	regimenfiscal
7	clientes	tipoidentificacion
8	clientes	tipoorganizacion
9	clientes	tiposociedad
10	clientes	cliente
11	gestores	tipoenlace
12	gestores	servicio
13	gestores	publicidad
14	gestores	enlace
15	gestores	campana
16	sedes	zona
17	sedes	ptooperacion
18	tipificaciones	etapa
19	tipificaciones	motivo
20	tipificaciones	homologacion
21	ubicacion	pais
22	ubicacion	departamento
23	ubicacion	ciudad
24	usuarios	usuario
25	usuarios	tipoidentificacion
26	sedes	regional
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
107	ubicacion	0001_initial	2019-06-20 10:45:21.936159-05
108	clientes	0001_initial	2019-06-20 10:45:21.952465-05
109	contenttypes	0001_initial	2019-06-20 10:45:21.960342-05
110	contenttypes	0002_remove_content_type_name	2019-06-20 10:45:21.970069-05
111	auth	0001_initial	2019-06-20 10:45:21.97791-05
112	auth	0002_alter_permission_name_max_length	2019-06-20 10:45:21.985506-05
113	auth	0003_alter_user_email_max_length	2019-06-20 10:45:21.998417-05
114	auth	0004_alter_user_username_opts	2019-06-20 10:45:22.01565-05
115	auth	0005_alter_user_last_login_null	2019-06-20 10:45:22.028148-05
116	auth	0006_require_contenttypes_0002	2019-06-20 10:45:22.036447-05
117	auth	0007_alter_validators_add_error_messages	2019-06-20 10:45:22.046396-05
118	auth	0008_alter_user_username_max_length	2019-06-20 10:45:22.053414-05
119	auth	0009_alter_user_last_name_max_length	2019-06-20 10:45:22.061378-05
120	auth	0010_alter_group_name_max_length	2019-06-20 10:45:22.068843-05
121	auth	0011_update_proxy_permissions	2019-06-20 10:45:22.077588-05
122	usuarios	0001_initial	2019-06-20 10:45:22.086545-05
123	admin	0001_initial	2019-06-20 10:45:22.093876-05
124	admin	0002_logentry_remove_auto_add	2019-06-20 10:45:22.10236-05
125	admin	0003_logentry_add_action_flag_choices	2019-06-20 10:45:22.110792-05
126	usuarios	0002_remove_usuario_tipo_identificacion	2019-06-20 10:45:22.119758-05
127	usuarios	0003_auto_20190425_0846	2019-06-20 10:45:22.127076-05
128	ubicacion	0002_auto_20190411_1646	2019-06-20 10:45:22.138277-05
129	ubicacion	0003_auto_20190411_1653	2019-06-20 10:45:22.156187-05
130	clientes	0002_cliente_usuario	2019-06-20 10:45:22.170127-05
131	clientes	0003_auto_20190411_1636	2019-06-20 10:45:22.177747-05
132	clientes	0004_auto_20190415_1126	2019-06-20 10:45:22.186475-05
133	clientes	0005_auto_20190415_1128	2019-06-20 10:45:22.193751-05
134	clientes	0006_auto_20190415_1131	2019-06-20 10:45:22.202588-05
135	clientes	0007_cliente_estado	2019-06-20 10:45:22.210876-05
136	clientes	0008_auto_20190415_1153	2019-06-20 10:45:22.223964-05
137	clientes	0009_cliente_telefono	2019-06-20 10:45:22.235824-05
138	clientes	0010_auto_20190422_1407	2019-06-20 10:45:22.244492-05
139	clientes	0011_remove_cliente_estado	2019-06-20 10:45:22.253888-05
140	clientes	0012_auto_20190423_1422	2019-06-20 10:45:22.26122-05
141	clientes	0013_auto_20190423_1433	2019-06-20 10:45:22.270803-05
142	clientes	0014_cliente_estado	2019-06-20 10:45:22.277781-05
143	clientes	0015_remove_cliente_estado	2019-06-20 10:45:22.285614-05
144	clientes	0016_cliente_estado	2019-06-20 10:45:22.294295-05
145	usuarios	0004_usuario_tipo_identificacion	2019-06-20 10:45:22.303723-05
146	usuarios	0005_auto_20190425_1103	2019-06-20 10:45:22.311203-05
147	usuarios	0006_auto_20190425_1108	2019-06-20 10:45:22.31907-05
148	clientes	0017_remove_cliente_tipo_identificacion	2019-06-20 10:45:22.327829-05
149	clientes	0018_delete_tipoidentificacion	2019-06-20 10:45:22.337068-05
150	clientes	0019_cliente_tipo_identificacion	2019-06-20 10:45:22.344437-05
151	clientes	0020_auto_20190425_1539	2019-06-20 10:45:22.352238-05
152	clientes	0021_auto_20190425_1551	2019-06-20 10:45:22.361092-05
153	clientes	0022_auto_20190429_1134	2019-06-20 10:45:22.37123-05
154	clientes	0023_auto_20190502_1148	2019-06-20 10:45:22.377888-05
155	clientes	0024_auto_20190503_1141	2019-06-20 10:45:22.385716-05
156	clientes	0025_auto_20190509_1250	2019-06-20 10:45:22.394648-05
157	clientes	0026_auto_20190509_1308	2019-06-20 10:45:22.403684-05
158	clientes	0027_auto_20190510_1135	2019-06-20 10:45:22.411265-05
159	clientes	0028_auto_20190513_1556	2019-06-20 10:45:22.419262-05
160	clientes	0029_auto_20190514_0947	2019-06-20 10:45:22.428044-05
161	clientes	0030_auto_20190517_0930	2019-06-20 10:45:22.437547-05
162	sedes	0001_initial	2019-06-20 10:45:22.444638-05
163	gestores	0001_initial	2019-06-20 10:45:22.455671-05
164	gestores	0002_campana_pto_operacion	2019-06-20 10:45:22.472156-05
165	gestores	0003_campana_usuario	2019-06-20 10:45:22.488091-05
166	gestores	0004_auto_20190614_1709	2019-06-20 10:45:22.495121-05
167	gestores	0005_servicio_eliminado	2019-06-20 10:45:22.503731-05
168	gestores	0006_auto_20190617_1201	2019-06-20 10:45:22.51128-05
169	gestores	0007_auto_20190617_1202	2019-06-20 10:45:22.519407-05
170	gestores	0008_auto_20190617_1239	2019-06-20 10:45:22.527454-05
171	gestores	0009_remove_enlace_tipo_enlace	2019-06-20 10:45:22.53907-05
172	gestores	0010_auto_20190618_1612	2019-06-20 10:45:22.555815-05
173	sedes	0002_auto_20190411_1525	2019-06-20 10:45:22.574898-05
174	sedes	0003_auto_20190411_1636	2019-06-20 10:45:22.590524-05
175	sedes	0004_auto_20190515_1639	2019-06-20 10:45:22.605534-05
176	sedes	0005_remove_regional_cliente	2019-06-20 10:45:22.622388-05
177	sedes	0006_regional_cliente	2019-06-20 10:45:22.63854-05
178	sedes	0007_auto_20190515_1731	2019-06-20 10:45:22.655906-05
179	sedes	0008_auto_20190515_1733	2019-06-20 10:45:22.672526-05
180	sedes	0009_auto_20190521_0827	2019-06-20 10:45:22.688968-05
181	sedes	0010_auto_20190521_0850	2019-06-20 10:45:22.706283-05
182	sedes	0011_regional_estado	2019-06-20 10:45:22.722503-05
183	sedes	0012_regional_descripcion	2019-06-20 10:45:22.74131-05
184	sedes	0013_auto_20190610_1043	2019-06-20 10:45:22.755589-05
185	sedes	0014_ptooperacion_estado	2019-06-20 10:45:22.772792-05
186	sedes	0015_ptooperacion_cliente	2019-06-20 10:45:22.794816-05
187	sessions	0001_initial	2019-06-20 10:45:22.816733-05
188	tipificaciones	0001_initial	2019-06-20 10:45:22.876052-05
189	usuarios	0007_auto_20190425_1454	2019-06-20 10:45:22.905087-05
190	usuarios	0008_usuario_is_administrador_punto	2019-06-20 10:45:22.922144-05
191	usuarios	0009_auto_20190510_1029	2019-06-20 10:45:22.940854-05
192	usuarios	0010_auto_20190510_1051	2019-06-20 10:45:22.960089-05
193	usuarios	0011_usuario_tipo_via	2019-06-20 10:45:22.972751-05
194	usuarios	0012_auto_20190510_1058	2019-06-20 10:45:22.991036-05
195	usuarios	0013_auto_20190510_1132	2019-06-20 10:45:23.00597-05
196	usuarios	0014_auto_20190510_1135	2019-06-20 10:45:23.023484-05
197	usuarios	0015_auto_20190513_1459	2019-06-20 10:45:23.040655-05
198	usuarios	0016_auto_20190514_0947	2019-06-20 10:45:23.060569-05
199	usuarios	0017_usuario_is_administrador_punto	2019-06-20 10:45:23.08047-05
200	usuarios	0018_auto_20190514_1658	2019-06-20 10:45:23.099163-05
201	usuarios	0018_auto_20190514_1552	2019-06-20 10:45:23.114128-05
202	usuarios	0019_merge_20190514_1707	2019-06-20 10:45:23.132169-05
203	usuarios	0020_auto_20190514_1723	2019-06-20 10:45:23.14825-05
204	usuarios	0021_auto_20190514_1733	2019-06-20 10:45:23.165347-05
205	usuarios	0022_auto_20190515_1023	2019-06-20 10:45:23.182845-05
206	usuarios	0023_auto_20190515_1031	2019-06-20 10:45:23.200361-05
207	usuarios	0024_auto_20190515_1528	2019-06-20 10:45:23.214728-05
208	usuarios	0025_auto_20190516_1239	2019-06-20 10:45:23.233424-05
209	usuarios	0026_usuario_asesor_to_responsable_campana	2019-06-20 10:45:23.247754-05
210	usuarios	0027_auto_20190610_1043	2019-06-20 10:45:23.265847-05
211	usuarios	0027_auto_20190604_1732	2019-06-20 10:45:23.281337-05
212	usuarios	0028_merge_20190610_1646	2019-06-20 10:45:23.301356-05
213	clientes	0031_auto_20190621_1139	2019-06-21 12:23:24.864701-05
214	sedes	0016_remove_regional_fecha_actualizacion	2019-06-21 15:14:07.148386-05
215	sedes	0017_regional_fecha_actualizacion	2019-06-21 15:15:03.113968-05
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
fk4ref0km8ju95mv8n3bple42mb2zg1y	ZWM4ZTAyNjYyMGU5MWYyZWFhMTRlMjkwZTBhMGUyY2QwMWRlMDQzMDp7Il9hdXRoX3VzZXJfaWQiOiIyNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6ImZkMWQyNjRiZmIxZTkwZGU3MWIxMTkyNDViMTk1MjZhNTIxMDQ5MjEifQ==	2019-07-11 08:12:42.54874-05
wk4oianz3j14aycy6nz7yjr2g5ffq0pt	ZWM4ZTAyNjYyMGU5MWYyZWFhMTRlMjkwZTBhMGUyY2QwMWRlMDQzMDp7Il9hdXRoX3VzZXJfaWQiOiIyNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6ImZkMWQyNjRiZmIxZTkwZGU3MWIxMTkyNDViMTk1MjZhNTIxMDQ5MjEifQ==	2019-07-11 08:58:56.020066-05
7uwg30qu4psn4zudauuwis1jb6oq65w6	YmU5OGQwMTI3ODllNjE4YmVhOWQ0MzNhOGVjNWFmOGJmNDI1Y2Q3Mjp7Il9hdXRoX3VzZXJfaWQiOiIyMzgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6ImZkMWQyNjRiZmIxZTkwZGU3MWIxMTkyNDViMTk1MjZhNTIxMDQ5MjEifQ==	2019-07-23 09:10:32.413-05
eatn2pilvzc85cxssxiohrvv0syr00n0	ZWM4ZTAyNjYyMGU5MWYyZWFhMTRlMjkwZTBhMGUyY2QwMWRlMDQzMDp7Il9hdXRoX3VzZXJfaWQiOiIyNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6ImZkMWQyNjRiZmIxZTkwZGU3MWIxMTkyNDViMTk1MjZhNTIxMDQ5MjEifQ==	2019-07-24 13:07:17.580728-05
dpha4wk4urigw3x3uba5xc2austyyvyh	ZWM4ZTAyNjYyMGU5MWYyZWFhMTRlMjkwZTBhMGUyY2QwMWRlMDQzMDp7Il9hdXRoX3VzZXJfaWQiOiIyNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6ImZkMWQyNjRiZmIxZTkwZGU3MWIxMTkyNDViMTk1MjZhNTIxMDQ5MjEifQ==	2019-07-24 17:23:27.888114-05
\.


--
-- Data for Name: gestores_campana; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gestores_campana (id, nombre, inicio, fin, descripcion, estado, fecha_creacion, fecha_actualizacion, cliente_id) FROM stdin;
\.


--
-- Data for Name: gestores_campana_pto_operacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gestores_campana_pto_operacion (id, campana_id, ptooperacion_id) FROM stdin;
\.


--
-- Data for Name: gestores_campana_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gestores_campana_usuario (id, campana_id, usuario_id) FROM stdin;
\.


--
-- Data for Name: gestores_enlace; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gestores_enlace (id, url, nombre, fecha_creacion, fecha_actualizacion, cliente_id, estado) FROM stdin;
\.


--
-- Data for Name: gestores_publicidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gestores_publicidad (id, nombre, imagen, url, estado, fecha_creacion, fecha_actualizacion, cliente_id) FROM stdin;
\.


--
-- Data for Name: gestores_servicio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gestores_servicio (id, nombre, icono, color_icono, color_fondo, fecha_creacion, fecha_actualizacion, cliente_id, estado, eliminado) FROM stdin;
\.


--
-- Data for Name: gestores_tipoenlace; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gestores_tipoenlace (id, nombre, fecha_creacion, fecha_actualizacion) FROM stdin;
\.


--
-- Data for Name: sedes_ptooperacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sedes_ptooperacion (id, nombre, fecha_creacion, fecha_actualizacion, regional_id, barrio, bis_via, bis_via_generadora, ciudad_id, complemento, cuadrante_via, cuadrante_via_generadora, departamento_id, extencion1, extencion2, imagen, numero_placa, numero_via, numero_via_generadora, pais_id, prefijo_via, prefijo_via_generadora, telefono1, telefono2, tipo_via, estado, cliente_id) FROM stdin;
\.


--
-- Data for Name: sedes_ptooperacion_usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sedes_ptooperacion_usuarios (id, ptooperacion_id, usuario_id) FROM stdin;
\.


--
-- Data for Name: sedes_regional; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sedes_regional (id, regional, fecha_creacion, cliente_id, estado, descripcion, fecha_actualizacion) FROM stdin;
\.


--
-- Data for Name: tipificaciones_etapa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipificaciones_etapa (id, nombre, fecha_creacion, fecha_actualizacion) FROM stdin;
\.


--
-- Data for Name: tipificaciones_homologacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipificaciones_homologacion (id, categoria, tipo_contacto, tipificacion, fecha_creacion, fecha_actualizacion, motivo_id) FROM stdin;
\.


--
-- Data for Name: tipificaciones_motivo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipificaciones_motivo (id, nombre, fecha_creacion, fecha_actualizacion, etapa_id) FROM stdin;
\.


--
-- Data for Name: ubicacion_ciudad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ubicacion_ciudad (id, nombre, fecha_creacion, fecha_actualizacion, departamento_id) FROM stdin;
1	ABRIAQUÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
2	ACACÍAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
3	ACANDÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
4	ACEVEDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
5	ACHÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
6	AGRADO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
7	AGUA DE DIOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
8	AGUACHICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
9	AGUADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
10	AGUADAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
11	AGUAZUL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
12	AGUSTÍN CODAZZI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
13	AIPE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
14	ALBANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
15	ALBANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
16	ALBANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
17	ALBÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
18	ALBÁN (SAN JOSÉ)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
19	ALCALÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
20	ALEJANDRIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
21	ALGARROBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
22	ALGECIRAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
23	ALMAGUER	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
24	ALMEIDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
25	ALPUJARRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
26	ALTAMIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
27	ALTO BAUDÓ (PIE DE PATO)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
28	ALTOS DEL ROSARIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
29	ALVARADO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
30	AMAGÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
31	AMALFI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
32	AMBALEMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
33	ANAPOIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
34	ANCUYA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
35	ANDALUCÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
36	ANDES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
37	ANGELÓPOLIS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
38	ANGOSTURA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
39	ANOLAIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
40	ANORÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
41	ANSERMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
42	ANSERMANUEVO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
43	ANZOÁTEGUI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
44	ANZÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
45	APARTADÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
46	APULO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
47	APÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
48	AQUITANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
49	ARACATACA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
50	ARANZAZU	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
51	ARATOCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
52	ARAUCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	81
53	ARAUQUITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	81
54	ARBELÁEZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
55	ARBOLEDA (BERRUECOS)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
56	ARBOLEDAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
57	ARBOLETES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
58	ARCABUCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
59	ARENAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
60	ARGELIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
61	ARGELIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
62	ARGELIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
63	ARIGUANÍ (EL DIFÍCIL)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
64	ARJONA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
65	ARMENIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
66	ARMENIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
67	ARMERO (GUAYABAL)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
68	ARROYOHONDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
69	ASTREA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
70	ATACO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
71	ATRATO (YUTO)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
72	AYAPEL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
73	BAGADÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
74	BAHÍA SOLANO (MÚTIS)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
75	BAJO BAUDÓ (PIZARRO)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
76	BALBOA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
77	BALBOA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
78	BARANOA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
79	BARAYA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
80	BARBACOAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
81	BARBOSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
82	BARBOSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
83	BARICHARA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
84	BARRANCA DE UPÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
85	BARRANCABERMEJA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
86	BARRANCAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
87	BARRANCO DE LOBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
88	BARRANQUILLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
89	BECERRÍL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
90	BELALCÁZAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
91	BELLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
92	BELMIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
93	BELTRÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
94	BELÉN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
95	BELÉN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
96	BELÉN DE BAJIRÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
97	BELÉN DE UMBRÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
98	BELÉN DE LOS ANDAQUÍES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
99	BERBEO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
100	BETANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
101	BETEITIVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
102	BETULIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
103	BETULIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
104	BITUIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
105	BOAVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
106	BOCHALEMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
107	BOGOTÁ D.C.	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	11
108	BOJACÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
109	BOJAYÁ (BELLAVISTA)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
110	BOLÍVAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
111	BOLÍVAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
112	BOLÍVAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
113	BOLÍVAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
114	BOSCONIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
115	BOYACÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
116	BRICEÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
117	BRICEÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
118	BUCARAMANGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
119	BUCARASICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
120	BUENAVENTURA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
121	BUENAVISTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
122	BUENAVISTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
123	BUENAVISTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
124	BUENAVISTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
125	BUENOS AIRES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
126	BUESACO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
127	BUGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
128	BUGALAGRANDE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
129	BURÍTICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
130	BUSBANZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
131	CABRERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
132	CABRERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
133	CABUYARO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
134	CACHIPAY	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
135	CAICEDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
136	CAICEDONIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
137	CAIMITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
138	CAJAMARCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
139	CAJIBÍO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
140	CAJICÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
141	CALAMAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
142	CALAMAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	95
143	CALARCÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
144	CALDAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
145	CALDAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
146	CALDONO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
147	CALIFORNIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
148	CALIMA (DARIÉN)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
149	CALOTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
150	CALÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
151	CAMPAMENTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
152	CAMPO DE LA CRUZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
153	CAMPOALEGRE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
154	CAMPOHERMOSO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
155	CANALETE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
156	CANDELARIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
157	CANDELARIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
158	CANTAGALLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
159	CANTÓN DE SAN PABLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
160	CAPARRAPÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
161	CAPITANEJO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
162	CARACOLÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
163	CARAMANTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
164	CARCASÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
165	CAREPA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
166	CARMEN DE APICALÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
167	CARMEN DE CARUPA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
168	CARMEN DE VIBORAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
169	CARMEN DEL DARIÉN (CURBARADÓ)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
170	CAROLINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
171	CARTAGENA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
172	CARTAGENA DEL CHAIRÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
173	CARTAGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
174	CARURÚ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	97
175	CASABIANCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
176	CASTILLA LA NUEVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
177	CAUCASIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
178	CAÑASGORDAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
179	CEPITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
180	CERETÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
181	CERINZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
182	CERRITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
183	CERRO SAN ANTONIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
184	CHACHAGUÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
185	CHAGUANÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
186	CHALÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
187	CHAPARRAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
188	CHARALÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
189	CHARTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
190	CHIGORODÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
191	CHIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
192	CHIMICHAGUA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
193	CHIMÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
194	CHINAVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
195	CHINCHINÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
196	CHINÁCOTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
197	CHINÚ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
198	CHIPAQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
199	CHIPATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
200	CHIQUINQUIRÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
201	CHIRIGUANÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
202	CHISCAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
203	CHITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
204	CHITAGÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
205	CHITARAQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
206	CHIVATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
207	CHIVOLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
208	CHOACHÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
209	CHOCONTÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
210	CHÁMEZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
211	CHÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
212	CHÍQUIZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
213	CHÍVOR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
214	CICUCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
215	CIMITARRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
216	CIRCASIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
217	CISNEROS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
218	CIÉNAGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
219	CIÉNAGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
220	CIÉNAGA DE ORO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
221	CLEMENCIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
222	COCORNÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
223	COELLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
224	COGUA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
225	COLOMBIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
226	COLOSÓ (RICAURTE)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
227	COLÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
228	COLÓN (GÉNOVA)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
229	CONCEPCIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
230	CONCEPCIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
231	CONCORDIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
232	CONCORDIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
233	CONDOTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
234	CONFINES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
235	CONSACA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
236	CONTADERO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
237	CONTRATACIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
238	CONVENCIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
239	COPACABANA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
240	COPER	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
241	CORDOBÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
242	CORINTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
243	COROMORO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
244	COROZAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
245	CORRALES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
246	COTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
247	COTORRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
248	COVARACHÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
249	COVEÑAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
250	COYAIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
251	CRAVO NORTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	81
252	CUASPUD (CARLOSAMA)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
253	CUBARRAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
254	CUBARÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
255	CUCAITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
256	CUCUNUBÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
257	CUCUTILLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
258	CUITIVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
259	CUMARAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
260	CUMARIBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	99
261	CUMBAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
262	CUMBITARA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
263	CUNDAY	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
264	CURILLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
265	CURITÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
266	CURUMANÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
267	CÁCERES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
268	CÁCHIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
269	CÁCOTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
270	CÁQUEZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
271	CÉRTEGUI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
272	CÓMBITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
273	CÓRDOBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
274	CÓRDOBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
275	CÚCUTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
276	DABEIBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
277	DAGUA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
278	DIBULLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
279	DISTRACCIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
280	DOLORES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
281	DON MATÍAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
282	DOS QUEBRADAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
283	DUITAMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
284	DURANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
285	EBÉJICO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
286	EL BAGRE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
287	EL BANCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
288	EL CAIRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
289	EL CALVARIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
290	EL CARMEN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
291	EL CARMEN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
292	EL CARMEN DE ATRATO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
293	EL CARMEN DE BOLÍVAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
294	EL CASTILLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
295	EL CERRITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
296	EL CHARCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
297	EL COCUY	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
298	EL COLEGIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
299	EL COPEY	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
300	EL DONCELLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
301	EL DORADO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
302	EL DOVIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
303	EL ESPINO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
304	EL GUACAMAYO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
305	EL GUAMO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
306	EL MOLINO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
307	EL PASO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
308	EL PAUJIL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
309	EL PEÑOL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
310	EL PEÑON	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
311	EL PEÑON	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
312	EL PEÑÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
313	EL PIÑON	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
314	EL PLAYÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
315	EL RETORNO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	95
316	EL RETÉN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
317	EL ROBLE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
318	EL ROSAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
319	EL ROSARIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
320	EL TABLÓN DE GÓMEZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
321	EL TAMBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
322	EL TAMBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
323	EL TARRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
324	EL ZULIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
325	EL ÁGUILA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
326	ELÍAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
327	ENCINO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
328	ENCISO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
329	ENTRERRÍOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
330	ENVIGADO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
331	ESPINAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
332	FACATATIVÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
333	FALAN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
334	FILADELFIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
335	FILANDIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
336	FIRAVITOBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
337	FLANDES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
338	FLORENCIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
339	FLORENCIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
340	FLORESTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
341	FLORIDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
342	FLORIDABLANCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
343	FLORIÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
344	FONSECA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
345	FORTÚL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	81
346	FOSCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
347	FRANCISCO PIZARRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
348	FREDONIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
349	FRESNO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
350	FRONTINO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
351	FUENTE DE ORO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
352	FUNDACIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
353	FUNES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
354	FUNZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
355	FUSAGASUGÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
356	FÓMEQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
357	FÚQUENE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
358	GACHALÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
359	GACHANCIPÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
360	GACHANTIVÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
361	GACHETÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
362	GALAPA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
363	GALERAS (NUEVA GRANADA)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
364	GALÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
365	GAMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
366	GAMARRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
367	GARAGOA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
368	GARZÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
369	GIGANTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
370	GINEBRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
371	GIRALDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
372	GIRARDOT	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
373	GIRARDOTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
374	GIRÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
375	GONZALEZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
376	GRAMALOTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
377	GRANADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
378	GRANADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
379	GRANADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
380	GUACA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
381	GUACAMAYAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
382	GUACARÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
383	GUACHAVÉS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
384	GUACHENÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
385	GUACHETÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
386	GUACHUCAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
387	GUADALUPE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
388	GUADALUPE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
389	GUADALUPE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
390	GUADUAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
391	GUAITARILLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
392	GUALMATÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
393	GUAMAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
394	GUAMAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
395	GUAMO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
396	GUAPOTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
397	GUAPÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
398	GUARANDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
399	GUARNE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
400	GUASCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
401	GUATAPÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
402	GUATAQUÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
403	GUATAVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
404	GUATEQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
405	GUAVATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
406	GUAYABAL DE SIQUIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
407	GUAYABETAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
408	GUAYATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
409	GUEPSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
410	GUICÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
411	GUTIÉRREZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
412	GUÁTICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
413	GÁMBITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
414	GÁMEZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
415	GÉNOVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
416	GÓMEZ PLATA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
417	HACARÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
418	HATILLO DE LOBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
419	HATO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
420	HATO COROZAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
421	HATONUEVO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
422	HELICONIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
423	HERRÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
424	HERVEO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
425	HISPANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
426	HOBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
427	HONDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
428	IBAGUÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
429	ICONONZO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
430	ILES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
431	IMÚES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
432	INZÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
433	INÍRIDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	94
434	IPIALES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
435	ISNOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
436	ISTMINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
437	ITAGÜÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
438	ITUANGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
439	IZÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
440	JAMBALÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
441	JAMUNDÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
442	JARDÍN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
443	JENESANO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
444	JERICÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
445	JERICÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
446	JERUSALÉN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
447	JESÚS MARÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
448	JORDÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
449	JUAN DE ACOSTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
450	JUNÍN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
451	JURADÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
452	LA APARTADA Y LA FRONTERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
453	LA ARGENTINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
454	LA BELLEZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
455	LA CALERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
456	LA CAPILLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
457	LA CEJA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
458	LA CELIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
459	LA CRUZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
460	LA CUMBRE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
461	LA DORADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
462	LA ESPERANZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
463	LA ESTRELLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
464	LA FLORIDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
465	LA GLORIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
466	LA JAGUA DE IBIRICO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
467	LA JAGUA DEL PILAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
468	LA LLANADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
469	LA MACARENA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
470	LA MERCED	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
471	LA MESA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
472	LA MONTAÑITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
473	LA PALMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
474	LA PAZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
475	LA PAZ (ROBLES)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
476	LA PEÑA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
477	LA PINTADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
478	LA PLATA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
479	LA PLAYA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
480	LA PRIMAVERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	99
481	LA SALINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
482	LA SIERRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
483	LA TEBAIDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
484	LA TOLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
485	LA UNIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
486	LA UNIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
487	LA UNIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
488	LA UNIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
489	LA UVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
490	LA VEGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
491	LA VEGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
492	LA VICTORIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
493	LA VICTORIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
494	LA VICTORIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
495	LA VIRGINIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
496	LABATECA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
497	LABRANZAGRANDE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
498	LANDÁZURI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
499	LEBRIJA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
500	LEIVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
501	LEJANÍAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
502	LENGUAZAQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
503	LETICIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	91
504	LIBORINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
505	LINARES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
506	LLORÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
507	LORICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
508	LOS CÓRDOBAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
509	LOS PALMITOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
510	LOS PATIOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
511	LOS SANTOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
512	LOURDES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
513	LURUACO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
514	LÉRIDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
515	LÍBANO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
516	LÓPEZ (MICAY)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
517	MACANAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
518	MACARAVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
519	MACEO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
520	MACHETÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
521	MADRID	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
522	MAGANGUÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
523	MAGÜI (PAYÁN)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
524	MAHATES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
525	MAICAO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
526	MAJAGUAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
527	MALAMBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
528	MALLAMA (PIEDRANCHA)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
529	MANATÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
530	MANAURE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
531	MANAURE BALCÓN DEL CESAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
532	MANIZALES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
533	MANTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
534	MANZANARES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
535	MANÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
536	MAPIRIPAN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
537	MARGARITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
538	MARINILLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
539	MARIPÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
540	MARIQUITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
541	MARMATO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
542	MARQUETALIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
543	MARSELLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
544	MARULANDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
545	MARÍA LA BAJA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
546	MATANZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
547	MEDELLÍN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
548	MEDINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
549	MEDIO ATRATO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
550	MEDIO BAUDÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
551	MEDIO SAN JUAN (ANDAGOYA)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
552	MELGAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
553	MERCADERES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
554	MESETAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
555	MILÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
556	MIRAFLORES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
557	MIRAFLORES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	95
558	MIRANDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
559	MISTRATÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
560	MITÚ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	97
561	MOCOA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
562	MOGOTES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
563	MOLAGAVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
564	MOMIL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
565	MOMPÓS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
566	MONGUA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
567	MONGUÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
568	MONIQUIRÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
569	MONTEBELLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
570	MONTECRISTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
571	MONTELÍBANO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
572	MONTENEGRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
573	MONTERIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
574	MONTERREY	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
575	MORALES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
576	MORALES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
577	MORELIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
578	MORROA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
579	MOSQUERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
580	MOSQUERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
581	MOTAVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
582	MOÑITOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
583	MURILLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
584	MURINDÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
585	MUTATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
586	MUTISCUA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
587	MUZO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
588	MÁLAGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
589	NARIÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
590	NARIÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
591	NARIÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
592	NATAGAIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
593	NECHÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
594	NECOCLÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
595	NEIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
596	NEIVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
597	NEMOCÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
598	NILO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
599	NIMAIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
600	NOBSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
601	NOCAIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
602	NORCASIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
603	NOROSÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
604	NOVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
605	NUEVA GRANADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
606	NUEVO COLÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
607	NUNCHÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
608	NUQUÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
609	NÁTAGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
610	OBANDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
611	OCAMONTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
612	OCAÑA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
613	OIBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
614	OICATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
615	OLAYA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
616	OLAYA HERRERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
617	ONZAGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
618	OPORAPA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
619	ORITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
620	OROCUÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
621	ORTEGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
622	OSPINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
623	OTANCHE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
624	OVEJAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
625	PACHAVITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
626	PACHO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
627	PADILLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
628	PAICOL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
629	PAILITAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
630	PAIME	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
631	PAIPA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
632	PAJARITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
633	PALERMO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
634	PALESTINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
635	PALESTINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
636	PALMAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
637	PALMAR DE VARELA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
638	PALMAS DEL SOCORRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
639	PALMIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
640	PALMITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
641	PALOCABILDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
642	PAMPLONA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
643	PAMPLONITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
644	PANDI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
645	PANQUEBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
646	PARATEBUENO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
647	PASCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
648	PATÍA (EL BORDO)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
649	PAUNA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
650	PAYA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
651	PAZ DE ARIPORO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
652	PAZ DE RÍO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
653	PEDRAZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
654	PELAYA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
655	PENSILVANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
656	PEQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
657	PEREIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
658	PESCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
659	PEÑOL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
660	PIAMONTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
661	PIE DE CUESTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
662	PIEDRAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
663	PIENDAMÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
664	PIJAO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
665	PIJIÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
666	PINCHOTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
667	PINILLOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
668	PIOJO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
669	PISVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
670	PITAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
671	PITALITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
672	PIVIJAY	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
673	PLANADAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
674	PLANETA RICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
675	PLATO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
676	POLICARPA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
677	POLONUEVO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
678	PONEDERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
679	POPAYÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
680	PORE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
681	POTOSÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
682	PRADERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
683	PRADO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
684	PROVIDENCIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
685	PROVIDENCIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	88
686	PUEBLO BELLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
687	PUEBLO NUEVO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
688	PUEBLO RICO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
689	PUEBLORRICO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
690	PUEBLOVIEJO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
691	PUENTE NACIONAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
692	PUERRES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
693	PUERTO ASÍS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
694	PUERTO BERRÍO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
695	PUERTO BOYACÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
696	PUERTO CAICEDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
697	PUERTO CARREÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	99
698	PUERTO COLOMBIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
699	PUERTO CONCORDIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
700	PUERTO ESCONDIDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
701	PUERTO GAITÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
702	PUERTO GUZMÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
703	PUERTO LEGUÍZAMO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
704	PUERTO LIBERTADOR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
705	PUERTO LLERAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
706	PUERTO LÓPEZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
707	PUERTO NARE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
708	PUERTO NARIÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	91
709	PUERTO PARRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
710	PUERTO RICO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
711	PUERTO RICO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
712	PUERTO RONDÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	81
713	PUERTO SALGAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
714	PUERTO SANTANDER	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
715	PUERTO TEJADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
716	PUERTO TRIUNFO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
717	PUERTO WILCHES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
718	PULÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
719	PUPIALES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
720	PURACÉ (COCONUCO)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
721	PURIFICACIÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
722	PURÍSIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
723	PÁCORA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
724	PÁEZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
725	PÁEZ (BELALCAZAR)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
726	PÁRAMO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
727	QUEBRADANEGRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
728	QUETAME	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
729	QUIBDÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
730	QUIMBAYA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
731	QUINCHÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
732	QUIPAMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
733	QUIPILE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
734	RAGONVALIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
735	RAMIRIQUÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
736	RECETOR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
737	REGIDOR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
738	REMEDIOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
739	REMOLINO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
740	REPELÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
741	RESTREPO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
742	RESTREPO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
743	RETIRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
744	RICAURTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
745	RICAURTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
746	RIO NEGRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
747	RIOBLANCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
748	RIOFRÍO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
749	RIOHACHA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
750	RISARALDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
751	RIVERA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
752	ROBERTO PAYÁN (SAN JOSÉ)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
753	ROLDANILLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
754	RONCESVALLES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
755	RONDÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
756	ROSAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
757	ROVIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
758	RÁQUIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
759	RÍO IRÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
760	RÍO QUITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
761	RÍO SUCIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
762	RÍO VIEJO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
763	RÍO DE ORO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
764	RÍONEGRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
765	RÍOSUCIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
766	SABANA DE TORRES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
877	SANTA MARTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
767	SABANAGRANDE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
768	SABANALARGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
769	SABANALARGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
770	SABANALARGA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
771	SABANAS DE SAN ANGEL (SAN ANGEL)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
772	SABANETA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
773	SABOYÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
774	SAHAGÚN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
775	SALADOBLANCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
776	SALAMINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
777	SALAMINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
778	SALAZAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
779	SALDAÑA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
780	SALENTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	63
781	SALGAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
782	SAMACÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
783	SAMANIEGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
784	SAMANÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
785	SAMPUÉS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
786	SAN AGUSTÍN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
787	SAN ALBERTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
788	SAN ANDRÉS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
789	SAN ANDRÉS SOTAVENTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
790	SAN ANDRÉS DE CUERQUÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
791	SAN ANTERO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
792	SAN ANTONIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
793	SAN ANTONIO DE TEQUENDAMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
794	SAN BENITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
795	SAN BENITO ABAD	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
796	SAN BERNARDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
797	SAN BERNARDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
798	SAN BERNARDO DEL VIENTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
799	SAN CALIXTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
800	SAN CARLOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
801	SAN CARLOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
802	SAN CARLOS DE GUAROA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
803	SAN CAYETANO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
804	SAN CAYETANO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
805	SAN CRISTOBAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
806	SAN DIEGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
807	SAN EDUARDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
808	SAN ESTANISLAO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
809	SAN FERNANDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
810	SAN FRANCISCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
811	SAN FRANCISCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
812	SAN FRANCISCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
813	SAN GÍL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
814	SAN JACINTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
815	SAN JACINTO DEL CAUCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
816	SAN JERÓNIMO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
817	SAN JOAQUÍN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
818	SAN JOSÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
819	SAN JOSÉ DE MIRANDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
820	SAN JOSÉ DE MONTAÑA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
821	SAN JOSÉ DE PARE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
822	SAN JOSÉ DE URÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
823	SAN JOSÉ DEL FRAGUA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
824	SAN JOSÉ DEL GUAVIARE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	95
825	SAN JOSÉ DEL PALMAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
826	SAN JUAN DE ARAMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
827	SAN JUAN DE BETULIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
828	SAN JUAN DE NEPOMUCENO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
829	SAN JUAN DE PASTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
830	SAN JUAN DE RÍO SECO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
831	SAN JUAN DE URABÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
832	SAN JUAN DEL CESAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
833	SAN JUANITO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
834	SAN LORENZO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
835	SAN LUIS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
836	SAN LUÍS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
837	SAN LUÍS DE GACENO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
838	SAN LUÍS DE PALENQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
839	SAN MARCOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
840	SAN MARTÍN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
841	SAN MARTÍN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
842	SAN MARTÍN DE LOBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
843	SAN MATEO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
844	SAN MIGUEL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
845	SAN MIGUEL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
846	SAN MIGUEL DE SEMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
847	SAN ONOFRE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
848	SAN PABLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
849	SAN PABLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
850	SAN PABLO DE BORBUR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
851	SAN PEDRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
852	SAN PEDRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
853	SAN PEDRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
854	SAN PEDRO DE CARTAGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
855	SAN PEDRO DE URABÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
856	SAN PELAYO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
857	SAN RAFAEL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
858	SAN ROQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
859	SAN SEBASTIÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
860	SAN SEBASTIÁN DE BUENAVISTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
861	SAN VICENTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
862	SAN VICENTE DEL CAGUÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
863	SAN VICENTE DEL CHUCURÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
864	SAN ZENÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
865	SANDONÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
866	SANTA ANA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
867	SANTA BÁRBARA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
868	SANTA BÁRBARA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
869	SANTA BÁRBARA (ISCUANDÉ)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
870	SANTA BÁRBARA DE PINTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
871	SANTA CATALINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
872	SANTA FÉ DE ANTIOQUIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
873	SANTA GENOVEVA DE DOCORODÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
874	SANTA HELENA DEL OPÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
875	SANTA ISABEL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
876	SANTA LUCÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
878	SANTA MARÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
879	SANTA MARÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
880	SANTA ROSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
881	SANTA ROSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
882	SANTA ROSA DE CABAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
883	SANTA ROSA DE OSOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
884	SANTA ROSA DE VITERBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
885	SANTA ROSA DEL SUR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
886	SANTA ROSALÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	99
887	SANTA SOFÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
888	SANTANA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
889	SANTANDER DE QUILICHAO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
890	SANTIAGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
891	SANTIAGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
892	SANTO DOMINGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
893	SANTO TOMÁS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
894	SANTUARIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
895	SANTUARIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	66
896	SAPUYES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
897	SARAVENA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	81
898	SARDINATA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
899	SASAIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
900	SATIVANORTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
901	SATIVASUR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
902	SEGOVIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
903	SESQUILÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
904	SEVILLA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
905	SIACHOQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
906	SIBATÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
907	SIBUNDOY	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
908	SILOS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
909	SILVANIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
910	SILVIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
911	SIMACOTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
912	SIMIJACA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
913	SIMITÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
914	SINCELEJO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
915	SINCÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
916	SIPÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
917	SITIONUEVO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
918	SOACHA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
919	SOATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
920	SOCHA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
921	SOCORRO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
922	SOCOTÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
923	SOGAMOSO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
924	SOLANO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
925	SOLEDAD	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
926	SOLITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
927	SOMONDOCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
928	SONSÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
929	SOPETRÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
930	SOPLAVIENTO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
931	SOPÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
932	SORA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
933	SORACÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
934	SOTAQUIRÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
935	SOTARA (PAISPAMBA)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
936	SOTOMAYOR (LOS ANDES)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
937	SUAITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
938	SUAN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
939	SUAZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
940	SUBACHOQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
941	SUCRE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
942	SUCRE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
943	SUCRE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
944	SUESCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
945	SUPATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
946	SUPÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
947	SURATÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
948	SUSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
949	SUSACÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
950	SUTAMARCHÁN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
951	SUTATAUSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
952	SUTATENZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
953	SUÁREZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
954	SUÁREZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
955	SÁCAMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
956	SÁCHICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
957	TABIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
958	TADÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
959	TALAIGUA NUEVO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
960	TAMALAMEQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
961	TAME	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	81
962	TAMINANGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
963	TANGUA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
964	TARAIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	97
965	TARAZÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
966	TARQUI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
967	TARSO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
968	TASCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
969	TAURAMENA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
970	TAUSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
971	TELLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
972	TENA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
973	TENERIFE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
974	TENJO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
975	TENZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
976	TEORAMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
977	TERUEL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
978	TESALIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
979	TIBACUY	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
980	TIBANÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
981	TIBASOSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
982	TIBIRITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
983	TIBÚ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
984	TIERRALTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
985	TIMANÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
986	TIMBIQUÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
987	TIMBÍO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
988	TINJACÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
989	TIPACOQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
990	TIQUISIO (PUERTO RICO)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
991	TITIRIBÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
992	TOCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
993	TOCAIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
994	TOCANCIPÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
995	TOGUÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
996	TOLEDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
997	TOLEDO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
998	TOLÚ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
999	TOLÚ VIEJO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	70
1000	TONA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
1001	TOPAGÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1002	TOPAIPÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1003	TORIBÍO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
1004	TORO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1005	TOTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1006	TOTORÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
1007	TRINIDAD	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
1008	TRUJILLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1009	TUBARÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
1010	TUCHÍN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
1011	TULÚA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1012	TUMACO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
1013	TUNJA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1014	TUNUNGUA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1015	TURBACO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
1016	TURBANÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
1017	TURBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1018	TURMEQUÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1019	TUTA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1020	TUTASÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1021	TÁMARA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
1022	TÁMESIS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1023	TÚQUERRES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
1024	UBALÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1025	UBAQUE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1026	UBATÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1027	ULLOA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1028	UNE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1029	UNGUÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
1030	UNIÓN PANAMERICANA (ÁNIMAS)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	27
1031	URAMITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1032	URIBE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
1033	URIBIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
1034	URRAO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1035	URUMITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
1036	USIACURI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	8
1037	VALDIVIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1038	VALENCIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	23
1039	VALLE DE SAN JOSÉ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
1040	VALLE DE SAN JUAN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
1041	VALLE DEL GUAMUEZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
1042	VALLEDUPAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	20
1043	VALPARAISO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1044	VALPARAISO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	18
1045	VEGACHÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1046	VENADILLO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
1047	VENECIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1048	VENECIA (OSPINA PÉREZ)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1049	VENTAQUEMADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1050	VERGARA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1051	VERSALLES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1052	VETAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
1053	VIANI	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1054	VIGÍA DEL FUERTE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1055	VIJES	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1056	VILLA CARO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
1057	VILLA RICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	19
1058	VILLA DE LEIVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1059	VILLA DEL ROSARIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
1060	VILLAGARZÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	86
1061	VILLAGÓMEZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1062	VILLAHERMOSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
1063	VILLAMARÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
1064	VILLANUEVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
1065	VILLANUEVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	44
1066	VILLANUEVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
1067	VILLANUEVA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
1068	VILLAPINZÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1069	VILLARRICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	73
1070	VILLAVICENCIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
1071	VILLAVIEJA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
1072	VILLETA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1073	VIOTÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1074	VIRACACHÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1075	VISTA HERMOSA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	50
1076	VITERBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	17
1077	VÉLEZ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
1078	YACOPÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1079	YACUANQUER	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	52
1080	YAGUARÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
1081	YALÍ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1082	YARUMAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1083	YOLOMBÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1084	YONDÓ (CASABE)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1085	YOPAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	85
1086	YOTOCO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1087	YUMBO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1088	ZAMBRANO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	13
1089	ZAPATOCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	68
1090	ZAPAYÁN (PUNTA DE PIEDRAS)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
1091	ZARAGOZA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	5
1092	ZARZAL	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	76
1093	ZETAQUIRÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1094	ZIPACÓN	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1095	ZIPAQUIRÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
1096	ZONA BANANERA (PRADO - SEVILLA)	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	47
1097	ÁBREGO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	54
1098	ÍQUIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	41
1099	ÚMBITA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	15
1100	ÚTICA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	25
\.


--
-- Data for Name: ubicacion_departamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ubicacion_departamento (id, nombre, fecha_creacion, fecha_actualizacion, pais_id) FROM stdin;
5	ANTIOQUIA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759989-05	1
8	ATLÁNTICO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
11	BOGOTÁ, D.C.	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
13	BOLÍVAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
15	BOYACÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
17	CALDAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
18	CAQUETÁ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
19	CAUCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
20	CESAR	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
23	CÓRDOBA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
25	CUNDINAMARCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
27	CHOCÓ	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
41	HUILA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
44	LA GUAJIRA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
47	MAGDALENA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
50	META	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
52	NARIÑO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
54	NORTE DE SANTANDER	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
63	QUINDIO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
66	RISARALDA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
68	SANTANDER	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
70	SUCRE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
73	TOLIMA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
76	VALLE DEL CAUCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
81	ARAUCA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
85	CASANARE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
86	PUTUMAYO	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
88	ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
91	AMAZONAS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
94	GUAINÍA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
95	GUAVIARE	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
97	VAUPÉS	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
99	VICHADA	2019-04-11 16:47:19.759969-05	2019-04-11 16:47:19.759969-05	1
\.


--
-- Data for Name: ubicacion_pais; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ubicacion_pais (id, nombre, nomenclatura, fecha_creacion, fecha_actualizacion) FROM stdin;
1	Colombia	CO	2019-04-11 15:30:16.447393-05	2019-04-11 15:30:16.447411-05
\.


--
-- Data for Name: usuarios_tipoidentificacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios_tipoidentificacion (id, nombre, fecha_creacion, fecha_actualizacion) FROM stdin;
1	Cedula Ciudadanía	2019-04-25 11:17:24.732024-05	2019-04-25 11:17:24.732111-05
2	Tarjeta De Extranjería	2019-04-25 11:19:28.936161-05	2019-04-25 11:19:28.936198-05
3	Cedula Extranjería	2019-04-25 11:17:44.022671-05	2019-04-25 11:17:44.022722-05
4	Nit	2019-04-25 11:17:48.124827-05	2019-04-25 11:17:48.124863-05
5	Pasaporte	2019-04-25 11:17:54.10421-05	2019-04-25 11:17:54.104233-05
7	Nit De Otro País	2019-04-25 11:18:11.543615-05	2019-04-25 11:18:11.54366-05
6	Documento De Identificación Extranjero	2019-04-25 11:18:03.751024-05	2019-04-25 11:18:03.751097-05
\.


--
-- Data for Name: usuarios_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios_usuario (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined, codigo, fecha_nacimiento, is_super_administrador, is_administrador, is_responsable_campana, is_asesor, is_persona_natural, serial, modelo, fabricante, plataforma, version_plataforma, md5, imagen, numero_documento, fecha_creacion, fecha_actualizacion, padre_id, tipo_identificacion_id, barrio, bis_via, bis_via_generadora, complemento, numero_placa, numero_via, numero_via_generadora, tipo_via, cuadrante_via, cuadrante_via_generadora, prefijo_via, prefijo_via_generadora, is_administrador_punto, ciudad_id, departamento_id, pais_id, eliminado, estado, telefono, extencion, asesor_id, asesor_to_responsable_campana_id) FROM stdin;
1	pbkdf2_sha256$150000$925yeNl0A7lk$95mxnB/wJvmazWJWNO74mJdm5oBmEp5bnrqgi1e+CsI=	2019-07-10 13:02:07.775909-05	t	admin	admin		fernando9311@gmail.com6	t	t	2019-04-11 15:28:06.640284-05	\N	2019-06-27	f	f	f	f	f	\N	\N	\N	\N	\N				2019-04-11 15:28:06.760877-05	2019-04-11 15:28:06.760884-05	\N	\N	\N	f	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f	t	\N	\N	\N	\N
238	pbkdf2_sha256$150000$925yeNl0A7lk$95mxnB/wJvmazWJWNO74mJdm5oBmEp5bnrqgi1e+CsI=	2019-07-09 09:10:32.375364-05	f	carolina	carolina	carolina	fernando9311@gmail.com5	f	t	2019-06-20 11:22:16-05	\N	2019-06-27	t	f	f	f	f	\N	\N	\N	\N	\N		imagen-cliente/user2.svg	1	2019-06-20 11:24:02.150203-05	2019-06-20 11:24:02.150219-05	\N	1	\N	f	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	f	t	\N	\N	\N	\N
249	pbkdf2_sha256$150000$925yeNl0A7lk$95mxnB/wJvmazWJWNO74mJdm5oBmEp5bnrqgi1e+CsI=	2019-07-10 17:23:27.879089-05	f	tncolombia@tncolombia.com	tncolombia	tnsas	fernando9311@gmail.com	f	t	2019-06-26 17:42:43.227294-05	\N	\N	f	t	f	f	f	\N	\N	\N	\N	\N		imagen-cliente/images (1).jpeg	01	2019-06-26 17:42:43.387398-05	2019-07-04 17:14:36.801858-05	238	1	barrio	t	t	complemento	30	1	1	CARRERA	SUR	SUR	A	A	f	\N	\N	\N	f	t	\N	\N	\N	\N
\.


--
-- Data for Name: usuarios_usuario_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios_usuario_groups (id, usuario_id, group_id) FROM stdin;
\.


--
-- Data for Name: usuarios_usuario_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios_usuario_user_permissions (id, usuario_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 108, true);


--
-- Name: clientes_cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clientes_cliente_id_seq', 197, true);


--
-- Name: clientes_cliente_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clientes_cliente_usuario_id_seq', 120, true);


--
-- Name: clientes_regimenfiscal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clientes_regimenfiscal_id_seq', 2, true);


--
-- Name: clientes_tipoorganizacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clientes_tipoorganizacion_id_seq', 2, true);


--
-- Name: clientes_tiposociedad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clientes_tiposociedad_id_seq', 8, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 327, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 26, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 215, true);


--
-- Name: gestores_campana_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gestores_campana_id_seq', 1, false);


--
-- Name: gestores_campana_pto_operacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gestores_campana_pto_operacion_id_seq', 1, false);


--
-- Name: gestores_campana_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gestores_campana_usuario_id_seq', 1, false);


--
-- Name: gestores_enlace_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gestores_enlace_id_seq', 9, true);


--
-- Name: gestores_publicidad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gestores_publicidad_id_seq', 10, true);


--
-- Name: gestores_servicio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gestores_servicio_id_seq', 18, true);


--
-- Name: gestores_tipoenlace_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gestores_tipoenlace_id_seq', 1, false);


--
-- Name: sedes_ptooperacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sedes_ptooperacion_id_seq', 41, true);


--
-- Name: sedes_ptooperacion_usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sedes_ptooperacion_usuarios_id_seq', 52, true);


--
-- Name: sedes_regional_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sedes_regional_id_seq', 41, true);


--
-- Name: tipificaciones_etapa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipificaciones_etapa_id_seq', 1, false);


--
-- Name: tipificaciones_homologacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipificaciones_homologacion_id_seq', 1, false);


--
-- Name: tipificaciones_motivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipificaciones_motivo_id_seq', 1, false);


--
-- Name: ubicacion_ciudad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ubicacion_ciudad_id_seq', 1, false);


--
-- Name: ubicacion_departamento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ubicacion_departamento_id_seq', 2, true);


--
-- Name: ubicacion_pais_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ubicacion_pais_id_seq', 1, true);


--
-- Name: usuarios_tipoidentificacione_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_tipoidentificacione_id_seq', 14, true);


--
-- Name: usuarios_usuario_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_usuario_groups_id_seq', 1, false);


--
-- Name: usuarios_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_usuario_id_seq', 261, true);


--
-- Name: usuarios_usuario_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_usuario_user_permissions_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: clientes_cliente clientes_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente
    ADD CONSTRAINT clientes_cliente_pkey PRIMARY KEY (id);


--
-- Name: clientes_cliente_usuario clientes_cliente_usuario_cliente_id_usuario_id_82221810_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente_usuario
    ADD CONSTRAINT clientes_cliente_usuario_cliente_id_usuario_id_82221810_uniq UNIQUE (cliente_id, usuario_id);


--
-- Name: clientes_cliente_usuario clientes_cliente_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente_usuario
    ADD CONSTRAINT clientes_cliente_usuario_pkey PRIMARY KEY (id);


--
-- Name: clientes_regimenfiscal clientes_regimenfiscal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_regimenfiscal
    ADD CONSTRAINT clientes_regimenfiscal_pkey PRIMARY KEY (id);


--
-- Name: clientes_tipoorganizacion clientes_tipoorganizacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_tipoorganizacion
    ADD CONSTRAINT clientes_tipoorganizacion_pkey PRIMARY KEY (id);


--
-- Name: clientes_tiposociedad clientes_tiposociedad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_tiposociedad
    ADD CONSTRAINT clientes_tiposociedad_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: gestores_campana gestores_campana_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana
    ADD CONSTRAINT gestores_campana_pkey PRIMARY KEY (id);


--
-- Name: gestores_campana_pto_operacion gestores_campana_pto_ope_campana_id_ptooperacion__6a9d88b4_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_pto_operacion
    ADD CONSTRAINT gestores_campana_pto_ope_campana_id_ptooperacion__6a9d88b4_uniq UNIQUE (campana_id, ptooperacion_id);


--
-- Name: gestores_campana_pto_operacion gestores_campana_pto_operacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_pto_operacion
    ADD CONSTRAINT gestores_campana_pto_operacion_pkey PRIMARY KEY (id);


--
-- Name: gestores_campana_usuario gestores_campana_usuario_campana_id_usuario_id_5dceeea6_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_usuario
    ADD CONSTRAINT gestores_campana_usuario_campana_id_usuario_id_5dceeea6_uniq UNIQUE (campana_id, usuario_id);


--
-- Name: gestores_campana_usuario gestores_campana_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_usuario
    ADD CONSTRAINT gestores_campana_usuario_pkey PRIMARY KEY (id);


--
-- Name: gestores_enlace gestores_enlace_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_enlace
    ADD CONSTRAINT gestores_enlace_pkey PRIMARY KEY (id);


--
-- Name: gestores_publicidad gestores_publicidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_publicidad
    ADD CONSTRAINT gestores_publicidad_pkey PRIMARY KEY (id);


--
-- Name: gestores_servicio gestores_servicio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_servicio
    ADD CONSTRAINT gestores_servicio_pkey PRIMARY KEY (id);


--
-- Name: gestores_tipoenlace gestores_tipoenlace_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_tipoenlace
    ADD CONSTRAINT gestores_tipoenlace_pkey PRIMARY KEY (id);


--
-- Name: sedes_ptooperacion sedes_ptooperacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion
    ADD CONSTRAINT sedes_ptooperacion_pkey PRIMARY KEY (id);


--
-- Name: sedes_ptooperacion_usuarios sedes_ptooperacion_usuar_ptooperacion_id_usuario__820a8fd3_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion_usuarios
    ADD CONSTRAINT sedes_ptooperacion_usuar_ptooperacion_id_usuario__820a8fd3_uniq UNIQUE (ptooperacion_id, usuario_id);


--
-- Name: sedes_ptooperacion_usuarios sedes_ptooperacion_usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion_usuarios
    ADD CONSTRAINT sedes_ptooperacion_usuarios_pkey PRIMARY KEY (id);


--
-- Name: sedes_regional sedes_regional_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_regional
    ADD CONSTRAINT sedes_regional_pkey PRIMARY KEY (id);


--
-- Name: tipificaciones_etapa tipificaciones_etapa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_etapa
    ADD CONSTRAINT tipificaciones_etapa_pkey PRIMARY KEY (id);


--
-- Name: tipificaciones_homologacion tipificaciones_homologacion_motivo_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_homologacion
    ADD CONSTRAINT tipificaciones_homologacion_motivo_id_key UNIQUE (motivo_id);


--
-- Name: tipificaciones_homologacion tipificaciones_homologacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_homologacion
    ADD CONSTRAINT tipificaciones_homologacion_pkey PRIMARY KEY (id);


--
-- Name: tipificaciones_motivo tipificaciones_motivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_motivo
    ADD CONSTRAINT tipificaciones_motivo_pkey PRIMARY KEY (id);


--
-- Name: ubicacion_ciudad ubicacion_ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_ciudad
    ADD CONSTRAINT ubicacion_ciudad_pkey PRIMARY KEY (id);


--
-- Name: ubicacion_departamento ubicacion_departamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_departamento
    ADD CONSTRAINT ubicacion_departamento_pkey PRIMARY KEY (id);


--
-- Name: ubicacion_pais ubicacion_pais_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_pais
    ADD CONSTRAINT ubicacion_pais_pkey PRIMARY KEY (id);


--
-- Name: usuarios_tipoidentificacion usuarios_tipoidentificacione_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_tipoidentificacion
    ADD CONSTRAINT usuarios_tipoidentificacione_pkey PRIMARY KEY (id);


--
-- Name: usuarios_usuario_groups usuarios_usuario_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_groups
    ADD CONSTRAINT usuarios_usuario_groups_pkey PRIMARY KEY (id);


--
-- Name: usuarios_usuario_groups usuarios_usuario_groups_usuario_id_group_id_4ed5b09e_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_groups
    ADD CONSTRAINT usuarios_usuario_groups_usuario_id_group_id_4ed5b09e_uniq UNIQUE (usuario_id, group_id);


--
-- Name: usuarios_usuario usuarios_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_pkey PRIMARY KEY (id);


--
-- Name: usuarios_usuario_user_permissions usuarios_usuario_user_pe_usuario_id_permission_id_217cadcd_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_user_permissions
    ADD CONSTRAINT usuarios_usuario_user_pe_usuario_id_permission_id_217cadcd_uniq UNIQUE (usuario_id, permission_id);


--
-- Name: usuarios_usuario_user_permissions usuarios_usuario_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_user_permissions
    ADD CONSTRAINT usuarios_usuario_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: usuarios_usuario usuarios_usuario_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_username_key UNIQUE (username);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: clientes_cliente_ciudad_id_449706d9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_ciudad_id_449706d9 ON public.clientes_cliente USING btree (ciudad_id);


--
-- Name: clientes_cliente_departamento_id_602bae8c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_departamento_id_602bae8c ON public.clientes_cliente USING btree (departamento_id);


--
-- Name: clientes_cliente_pais_id_f342ccfc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_pais_id_f342ccfc ON public.clientes_cliente USING btree (pais_id);


--
-- Name: clientes_cliente_regimenfiscal_id_a370ac96; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_regimenfiscal_id_a370ac96 ON public.clientes_cliente USING btree (regimenfiscal_id);


--
-- Name: clientes_cliente_tipo_identificacion_id_b34b38c0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_tipo_identificacion_id_b34b38c0 ON public.clientes_cliente USING btree (tipo_identificacion_id);


--
-- Name: clientes_cliente_tipo_organizacion_id_95117564; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_tipo_organizacion_id_95117564 ON public.clientes_cliente USING btree (tipo_organizacion_id);


--
-- Name: clientes_cliente_tipo_sociedad_id_bb7bbc3c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_tipo_sociedad_id_bb7bbc3c ON public.clientes_cliente USING btree (tipo_sociedad_id);


--
-- Name: clientes_cliente_usuario_cliente_id_6d863c93; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_usuario_cliente_id_6d863c93 ON public.clientes_cliente_usuario USING btree (cliente_id);


--
-- Name: clientes_cliente_usuario_usuario_id_d223c6b2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX clientes_cliente_usuario_usuario_id_d223c6b2 ON public.clientes_cliente_usuario USING btree (usuario_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: gestores_campana_cliente_id_cc3fd17f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gestores_campana_cliente_id_cc3fd17f ON public.gestores_campana USING btree (cliente_id);


--
-- Name: gestores_campana_pto_operacion_campana_id_1e595686; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gestores_campana_pto_operacion_campana_id_1e595686 ON public.gestores_campana_pto_operacion USING btree (campana_id);


--
-- Name: gestores_campana_pto_operacion_ptooperacion_id_737279dd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gestores_campana_pto_operacion_ptooperacion_id_737279dd ON public.gestores_campana_pto_operacion USING btree (ptooperacion_id);


--
-- Name: gestores_campana_usuario_campana_id_a34429bc; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gestores_campana_usuario_campana_id_a34429bc ON public.gestores_campana_usuario USING btree (campana_id);


--
-- Name: gestores_campana_usuario_usuario_id_e220a5d0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gestores_campana_usuario_usuario_id_e220a5d0 ON public.gestores_campana_usuario USING btree (usuario_id);


--
-- Name: gestores_enlace_cliente_id_9def73e1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gestores_enlace_cliente_id_9def73e1 ON public.gestores_enlace USING btree (cliente_id);


--
-- Name: gestores_publicidad_cliente_id_68d8621a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gestores_publicidad_cliente_id_68d8621a ON public.gestores_publicidad USING btree (cliente_id);


--
-- Name: gestores_servicio_cliente_id_5aa02995; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gestores_servicio_cliente_id_5aa02995 ON public.gestores_servicio USING btree (cliente_id);


--
-- Name: sedes_ptooperacion_ciudad_id_31956f1e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sedes_ptooperacion_ciudad_id_31956f1e ON public.sedes_ptooperacion USING btree (ciudad_id);


--
-- Name: sedes_ptooperacion_cliente_id_e89fdeb2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sedes_ptooperacion_cliente_id_e89fdeb2 ON public.sedes_ptooperacion USING btree (cliente_id);


--
-- Name: sedes_ptooperacion_departamento_id_4fa5b41a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sedes_ptooperacion_departamento_id_4fa5b41a ON public.sedes_ptooperacion USING btree (departamento_id);


--
-- Name: sedes_ptooperacion_pais_id_7448edb9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sedes_ptooperacion_pais_id_7448edb9 ON public.sedes_ptooperacion USING btree (pais_id);


--
-- Name: sedes_ptooperacion_regional_id_12cca07f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sedes_ptooperacion_regional_id_12cca07f ON public.sedes_ptooperacion USING btree (regional_id);


--
-- Name: sedes_ptooperacion_usuarios_ptooperacion_id_09bba277; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sedes_ptooperacion_usuarios_ptooperacion_id_09bba277 ON public.sedes_ptooperacion_usuarios USING btree (ptooperacion_id);


--
-- Name: sedes_ptooperacion_usuarios_usuario_id_357481c2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sedes_ptooperacion_usuarios_usuario_id_357481c2 ON public.sedes_ptooperacion_usuarios USING btree (usuario_id);


--
-- Name: sedes_regional_cliente_id_bfd22184; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sedes_regional_cliente_id_bfd22184 ON public.sedes_regional USING btree (cliente_id);


--
-- Name: tipificaciones_motivo_etapa_id_4cec1791; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tipificaciones_motivo_etapa_id_4cec1791 ON public.tipificaciones_motivo USING btree (etapa_id);


--
-- Name: ubicacion_ciudad_departamento_id_56bf5c84; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ubicacion_ciudad_departamento_id_56bf5c84 ON public.ubicacion_ciudad USING btree (departamento_id);


--
-- Name: ubicacion_departamento_pais_id_1ea9e448; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ubicacion_departamento_pais_id_1ea9e448 ON public.ubicacion_departamento USING btree (pais_id);


--
-- Name: usuarios_usuario_asesor_id_e9788293; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_asesor_id_e9788293 ON public.usuarios_usuario USING btree (asesor_id);


--
-- Name: usuarios_usuario_asesor_to_responsable_campana_id_4e5fa278; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_asesor_to_responsable_campana_id_4e5fa278 ON public.usuarios_usuario USING btree (asesor_to_responsable_campana_id);


--
-- Name: usuarios_usuario_ciudad_id_313b4780; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_ciudad_id_313b4780 ON public.usuarios_usuario USING btree (ciudad_id);


--
-- Name: usuarios_usuario_departamento_id_b6b46aa0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_departamento_id_b6b46aa0 ON public.usuarios_usuario USING btree (departamento_id);


--
-- Name: usuarios_usuario_groups_group_id_e77f6dcf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_groups_group_id_e77f6dcf ON public.usuarios_usuario_groups USING btree (group_id);


--
-- Name: usuarios_usuario_groups_usuario_id_7a34077f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_groups_usuario_id_7a34077f ON public.usuarios_usuario_groups USING btree (usuario_id);


--
-- Name: usuarios_usuario_padre_id_59bf7f95; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_padre_id_59bf7f95 ON public.usuarios_usuario USING btree (padre_id);


--
-- Name: usuarios_usuario_pais_id_ee489c87; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_pais_id_ee489c87 ON public.usuarios_usuario USING btree (pais_id);


--
-- Name: usuarios_usuario_tipo_identificacion_id_230ef794; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_tipo_identificacion_id_230ef794 ON public.usuarios_usuario USING btree (tipo_identificacion_id);


--
-- Name: usuarios_usuario_user_permissions_permission_id_4e5c0f2f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_user_permissions_permission_id_4e5c0f2f ON public.usuarios_usuario_user_permissions USING btree (permission_id);


--
-- Name: usuarios_usuario_user_permissions_usuario_id_60aeea80; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_user_permissions_usuario_id_60aeea80 ON public.usuarios_usuario_user_permissions USING btree (usuario_id);


--
-- Name: usuarios_usuario_username_be9def2b_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX usuarios_usuario_username_be9def2b_like ON public.usuarios_usuario USING btree (username varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente clientes_cliente_ciudad_id_449706d9_fk_ubicacion_ciudad_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente
    ADD CONSTRAINT clientes_cliente_ciudad_id_449706d9_fk_ubicacion_ciudad_id FOREIGN KEY (ciudad_id) REFERENCES public.ubicacion_ciudad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente clientes_cliente_departamento_id_602bae8c_fk_ubicacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente
    ADD CONSTRAINT clientes_cliente_departamento_id_602bae8c_fk_ubicacion FOREIGN KEY (departamento_id) REFERENCES public.ubicacion_departamento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente clientes_cliente_pais_id_f342ccfc_fk_ubicacion_pais_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente
    ADD CONSTRAINT clientes_cliente_pais_id_f342ccfc_fk_ubicacion_pais_id FOREIGN KEY (pais_id) REFERENCES public.ubicacion_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente clientes_cliente_regimenfiscal_id_a370ac96_fk_clientes_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente
    ADD CONSTRAINT clientes_cliente_regimenfiscal_id_a370ac96_fk_clientes_ FOREIGN KEY (regimenfiscal_id) REFERENCES public.clientes_regimenfiscal(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente clientes_cliente_tipo_identificacion__b34b38c0_fk_usuarios_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente
    ADD CONSTRAINT clientes_cliente_tipo_identificacion__b34b38c0_fk_usuarios_ FOREIGN KEY (tipo_identificacion_id) REFERENCES public.usuarios_tipoidentificacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente clientes_cliente_tipo_organizacion_id_95117564_fk_clientes_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente
    ADD CONSTRAINT clientes_cliente_tipo_organizacion_id_95117564_fk_clientes_ FOREIGN KEY (tipo_organizacion_id) REFERENCES public.clientes_tipoorganizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente clientes_cliente_tipo_sociedad_id_bb7bbc3c_fk_clientes_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente
    ADD CONSTRAINT clientes_cliente_tipo_sociedad_id_bb7bbc3c_fk_clientes_ FOREIGN KEY (tipo_sociedad_id) REFERENCES public.clientes_tiposociedad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente_usuario clientes_cliente_usu_cliente_id_6d863c93_fk_clientes_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente_usuario
    ADD CONSTRAINT clientes_cliente_usu_cliente_id_6d863c93_fk_clientes_ FOREIGN KEY (cliente_id) REFERENCES public.clientes_cliente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: clientes_cliente_usuario clientes_cliente_usu_usuario_id_d223c6b2_fk_usuarios_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clientes_cliente_usuario
    ADD CONSTRAINT clientes_cliente_usu_usuario_id_d223c6b2_fk_usuarios_ FOREIGN KEY (usuario_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_usuarios_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_usuarios_usuario_id FOREIGN KEY (user_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gestores_campana gestores_campana_cliente_id_cc3fd17f_fk_clientes_cliente_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana
    ADD CONSTRAINT gestores_campana_cliente_id_cc3fd17f_fk_clientes_cliente_id FOREIGN KEY (cliente_id) REFERENCES public.clientes_cliente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gestores_campana_pto_operacion gestores_campana_pto_campana_id_1e595686_fk_gestores_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_pto_operacion
    ADD CONSTRAINT gestores_campana_pto_campana_id_1e595686_fk_gestores_ FOREIGN KEY (campana_id) REFERENCES public.gestores_campana(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gestores_campana_pto_operacion gestores_campana_pto_ptooperacion_id_737279dd_fk_sedes_pto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_pto_operacion
    ADD CONSTRAINT gestores_campana_pto_ptooperacion_id_737279dd_fk_sedes_pto FOREIGN KEY (ptooperacion_id) REFERENCES public.sedes_ptooperacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gestores_campana_usuario gestores_campana_usu_campana_id_a34429bc_fk_gestores_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_usuario
    ADD CONSTRAINT gestores_campana_usu_campana_id_a34429bc_fk_gestores_ FOREIGN KEY (campana_id) REFERENCES public.gestores_campana(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gestores_campana_usuario gestores_campana_usu_usuario_id_e220a5d0_fk_usuarios_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_campana_usuario
    ADD CONSTRAINT gestores_campana_usu_usuario_id_e220a5d0_fk_usuarios_ FOREIGN KEY (usuario_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gestores_enlace gestores_enlace_cliente_id_9def73e1_fk_clientes_cliente_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_enlace
    ADD CONSTRAINT gestores_enlace_cliente_id_9def73e1_fk_clientes_cliente_id FOREIGN KEY (cliente_id) REFERENCES public.clientes_cliente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gestores_publicidad gestores_publicidad_cliente_id_68d8621a_fk_clientes_cliente_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_publicidad
    ADD CONSTRAINT gestores_publicidad_cliente_id_68d8621a_fk_clientes_cliente_id FOREIGN KEY (cliente_id) REFERENCES public.clientes_cliente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gestores_servicio gestores_servicio_cliente_id_5aa02995_fk_clientes_cliente_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gestores_servicio
    ADD CONSTRAINT gestores_servicio_cliente_id_5aa02995_fk_clientes_cliente_id FOREIGN KEY (cliente_id) REFERENCES public.clientes_cliente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sedes_ptooperacion sedes_ptooperacion_ciudad_id_31956f1e_fk_ubicacion_ciudad_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion
    ADD CONSTRAINT sedes_ptooperacion_ciudad_id_31956f1e_fk_ubicacion_ciudad_id FOREIGN KEY (ciudad_id) REFERENCES public.ubicacion_ciudad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sedes_ptooperacion sedes_ptooperacion_cliente_id_e89fdeb2_fk_clientes_cliente_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion
    ADD CONSTRAINT sedes_ptooperacion_cliente_id_e89fdeb2_fk_clientes_cliente_id FOREIGN KEY (cliente_id) REFERENCES public.clientes_cliente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sedes_ptooperacion sedes_ptooperacion_departamento_id_4fa5b41a_fk_ubicacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion
    ADD CONSTRAINT sedes_ptooperacion_departamento_id_4fa5b41a_fk_ubicacion FOREIGN KEY (departamento_id) REFERENCES public.ubicacion_departamento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sedes_ptooperacion sedes_ptooperacion_pais_id_7448edb9_fk_ubicacion_pais_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion
    ADD CONSTRAINT sedes_ptooperacion_pais_id_7448edb9_fk_ubicacion_pais_id FOREIGN KEY (pais_id) REFERENCES public.ubicacion_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sedes_ptooperacion sedes_ptooperacion_regional_id_12cca07f_fk_sedes_regional_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion
    ADD CONSTRAINT sedes_ptooperacion_regional_id_12cca07f_fk_sedes_regional_id FOREIGN KEY (regional_id) REFERENCES public.sedes_regional(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sedes_ptooperacion_usuarios sedes_ptooperacion_u_ptooperacion_id_09bba277_fk_sedes_pto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion_usuarios
    ADD CONSTRAINT sedes_ptooperacion_u_ptooperacion_id_09bba277_fk_sedes_pto FOREIGN KEY (ptooperacion_id) REFERENCES public.sedes_ptooperacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sedes_ptooperacion_usuarios sedes_ptooperacion_u_usuario_id_357481c2_fk_usuarios_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_ptooperacion_usuarios
    ADD CONSTRAINT sedes_ptooperacion_u_usuario_id_357481c2_fk_usuarios_ FOREIGN KEY (usuario_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sedes_regional sedes_regional_cliente_id_bfd22184_fk_clientes_cliente_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sedes_regional
    ADD CONSTRAINT sedes_regional_cliente_id_bfd22184_fk_clientes_cliente_id FOREIGN KEY (cliente_id) REFERENCES public.clientes_cliente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tipificaciones_homologacion tipificaciones_homol_motivo_id_484ba08e_fk_tipificac; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_homologacion
    ADD CONSTRAINT tipificaciones_homol_motivo_id_484ba08e_fk_tipificac FOREIGN KEY (motivo_id) REFERENCES public.tipificaciones_motivo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tipificaciones_motivo tipificaciones_motiv_etapa_id_4cec1791_fk_tipificac; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipificaciones_motivo
    ADD CONSTRAINT tipificaciones_motiv_etapa_id_4cec1791_fk_tipificac FOREIGN KEY (etapa_id) REFERENCES public.tipificaciones_etapa(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ubicacion_ciudad ubicacion_ciudad_departamento_id_56bf5c84_fk_ubicacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_ciudad
    ADD CONSTRAINT ubicacion_ciudad_departamento_id_56bf5c84_fk_ubicacion FOREIGN KEY (departamento_id) REFERENCES public.ubicacion_departamento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ubicacion_departamento ubicacion_departamento_pais_id_1ea9e448_fk_ubicacion_pais_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_departamento
    ADD CONSTRAINT ubicacion_departamento_pais_id_1ea9e448_fk_ubicacion_pais_id FOREIGN KEY (pais_id) REFERENCES public.ubicacion_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario usuarios_usuario_asesor_id_e9788293_fk_usuarios_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_asesor_id_e9788293_fk_usuarios_usuario_id FOREIGN KEY (asesor_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario usuarios_usuario_asesor_to_responsabl_4e5fa278_fk_usuarios_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_asesor_to_responsabl_4e5fa278_fk_usuarios_ FOREIGN KEY (asesor_to_responsable_campana_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario usuarios_usuario_ciudad_id_313b4780_fk_ubicacion_ciudad_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_ciudad_id_313b4780_fk_ubicacion_ciudad_id FOREIGN KEY (ciudad_id) REFERENCES public.ubicacion_ciudad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario usuarios_usuario_departamento_id_b6b46aa0_fk_ubicacion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_departamento_id_b6b46aa0_fk_ubicacion FOREIGN KEY (departamento_id) REFERENCES public.ubicacion_departamento(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario_groups usuarios_usuario_gro_usuario_id_7a34077f_fk_usuarios_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_groups
    ADD CONSTRAINT usuarios_usuario_gro_usuario_id_7a34077f_fk_usuarios_ FOREIGN KEY (usuario_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario_groups usuarios_usuario_groups_group_id_e77f6dcf_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_groups
    ADD CONSTRAINT usuarios_usuario_groups_group_id_e77f6dcf_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario usuarios_usuario_padre_id_59bf7f95_fk_usuarios_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_padre_id_59bf7f95_fk_usuarios_usuario_id FOREIGN KEY (padre_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario usuarios_usuario_pais_id_ee489c87_fk_ubicacion_pais_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_pais_id_ee489c87_fk_ubicacion_pais_id FOREIGN KEY (pais_id) REFERENCES public.ubicacion_pais(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario usuarios_usuario_tipo_identificacion__230ef794_fk_usuarios_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario
    ADD CONSTRAINT usuarios_usuario_tipo_identificacion__230ef794_fk_usuarios_ FOREIGN KEY (tipo_identificacion_id) REFERENCES public.usuarios_tipoidentificacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario_user_permissions usuarios_usuario_use_permission_id_4e5c0f2f_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_user_permissions
    ADD CONSTRAINT usuarios_usuario_use_permission_id_4e5c0f2f_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuarios_usuario_user_permissions usuarios_usuario_use_usuario_id_60aeea80_fk_usuarios_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios_usuario_user_permissions
    ADD CONSTRAINT usuarios_usuario_use_usuario_id_60aeea80_fk_usuarios_ FOREIGN KEY (usuario_id) REFERENCES public.usuarios_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

